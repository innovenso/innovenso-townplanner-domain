package com.innovenso.townplan.domain


import com.innovenso.townplan.api.command.AddCapabilityCommand
import com.innovenso.townplan.api.command.AddContentDistributionAspectCommand
import com.innovenso.townplan.api.command.AddEnterpriseCommand
import com.innovenso.townplan.api.value.aspects.ContentOutputType
import com.innovenso.townplan.api.value.business.Enterprise
import com.innovenso.townplan.api.value.strategy.BusinessCapability
import spock.lang.Specification

class ContentDistributionAspectSpec extends Specification {
	TownPlanImpl townPlan

	def setup() {
		townPlan = new TownPlanImpl()
	}

	def "content distribution can be added to an element"() {
		given:
		townPlan.execute(AddEnterpriseCommand.builder()
				.key("innovenso")
				.title("Innovenso")
				.description("Innovenso BV")
				.build())
		townPlan.execute(AddCapabilityCommand.builder()
				.key("cap1")
				.title("Capability 1")
				.description("A capability")
				.enterprise("innovenso")
				.parent("")
				.build())
		when:
		townPlan.execute(AddContentDistributionAspectCommand.builder()
				.modelComponentKey("innovenso")
				.type(ContentOutputType.PNG)
				.url("https://innovenso.com/test.png")
				.build())
		then:
		townPlan.getElement(Enterprise.class, "innovenso").get().with { enterprise ->
			enterprise.getContentDistributions().with {distributions ->
				!distributions.isEmpty()
				distributions.each {distribution ->
					distribution.getUrl() == "https://innovenso.com/test.png"
					distribution.getOutputType() == ContentOutputType.PNG
				}
			}
		}
		townPlan.getElement(BusinessCapability.class, "cap1").get().with {it.getContentDistributions().isEmpty()}
	}
}
