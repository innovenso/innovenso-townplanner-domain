package com.innovenso.townplan.domain


import spock.lang.Ignore
import spock.lang.Specification

@Ignore
class ElementImplSpec extends Specification {
	def "IT System can connect to IT System"() {
		when:
		ItSystemImpl system1 = ItSystemImpl.builder().build();
		ItSystemImpl system2 = ItSystemImpl.builder().build();

		then:
		system1.acceptsIncomingDependency(system2.getClass())
	}
}
