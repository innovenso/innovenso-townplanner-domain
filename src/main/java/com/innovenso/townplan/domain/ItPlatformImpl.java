package com.innovenso.townplan.domain;

import com.innovenso.townplan.api.value.Element;
import com.innovenso.townplan.api.value.business.BusinessActor;
import com.innovenso.townplan.api.value.enums.TechnologyType;
import com.innovenso.townplan.api.value.it.*;
import com.innovenso.townplan.api.value.it.decision.Decision;
import com.innovenso.townplan.api.value.it.decision.DecisionOption;
import lombok.experimental.SuperBuilder;

import java.util.Set;
import java.util.stream.Collectors;

@SuperBuilder
public class ItPlatformImpl extends AbstractElementImpl implements ItPlatform {

	@Override
	public Set<ItSystem> getSystems() {
		return getChildren().stream().filter(ItSystem.class::isInstance).map(ItSystem.class::cast)
				.collect(Collectors.toSet());
	}

	@Override
	public Set<ItContainer> getContainers() {
		return getSystems().stream().flatMap(system -> system.getContainers().stream()).collect(Collectors.toSet());
	}

	@Override
	public Set<ArchitectureBuildingBlock> getBuildingBlocks() {
		return getDirectDependencies(ArchitectureBuildingBlock.class).stream()
				.filter(ArchitectureBuildingBlock.class::isInstance).map(ArchitectureBuildingBlock.class::cast)
				.collect(Collectors.toSet());
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedIncomingDependencies() {
		return Set.of(ItPlatform.class, ItSystem.class, ItContainer.class, BusinessActor.class,
				ItProjectMilestone.class, Decision.class, DecisionOption.class);
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedOutgoingDependencies() {
		return Set.of(ItPlatform.class, ItSystem.class, ItContainer.class, ArchitectureBuildingBlock.class,
				ItSystemIntegration.class);
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedChildren() {
		return Set.of(ItSystem.class);
	}

	@Override
	public String getCategory() {
		return "Platform";
	}

	@Override
	public Set<Technology> getTechnologies(TechnologyType technologyType) {
		return getContainers().stream().flatMap(container -> container.getTechnologies(technologyType).stream())
				.collect(Collectors.toSet());
	}

	@Override
	public Set<Technology> getTechnologies() {
		return getContainers().stream().flatMap(container -> container.getTechnologies().stream())
				.collect(Collectors.toSet());
	}
}
