package com.innovenso.townplan.domain;

import com.innovenso.townplan.api.value.Element;
import com.innovenso.townplan.api.value.deployment.aws.AwsInstance;
import com.innovenso.townplan.api.value.deployment.aws.AwsSecurityGroup;
import com.innovenso.townplan.api.value.deployment.aws.AwsVpc;
import lombok.experimental.SuperBuilder;
import lombok.extern.log4j.Log4j2;

import java.util.Set;

@SuperBuilder
@Log4j2
public class AwsSecurityGroupImpl extends AbstractAwsElementImpl implements AwsSecurityGroup {
	private AwsVpc vpc;

	@Override
	public AwsVpc getVpc() {
		return vpc;
	}

	public void setVpc(AwsVpc vpc) {
		this.vpc = vpc;
	}

	@Override
	public Set<AwsInstance> getInstances() {
		return getChildren(AwsInstance.class);
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedIncomingDependencies() {
		return Set.of();
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedOutgoingDependencies() {
		return Set.of();
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedChildren() {
		return Set.of(AwsInstance.class);
	}

	@Override
	public String getType() {
		return "Security Group";
	}

	@Override
	public String getCategory() {
		return "AWS";
	}
}
