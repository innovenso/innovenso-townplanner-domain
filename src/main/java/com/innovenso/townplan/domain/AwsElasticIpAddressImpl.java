package com.innovenso.townplan.domain;

import com.innovenso.townplan.api.value.Element;
import com.innovenso.townplan.api.value.deployment.aws.AwsElasticIpAddress;
import com.innovenso.townplan.api.value.deployment.aws.AwsInstance;
import lombok.Getter;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;
import lombok.extern.log4j.Log4j2;

import java.util.Optional;
import java.util.Set;

@SuperBuilder
@Log4j2
@Getter
public class AwsElasticIpAddressImpl extends AbstractAwsElementImpl implements AwsElasticIpAddress {
	private final String allocationId;
	private final String associationId;
	private final String carrierIp;
	private final String customerOwnedIp;
	private final String customerOwnedIpv4Pool;
	private final String domain;
	private final String instanceId;
	private final String networkBorderGroup;
	private final String networkInterfaceId;
	private final String networkInterfaceOwnerId;
	private final String privateIpAddress;
	private final String publicIpAddress;
	private final String publicIpv4Pool;
	private final AwsAccountImpl account;
	private final AwsInstanceImpl instance;

	@Override
	public Optional<AwsInstance> getInstance() {
		return Optional.ofNullable(instance);
	}

	@NonNull
	private final String region;

	@Override
	public Set<Class<? extends Element>> getAcceptedIncomingDependencies() {
		return Set.of(AwsInstance.class);
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedOutgoingDependencies() {
		return Set.of(AwsInstance.class);
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedChildren() {
		return Set.of();
	}

	@Override
	public String getType() {
		return "elastic IP";
	}

	@Override
	public String getCategory() {
		return "aws";
	}
}
