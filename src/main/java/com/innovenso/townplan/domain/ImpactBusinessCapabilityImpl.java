package com.innovenso.townplan.domain;

import com.innovenso.townplan.api.value.strategy.BusinessCapability;
import com.innovenso.townplan.api.value.view.ImpactBusinessCapability;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;

@SuperBuilder
public class ImpactBusinessCapabilityImpl implements ImpactBusinessCapability {
	private @NonNull final BusinessCapability capability;
	private final boolean impacted;

	@Override
	public BusinessCapability getCapability() {
		return capability;
	}

	@Override
	public boolean isImpacted() {
		return impacted;
	}
}
