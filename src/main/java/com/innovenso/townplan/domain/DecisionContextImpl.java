package com.innovenso.townplan.domain;

import com.innovenso.townplan.api.value.Element;
import com.innovenso.townplan.api.value.it.decision.Decision;
import com.innovenso.townplan.api.value.it.decision.DecisionContext;
import com.innovenso.townplan.api.value.it.decision.DecisionContextType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
public class DecisionContextImpl extends AbstractElementImpl implements DecisionContext {
	private String sortKey;
	private final Set<Class<? extends Element>> acceptedIncomingDependencies = Set.of();
	private final Set<Class<? extends Element>> acceptedOutgoingDependencies = Set.of();
	private final Set<Class<? extends Element>> acceptedChildren = Set.of();
	private DecisionContextType contextType;

	@Override
	public Decision getDecision() {
		return getParent().filter(Decision.class::isInstance).map(Decision.class::cast).orElse(null);
	}
}
