package com.innovenso.townplan.domain;

import com.innovenso.townplan.api.value.Element;
import com.innovenso.townplan.api.value.business.Enterprise;
import com.innovenso.townplan.api.value.it.ArchitectureBuildingBlock;
import com.innovenso.townplan.api.value.it.ItPlatform;
import com.innovenso.townplan.api.value.it.ItSystem;
import com.innovenso.townplan.api.value.it.decision.Decision;
import com.innovenso.townplan.api.value.it.decision.DecisionOption;
import com.innovenso.townplan.api.value.strategy.BusinessCapability;
import lombok.experimental.SuperBuilder;

import java.util.Set;

@SuperBuilder
public class ArchitectureBuildingBlockImpl extends AbstractElementImpl implements ArchitectureBuildingBlock {
	@Override
	public Enterprise getEnterprise() {
		return enterprise;
	}

	public void setEnterprise(Enterprise enterprise) {
		this.enterprise = enterprise;
	}

	private Enterprise enterprise;

	@Override
	public Set<Class<? extends Element>> getAcceptedIncomingDependencies() {
		return Set.of(ItPlatform.class, ItSystem.class, ItProjectMilestoneImpl.class,
				ArchitectureBuildingBlockImpl.class, BusinessActorImpl.class, Decision.class, DecisionOption.class);
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedOutgoingDependencies() {
		return Set.of(ItSystem.class, BusinessCapability.class, ArchitectureBuildingBlockImpl.class);
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedChildren() {
		return Set.of();
	}
}
