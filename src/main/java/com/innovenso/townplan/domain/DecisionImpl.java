package com.innovenso.townplan.domain;

import com.innovenso.townplan.api.value.Element;
import com.innovenso.townplan.api.value.Relationship;
import com.innovenso.townplan.api.value.RelationshipType;
import com.innovenso.townplan.api.value.business.BusinessActor;
import com.innovenso.townplan.api.value.business.Enterprise;
import com.innovenso.townplan.api.value.it.*;
import com.innovenso.townplan.api.value.it.decision.Decision;
import com.innovenso.townplan.api.value.it.decision.DecisionContext;
import com.innovenso.townplan.api.value.it.decision.DecisionOption;
import com.innovenso.townplan.api.value.it.decision.DecisionStatus;
import com.innovenso.townplan.api.value.it.principle.Principle;
import com.innovenso.townplan.api.value.strategy.BusinessCapability;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@EqualsAndHashCode(callSuper = true)
@SuperBuilder
@Data
public class DecisionImpl extends AbstractElementImpl implements Decision {
	private Enterprise enterprise;
	private String outcome;
	private DecisionStatus status;
	private final Set<Class<? extends Element>> acceptedIncomingDependencies = Set.of(BusinessActor.class,
			Principle.class, ItProjectMilestone.class, ItProject.class);
	private final Set<Class<? extends Element>> acceptedOutgoingDependencies = Set.of(BusinessCapability.class,
			ArchitectureBuildingBlock.class, ItSystem.class, ItContainer.class, ItSystemIntegration.class,
			ItProjectMilestone.class, ItProject.class);
	private final Set<Class<? extends Element>> acceptedChildren = Set.of(DecisionContext.class, DecisionOption.class);

	@Override
	public List<DecisionContext> getContexts() {
		return getChildren().stream().filter(DecisionContext.class::isInstance).map(DecisionContext.class::cast)
				.sorted(Comparator.comparing(DecisionContext::getSortKey)).collect(Collectors.toList());
	}

	@Override
	public List<DecisionOption> getOptions() {
		return getChildren().stream().filter(DecisionOption.class::isInstance).map(DecisionOption.class::cast)
				.sorted(Comparator.comparing(DecisionOption::getSortKey)).collect(Collectors.toList());
	}

	@Override
	public Set<BusinessCapability> getImpactedBusinessCapabilities() {
		return getImpactedElements(BusinessCapability.class);
	}

	@Override
	public Set<BusinessCapability> getAddedBusinessCapabilities() {
		return getImpactedElements(BusinessCapability.class, RelationshipType.IMPACT_CREATE);
	}

	@Override
	public Set<BusinessCapability> getChangedBusinessCapabilities() {
		return getImpactedElements(BusinessCapability.class, RelationshipType.IMPACT_CHANGE);
	}

	@Override
	public Set<BusinessCapability> getRemovedBusinessCapabilities() {
		return getImpactedElements(BusinessCapability.class, RelationshipType.IMPACT_REMOVE);
	}

	@Override
	public Set<ArchitectureBuildingBlock> getImpactedBuildingBlocks() {
		return getImpactedElements(ArchitectureBuildingBlock.class);
	}

	@Override
	public Set<ArchitectureBuildingBlock> getAddedBuildingBlocks() {
		return getImpactedElements(ArchitectureBuildingBlock.class, RelationshipType.IMPACT_CREATE);
	}

	@Override
	public Set<ArchitectureBuildingBlock> getChangedBuildingBlocks() {
		return getImpactedElements(ArchitectureBuildingBlock.class, RelationshipType.IMPACT_CHANGE);
	}

	@Override
	public Set<ArchitectureBuildingBlock> getRemovedBuildingBlocks() {
		return getImpactedElements(ArchitectureBuildingBlock.class, RelationshipType.IMPACT_REMOVE);
	}

	@Override
	public Set<DataEntity> getImpactedDataEntities() {
		return getImpactedElements(DataEntity.class);
	}

	@Override
	public Set<DataEntity> getAddedDataEntities() {
		return getImpactedElements(DataEntity.class, RelationshipType.IMPACT_CREATE);
	}

	@Override
	public Set<DataEntity> getChangedDataEntities() {
		return getImpactedElements(DataEntity.class, RelationshipType.IMPACT_CHANGE);
	}

	@Override
	public Set<DataEntity> getRemovedDataEntities() {
		return getImpactedElements(DataEntity.class, RelationshipType.IMPACT_REMOVE);
	}

	@Override
	public Set<ItSystem> getImpactedItSystems() {
		return getImpactedElements(ItSystem.class);
	}

	@Override
	public Set<ItSystem> getAddedItSystems() {
		return getImpactedElements(ItSystem.class, RelationshipType.IMPACT_CREATE);
	}

	@Override
	public Set<ItSystem> getChangedItSystems() {
		return getImpactedElements(ItSystem.class, RelationshipType.IMPACT_CHANGE);
	}

	@Override
	public Set<ItSystem> getRemovedItSystems() {
		return getImpactedElements(ItSystem.class, RelationshipType.IMPACT_REMOVE);
	}

	@Override
	public Set<ItSystemIntegration> getImpactedItSystemIntegrations() {
		return getImpactedElements(ItSystemIntegration.class);
	}

	@Override
	public Set<ItSystemIntegration> getAddedItSystemIntegrations() {
		return getImpactedElements(ItSystemIntegration.class, RelationshipType.IMPACT_CREATE);
	}

	@Override
	public Set<ItSystemIntegration> getChangedItSystemIntegrations() {
		return getImpactedElements(ItSystemIntegration.class, RelationshipType.IMPACT_CHANGE);
	}

	@Override
	public Set<ItSystemIntegration> getRemovedItSystemIntegrations() {
		return getImpactedElements(ItSystemIntegration.class, RelationshipType.IMPACT_REMOVE);
	}

	@Override
	public Set<ItContainer> getImpactedItContainers() {
		return getImpactedElements(ItContainer.class);
	}

	@Override
	public Set<ItContainer> getAddedItContainers() {
		return getImpactedElements(ItContainer.class, RelationshipType.IMPACT_CREATE);
	}

	@Override
	public Set<ItContainer> getChangedItContainers() {
		return getImpactedElements(ItContainer.class, RelationshipType.IMPACT_CHANGE);
	}

	@Override
	public Set<ItContainer> getRemovedItContainers() {
		return getImpactedElements(ItContainer.class, RelationshipType.IMPACT_REMOVE);
	}

	private <T extends Element> Set<T> getImpactedElements(Class<T> elementClass, RelationshipType... impactTypes) {
		final Set<RelationshipType> impactsToCheck = impactTypes == null || impactTypes.length == 0
				? Set.of(RelationshipType.IMPACT_CHANGE, RelationshipType.IMPACT_CREATE, RelationshipType.IMPACT_REMOVE,
						RelationshipType.IMPACT_REMAINS_UNCHANGED)
				: Set.of(impactTypes);
		return getOutgoingRelationships(elementClass).stream()
				.filter(relationship -> impactsToCheck.stream()
						.anyMatch(relationshipType -> relationship.getRelationshipType().equals(relationshipType)))
				.map(Relationship::getTarget).filter(elementClass::isInstance).map(elementClass::cast)
				.collect(Collectors.toSet());
	}
}
