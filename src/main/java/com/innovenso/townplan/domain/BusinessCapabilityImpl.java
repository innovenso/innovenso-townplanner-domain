package com.innovenso.townplan.domain;

import com.innovenso.townplan.api.value.Element;
import com.innovenso.townplan.api.value.business.Enterprise;
import com.innovenso.townplan.api.value.it.ArchitectureBuildingBlock;
import com.innovenso.townplan.api.value.it.DataEntity;
import com.innovenso.townplan.api.value.it.ItProjectMilestone;
import com.innovenso.townplan.api.value.it.decision.Decision;
import com.innovenso.townplan.api.value.it.decision.DecisionOption;
import com.innovenso.townplan.api.value.strategy.BusinessCapability;
import lombok.experimental.SuperBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SuperBuilder
public class BusinessCapabilityImpl extends AbstractElementImpl implements BusinessCapability {
	@Override
	public Enterprise getEnterprise() {
		return enterprise;
	}

	@Override
	public Set<BusinessCapability> getDescendantCapabilities() {
		return getDescendantStream().collect(Collectors.toSet());
	}

	@Override
	public Stream<BusinessCapability> getDescendantStream() {
		if (!hasChildren())
			return Stream.of();
		else
			return Stream.concat(getChildCapabilities().stream(),
					getChildCapabilities().stream().flatMap(BusinessCapability::getDescendantStream));
	}

	public void setEnterprise(Enterprise enterprise) {
		this.enterprise = enterprise;
	}

	private Enterprise enterprise;

	@Override
	public String getSortKey() {
		return sortKey;
	}

	public void setSortKey(String sortKey) {
		this.sortKey = sortKey;
	}

	private String sortKey;

	@Override
	public Set<Class<? extends Element>> getAcceptedIncomingDependencies() {
		return Set.of(ArchitectureBuildingBlock.class, ItProjectMilestone.class, Decision.class, DecisionOption.class);
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedOutgoingDependencies() {
		return Set.of();
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedChildren() {
		return Set.of(BusinessCapability.class, DataEntity.class);
	}

	@Override
	public String getCategory() {
		return "Business Capability";
	}

	@Override
	public int getLevel() {
		if (getParent().isPresent() && getParent().get() instanceof BusinessCapability)
			return (((BusinessCapability) getParent().get()).getLevel()) + 1;
		else
			return 0;
	}

	@Override
	public String getType() {
		return "Business Capability Level " + getLevel();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("enterprise", enterprise).append("level", getLevel())
				.appendSuper(super.toString()).toString();
	}
}
