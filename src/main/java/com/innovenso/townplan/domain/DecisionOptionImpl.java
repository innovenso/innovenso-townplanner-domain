package com.innovenso.townplan.domain;

import com.innovenso.townplan.api.value.Element;
import com.innovenso.townplan.api.value.aspects.*;
import com.innovenso.townplan.api.value.business.BusinessActor;
import com.innovenso.townplan.api.value.it.ArchitectureBuildingBlock;
import com.innovenso.townplan.api.value.it.ItContainer;
import com.innovenso.townplan.api.value.it.ItSystem;
import com.innovenso.townplan.api.value.it.ItSystemIntegration;
import com.innovenso.townplan.api.value.it.decision.Decision;
import com.innovenso.townplan.api.value.it.decision.DecisionOption;
import com.innovenso.townplan.api.value.it.decision.DecisionOptionVerdict;
import com.innovenso.townplan.api.value.it.principle.Principle;
import com.innovenso.townplan.api.value.strategy.BusinessCapability;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;

import java.util.Optional;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
public class DecisionOptionImpl extends AbstractElementImpl implements DecisionOption {
	private String sortKey;
	private final Set<Class<? extends Element>> acceptedIncomingDependencies = Set.of(BusinessActor.class,
			Principle.class);
	private final Set<Class<? extends Element>> acceptedOutgoingDependencies = Set.of(BusinessCapability.class,
			ArchitectureBuildingBlock.class, ItSystem.class, ItContainer.class, ItSystemIntegration.class);
	private final Set<Class<? extends Element>> acceptedChildren = Set.of();
	@NonNull
	private DecisionOptionVerdict verdict;

	public DecisionOptionVerdict getVerdict() {
		if (failsMustHaveRequirements())
			return DecisionOptionVerdict.REJECTED;
		else
			return this.verdict;
	}

	public boolean failsMustHaveRequirements() {
		return getFunctionalRequirementScores().stream()
				.anyMatch(score -> score.getFunctionalRequirement().getWeight().equals(RequirementWeight.MUST_HAVE)
						&& score.getWeight().equals(ScoreWeight.DOES_NOT_MEET_EXPECTATIONS))
				|| getQualityAttributeRequirementScores().stream().anyMatch(
						score -> score.getQualityAttributeRequirement().getWeight().equals(RequirementWeight.MUST_HAVE)
								&& score.getWeight().equals(ScoreWeight.DOES_NOT_MEET_EXPECTATIONS))
				|| getConstraintScores().stream()
						.anyMatch(score -> score.getConstraint().getWeight().equals(RequirementWeight.MUST_HAVE)
								&& score.getWeight().equals(ScoreWeight.DOES_NOT_MEET_EXPECTATIONS));
	}

	@Override
	public Decision getDecision() {
		return getParent().filter(Decision.class::isInstance).map(Decision.class::cast).orElse(null);
	}

	public DecisionImpl getDecisionImpl() {
		return getParent().filter(DecisionImpl.class::isInstance).map(DecisionImpl.class::cast).orElse(null);
	}

	@Override
	public Set<FunctionalRequirement> getFunctionalRequirements() {
		return getDecision().getFunctionalRequirements();
	}

	@Override
	public Set<QualityAttributeRequirement> getQualityAttributes() {
		return getDecision().getQualityAttributes();
	}

	@Override
	public Set<Constraint> getConstraints() {
		return getDecision().getConstraints();
	}

	@Override
	public Optional<ConstraintImpl> getConstraint(String key) {
		return getDecisionImpl().getConstraint(key);
	}

	@Override
	public Optional<FunctionalRequirementImpl> getFunctionalRequirement(String key) {
		return getDecisionImpl().getFunctionalRequirement(key);
	}

	@Override
	public Optional<QualityAttributeRequirementImpl> getQualityAttributeRequirement(String key) {
		return getDecisionImpl().getQualityAttributeRequirement(key);
	}
}
