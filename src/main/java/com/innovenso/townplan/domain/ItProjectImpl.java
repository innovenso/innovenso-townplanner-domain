package com.innovenso.townplan.domain;

import com.innovenso.townplan.api.value.Concept;
import com.innovenso.townplan.api.value.Element;
import com.innovenso.townplan.api.value.Relationship;
import com.innovenso.townplan.api.value.RelationshipType;
import com.innovenso.townplan.api.value.business.BusinessActor;
import com.innovenso.townplan.api.value.business.Enterprise;
import com.innovenso.townplan.api.value.it.*;
import com.innovenso.townplan.api.value.strategy.BusinessCapability;
import lombok.experimental.SuperBuilder;

import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@SuperBuilder
public class ItProjectImpl extends AbstractElementImpl implements ItProject {

	@Override
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public List<ItProjectMilestone> getMilestones() {
		return getChildren().stream().filter(ItProjectMilestone.class::isInstance).map(ItProjectMilestone.class::cast)
				.sorted(Comparator.comparing(Concept::getSortKey)).collect(Collectors.toList());
	}

	@Override
	public Set<BusinessCapability> getImpactedBusinessCapabilities() {
		return getMilestones().stream().flatMap(milestone -> milestone.getImpactedBusinessCapabilities().stream())
				.collect(Collectors.toSet());
	}

	@Override
	public Set<BusinessCapability> getAddedBusinessCapabilities() {
		return getMilestones().stream().flatMap(milestone -> milestone.getAddedBusinessCapabilities().stream())
				.collect(Collectors.toSet());
	}

	@Override
	public Set<BusinessCapability> getChangedBusinessCapabilities() {
		return getMilestones().stream().flatMap(milestone -> milestone.getChangedBusinessCapabilities().stream())
				.collect(Collectors.toSet());
	}

	@Override
	public Set<BusinessCapability> getRemovedBusinessCapabilities() {
		return getMilestones().stream().flatMap(milestone -> milestone.getRemovedBusinessCapabilities().stream())
				.collect(Collectors.toSet());
	}

	@Override
	public Set<ItSystem> getImpactedItSystems() {
		return getMilestones().stream().flatMap(milestone -> milestone.getImpactedItSystems().stream())
				.collect(Collectors.toSet());
	}

	@Override
	public Set<ItSystem> getAddedItSystems() {
		return getMilestones().stream().flatMap(milestone -> milestone.getAddedItSystems().stream())
				.collect(Collectors.toSet());
	}

	@Override
	public Set<ItSystem> getChangedItSystems() {
		return getMilestones().stream().flatMap(milestone -> milestone.getChangedItSystems().stream())
				.collect(Collectors.toSet());
	}

	@Override
	public Set<ItSystem> getRemovedItSystems() {
		return getMilestones().stream().flatMap(milestone -> milestone.getRemovedItSystems().stream())
				.collect(Collectors.toSet());
	}

	@Override
	public Set<ItSystemIntegration> getImpactedItSystemIntegrations() {
		return getMilestones().stream().flatMap(milestone -> milestone.getImpactedItSystemIntegrations().stream())
				.collect(Collectors.toSet());
	}

	@Override
	public Set<ItSystemIntegration> getAddedItSystemIntegrations() {
		return getMilestones().stream().flatMap(milestone -> milestone.getAddedItSystemIntegrations().stream())
				.collect(Collectors.toSet());
	}

	@Override
	public Set<ItSystemIntegration> getChangedItSystemIntegrations() {
		return getMilestones().stream().flatMap(milestone -> milestone.getChangedItSystemIntegrations().stream())
				.collect(Collectors.toSet());
	}

	@Override
	public Set<ItSystemIntegration> getRemovedItSystemIntegrations() {
		return getMilestones().stream().flatMap(milestone -> milestone.getRemovedItSystemIntegrations().stream())
				.collect(Collectors.toSet());
	}

	@Override
	public Set<ItContainer> getImpactedItContainers() {
		return getMilestones().stream().flatMap(milestone -> milestone.getImpactedItContainers().stream())
				.collect(Collectors.toSet());
	}

	@Override
	public Set<ItContainer> getAddedItContainers() {
		return getMilestones().stream().flatMap(milestone -> milestone.getAddedItContainers().stream())
				.collect(Collectors.toSet());
	}

	@Override
	public Set<ItContainer> getChangedItContainers() {
		return getMilestones().stream().flatMap(milestone -> milestone.getChangedItContainers().stream())
				.collect(Collectors.toSet());
	}

	@Override
	public Set<ItContainer> getRemovedItContainers() {
		return getMilestones().stream().flatMap(milestone -> milestone.getRemovedItContainers().stream())
				.collect(Collectors.toSet());
	}

	@Override
	public Set<ArchitectureBuildingBlock> getImpactedBuildingBlocks() {
		return getMilestones().stream().flatMap(milestone -> milestone.getImpactedBuildingBlocks().stream())
				.collect(Collectors.toSet());
	}

	@Override
	public Set<ArchitectureBuildingBlock> getAddedBuildingBlocks() {
		return getMilestones().stream().flatMap(milestone -> milestone.getAddedBuildingBlocks().stream())
				.collect(Collectors.toSet());
	}

	@Override
	public Set<ArchitectureBuildingBlock> getChangedBuildingBlocks() {
		return getMilestones().stream().flatMap(milestone -> milestone.getChangedBuildingBlocks().stream())
				.collect(Collectors.toSet());
	}

	@Override
	public Set<ArchitectureBuildingBlock> getRemovedBuildingBlocks() {
		return getMilestones().stream().flatMap(milestone -> milestone.getRemovedBuildingBlocks().stream())
				.collect(Collectors.toSet());
	}

	@Override
	public Set<DataEntity> getImpactedDataEntities() {
		return getMilestones().stream().flatMap(milestone -> milestone.getImpactedDataEntities().stream())
				.collect(Collectors.toSet());
	}

	@Override
	public Set<DataEntity> getAddedDataEntities() {
		return getMilestones().stream().flatMap(milestone -> milestone.getAddedDataEntities().stream())
				.collect(Collectors.toSet());
	}

	@Override
	public Set<DataEntity> getChangedDataEntities() {
		return getMilestones().stream().flatMap(milestone -> milestone.getChangedDataEntities().stream())
				.collect(Collectors.toSet());
	}

	@Override
	public Set<DataEntity> getRemovedDataEntities() {
		return getMilestones().stream().flatMap(milestone -> milestone.getRemovedDataEntities().stream())
				.collect(Collectors.toSet());
	}

	private String type;

	@Override
	public Enterprise getEnterprise() {
		return getParent().filter(Enterprise.class::isInstance).map(Enterprise.class::cast).orElse(null);
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedIncomingDependencies() {
		return Set.of(BusinessActor.class);
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedOutgoingDependencies() {
		return Set.of();
	}

	@Override
	public Set<Relationship> getStakeholderRelationships() {
		return getIncomingRelationships(BusinessActor.class).stream()
				.filter(relationship -> relationship.getRelationshipType().equals(RelationshipType.STAKEHOLDER))
				.collect(Collectors.toSet());
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedChildren() {
		return Set.of(ItProjectMilestone.class);
	}
}
