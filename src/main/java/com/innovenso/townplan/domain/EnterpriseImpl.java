package com.innovenso.townplan.domain;

import com.innovenso.townplan.api.value.Element;
import com.innovenso.townplan.api.value.business.BusinessActor;
import com.innovenso.townplan.api.value.business.Enterprise;
import com.innovenso.townplan.api.value.it.DataEntity;
import com.innovenso.townplan.api.value.it.ItProject;
import com.innovenso.townplan.api.value.it.decision.Decision;
import com.innovenso.townplan.api.value.it.principle.Principle;
import com.innovenso.townplan.api.value.strategy.BusinessCapability;
import lombok.experimental.SuperBuilder;

import java.util.Set;

@SuperBuilder
public class EnterpriseImpl extends AbstractElementImpl implements Enterprise {
	@Override
	public Set<Class<? extends Element>> getAcceptedIncomingDependencies() {
		return Set.of();
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedOutgoingDependencies() {
		return Set.of();
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedChildren() {
		return Set.of(BusinessCapability.class, BusinessActor.class, DataEntity.class, ItProject.class, Principle.class,
				Decision.class);
	}
}
