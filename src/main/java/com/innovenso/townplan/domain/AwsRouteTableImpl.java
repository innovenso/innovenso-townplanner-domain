package com.innovenso.townplan.domain;

import com.innovenso.townplan.api.value.Element;
import com.innovenso.townplan.api.value.deployment.aws.AwsInstance;
import com.innovenso.townplan.api.value.deployment.aws.AwsRouteTable;
import com.innovenso.townplan.api.value.deployment.aws.AwsSubnet;
import com.innovenso.townplan.api.value.deployment.aws.AwsVpc;
import lombok.experimental.SuperBuilder;
import lombok.extern.log4j.Log4j2;

import java.util.Set;

@SuperBuilder
@Log4j2
public class AwsRouteTableImpl extends AbstractAwsElementImpl implements AwsRouteTable {
	private AwsVpc vpc;

	@Override
	public AwsVpc getVpc() {
		return vpc;
	}

	public void setVpc(AwsVpc vpc) {
		this.vpc = vpc;
	}

	@Override
	public Set<AwsSubnet> getSubnets() {
		return getChildren(AwsSubnet.class);
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedIncomingDependencies() {
		return Set.of(AwsSubnet.class, AwsInstance.class);
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedOutgoingDependencies() {
		return Set.of(AwsSubnet.class, AwsInstance.class);
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedChildren() {
		return Set.of(AwsSubnetImpl.class);
	}

	@Override
	public String getType() {
		return "Route Table";
	}

	@Override
	public String getCategory() {
		return "AWS";
	}
}
