package com.innovenso.townplan.domain;

import com.innovenso.townplan.api.value.Element;
import com.innovenso.townplan.api.value.business.BusinessActor;
import com.innovenso.townplan.api.value.business.Enterprise;
import com.innovenso.townplan.api.value.enums.ActorType;
import com.innovenso.townplan.api.value.it.ItContainer;
import com.innovenso.townplan.api.value.it.ItProject;
import com.innovenso.townplan.api.value.it.ItProjectMilestone;
import com.innovenso.townplan.api.value.it.ItSystem;
import com.innovenso.townplan.api.value.it.decision.Decision;
import com.innovenso.townplan.api.value.it.decision.DecisionOption;
import lombok.Getter;
import lombok.Setter;
import lombok.Value;
import lombok.experimental.SuperBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.Set;

@SuperBuilder
public class BusinessActorImpl extends AbstractElementImpl implements BusinessActor {

	@Getter
	@Setter
	private Enterprise enterprise;

	@Getter
	@Setter
	private ActorType actorType;

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("type", actorType).appendSuper(super.toString()).toString();
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedIncomingDependencies() {
		return Set.of(BusinessActor.class);
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedOutgoingDependencies() {
		return Set.of(ItSystem.class, ItContainer.class, BusinessActor.class, ItProject.class, ItProjectMilestone.class,
				ArchitectureBuildingBlockImpl.class, Decision.class, DecisionOption.class);
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedChildren() {
		return Set.of();
	}
}
