package com.innovenso.townplan.domain;

import com.innovenso.townplan.api.value.aspects.Lifecycle;
import com.innovenso.townplan.api.value.aspects.LifecyleType;
import lombok.Builder;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;

@Builder
public class LifecycleImpl implements Lifecycle {
	private LifecyleType type;
	private String description;

	@Override
	public String getKey() {
		final String keyString = type.label + "_" + description;
		return DigestUtils.sha1Hex(keyString.getBytes());
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("type", type).append("description", description).toString();
	}

	@Override
	public LifecyleType getType() {
		return type;
	}

	public void setType(LifecyleType type) {
		this.type = type;
	}

	@Override
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
