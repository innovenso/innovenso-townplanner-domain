package com.innovenso.townplan.domain;

import com.innovenso.townplan.api.value.aspects.SecurityImpact;
import com.innovenso.townplan.api.value.aspects.SecurityImpactLevel;
import com.innovenso.townplan.api.value.aspects.SecurityImpactType;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class SecurityImpactImpl implements SecurityImpact {
	private String key;
	private SecurityImpactType impactType;
	private String description;
	private SecurityImpactLevel impactLevel;
}
