package com.innovenso.townplan.domain;

import com.innovenso.townplan.api.value.aspects.QualityAttributeRequirement;
import com.innovenso.townplan.api.value.aspects.RequirementWeight;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class QualityAttributeRequirementImpl implements QualityAttributeRequirement {
	private String key;
	private String title;
	private String sourceOfStimulus;
	private String stimulus;
	private String environment;
	private String response;
	private String sortKey;
	private String responseMeasure;
	@Builder.Default
	private RequirementWeight weight = RequirementWeight.SHOULD_HAVE;

	public RequirementWeight getWeight() {
		return weight == null ? RequirementWeight.SHOULD_HAVE : weight;
	}
}
