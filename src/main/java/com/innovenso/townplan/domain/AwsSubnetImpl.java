package com.innovenso.townplan.domain;

import com.innovenso.townplan.api.value.Element;
import com.innovenso.townplan.api.value.deployment.aws.*;
import lombok.experimental.SuperBuilder;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.Set;

@SuperBuilder
@Log4j2
public class AwsSubnetImpl extends AbstractAwsElementImpl implements AwsSubnet {
	private AwsVpc vpc;
	private AwsAvailabilityZone availabilityZone;
	private String subnetMask;
	private boolean publicSubnet;

	@Override
	public boolean isPublicSubnet() {
		return publicSubnet;
	}

	public void setPublicSubnet(boolean publicSubnet) {
		this.publicSubnet = publicSubnet;
	}

	@Override
	public AwsVpc getVpc() {
		return vpc;
	}

	public void setVpc(AwsVpc vpc) {
		this.vpc = vpc;
	}

	public void setSubnetMask(String subnetMask) {
		this.subnetMask = subnetMask;
	}

	@Override
	public AwsAvailabilityZone getAvailabilityZone() {
		return availabilityZone;
	}

	public void setAvailabilityZone(AwsAvailabilityZone availabilityZone) {
		this.availabilityZone = availabilityZone;
	}

	@Override
	public String getSubnetMask() {
		return subnetMask;
	}

	@Override
	public Set<AwsInstance> getInstances() {
		return getChildren(AwsInstance.class);
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedIncomingDependencies() {
		return Set.of(AwsInstance.class, AwsRouteTable.class);
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedOutgoingDependencies() {
		return Set.of(AwsInstance.class, AwsRouteTable.class);
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedChildren() {
		return Set.of(AwsInstance.class);
	}

	@Override
	public String getType() {
		return "Subnet";
	}

	@Override
	public String getCategory() {
		return "AWS";
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).appendSuper(super.toString()).append("vpc", vpc)
				.append("availabilityZone", availabilityZone).append("subnetMask", subnetMask)
				.append("publicSubnet", publicSubnet).toString();
	}
}
