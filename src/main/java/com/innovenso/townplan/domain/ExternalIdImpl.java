package com.innovenso.townplan.domain;

import com.innovenso.townplan.api.value.aspects.ExternalId;
import com.innovenso.townplan.api.value.aspects.ExternalIdType;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class ExternalIdImpl implements ExternalId {
	String key;
	String value;
	ExternalIdType externalIdType;
}
