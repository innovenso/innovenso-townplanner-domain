package com.innovenso.townplan.domain;

import com.innovenso.townplan.api.value.Element;
import com.innovenso.townplan.api.value.deployment.aws.AwsAvailabilityZone;
import com.innovenso.townplan.api.value.deployment.aws.AwsRegion;
import lombok.experimental.SuperBuilder;
import lombok.extern.log4j.Log4j2;

import java.util.Set;

@SuperBuilder
@Log4j2
public class AwsAvailabilityZoneImpl extends AbstractAwsElementImpl implements AwsAvailabilityZone {
	private AwsRegion region;

	@Override
	public AwsRegion getRegion() {
		return region;
	}

	public void setRegion(AwsRegion region) {
		this.region = region;
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedIncomingDependencies() {
		return Set.of();
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedOutgoingDependencies() {
		return Set.of();
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedChildren() {
		return Set.of();
	}

	@Override
	public String getType() {
		return "Availability Zone";
	}

	@Override
	public String getCategory() {
		return "AWS";
	}
}
