package com.innovenso.townplan.domain;

import com.innovenso.townplan.api.value.aspects.Attachment;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Builder
@Value
public class AttachmentImpl implements Attachment {
	@NonNull
	String key;
	@NonNull
	String cdnContentUrl;
	@NonNull
	String title;
	String description;
}
