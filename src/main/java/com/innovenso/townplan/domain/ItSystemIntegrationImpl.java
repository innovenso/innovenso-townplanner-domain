package com.innovenso.townplan.domain;

import com.innovenso.townplan.api.value.Element;
import com.innovenso.townplan.api.value.enums.Criticality;
import com.innovenso.townplan.api.value.it.*;
import com.innovenso.townplan.api.value.it.decision.Decision;
import com.innovenso.townplan.api.value.it.decision.DecisionOption;
import com.innovenso.townplan.api.value.view.Step;
import lombok.Builder;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@SuperBuilder
public class ItSystemIntegrationImpl extends AbstractElementImpl implements ItSystemIntegration {
	private ItSystemImpl sourceSystem;
	private ItSystemImpl targetSystem;

	public void setVolume(String volume) {
		this.volume = volume;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

	public void setCriticality(Criticality criticality) {
		this.criticality = criticality;
	}

	private String volume;
	private String frequency;
	private Criticality criticality;
	private String criticalityDescription;

	public void setCriticalityDescription(String criticalityDescription) {
		this.criticalityDescription = criticalityDescription;
	}

	public void setResilience(String resilience) {
		this.resilience = resilience;
	}

	private String resilience;

	@Builder.Default
	private final List<StepImpl> steps = new ArrayList<>();

	@Override
	public String getCategory() {
		return "System Integration";
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedIncomingDependencies() {
		return Set.of(ItSystem.class, DataEntity.class, ItProject.class, ItProjectMilestone.class, Decision.class,
				DecisionOption.class);
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedOutgoingDependencies() {
		return Set.of();
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedChildren() {
		return Set.of();
	}

	@Override
	public ItSystem getSourceSystem() {
		return sourceSystem;
	}

	@Override
	public ItSystem getTargetSystem() {
		return targetSystem;
	}

	@Override
	public Optional<String> getVolume() {
		return Optional.ofNullable(volume);
	}

	@Override
	public Optional<String> getFrequency() {
		return Optional.ofNullable(frequency);
	}

	@Override
	public Optional<Criticality> getCriticality() {
		return Optional.ofNullable(criticality);
	}

	@Override
	public Optional<String> getCriticalityDescription() {
		return Optional.ofNullable(criticalityDescription);
	}

	@Override
	public Optional<String> getResilience() {
		return Optional.ofNullable(resilience);
	}

	@Override
	public String getCriticalityLabel() {
		return getCriticality().orElse(Criticality.UNKNOWN).getLabel();
	}

	@Override
	public List<Step> getSteps() {
		return steps.stream().map(step -> (Step) step).collect(Collectors.toList());
	}

	public void setSourceSystem(@NonNull final ItSystemImpl sourceSystem) {
		this.sourceSystem = sourceSystem;
	}

	public void setTargetSystem(@NonNull final ItSystemImpl targetSystem) {
		this.targetSystem = targetSystem;
	}

	void addStep(@NonNull final StepImpl step) {
		this.steps.add(step);
	}

	void clearSteps() {
		this.steps.clear();
	}
}
