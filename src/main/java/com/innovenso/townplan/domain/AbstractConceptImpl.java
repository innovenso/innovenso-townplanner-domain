package com.innovenso.townplan.domain;

import com.innovenso.townplan.api.KeyPointInTime;
import com.innovenso.townplan.api.value.Concept;
import com.innovenso.townplan.api.value.aspects.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.log4j.Log4j2;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Log4j2
@SuperBuilder
public abstract class AbstractConceptImpl extends AbstractModelComponentImpl implements Concept, Comparable<Concept> {
	@Builder.Default
	private Map<String, ModelAspect> aspects = new HashMap<>();
	@Getter
	@Setter
	private String title;
	@Setter
	private Lifecycle lifecycle;
	@Setter
	private ArchitectureVerdict architectureVerdict;
	@Getter
	@Setter
	private String description;

	@Override
	public Set<SWOT> getSwots() {
		return getAspects(SWOT.class);
	}

	private <C extends ModelAspect> Set<C> getAspects(@NonNull final Class<C> aspectClass) {
		return aspects.values().stream().filter(aspectClass::isInstance).map(aspectClass::cast)
				.collect(Collectors.toSet());
	}

	@Override
	public Set<Documentation> getDocumentations() {
		return getAspects(Documentation.class);
	}

	@Override
	public Set<SecurityConcern> getSecurityConcerns() {
		return getAspects(SecurityConcern.class);
	}

	@Override
	public Set<Attachment> getAttachments() {
		return getAspects(Attachment.class);
	}

	@Override
	public Set<ExternalId> getExternalIds() {
		return getAspects(ExternalId.class);
	}

	public void addAspect(@NonNull final ModelAspect aspect) {
		log.debug("adding aspect {} to concept {}", aspect, getTitle());
		this.aspects.put(aspect.getKey(), aspect);
	}

	@Override
	public Set<SecurityImpact> getSecurityImpacts() {
		return getAspects(SecurityImpact.class);
	}

	@Override
	public Set<FunctionalRequirement> getFunctionalRequirements() {
		return getAspects(FunctionalRequirement.class);
	}

	@Override
	public Set<Cost> getCosts() {
		return getAspects(Cost.class);
	}

	@Override
	public Set<FatherTime> getFatherTimes() {
		return getAspects(FatherTime.class);
	}

	@Override
	public Set<QualityAttributeRequirement> getQualityAttributes() {
		return getAspects(QualityAttributeRequirement.class);
	}

	@Override
	public Set<Constraint> getConstraints() {
		return getAspects(Constraint.class);
	}

	public Optional<ConstraintImpl> getConstraint(String key) {
		return getAspect(key).filter(ConstraintImpl.class::isInstance).map(ConstraintImpl.class::cast);
	}

	public Optional<FunctionalRequirementImpl> getFunctionalRequirement(String key) {
		return getAspect(key).filter(FunctionalRequirementImpl.class::isInstance)
				.map(FunctionalRequirementImpl.class::cast);
	}

	public Optional<QualityAttributeRequirementImpl> getQualityAttributeRequirement(String key) {
		return getAspect(key).filter(QualityAttributeRequirementImpl.class::isInstance)
				.map(QualityAttributeRequirementImpl.class::cast);
	}

	public boolean hasScore(String aspectKey) {
		return getConstraintScores().stream().anyMatch(score -> score.getConstraint().getKey().equals(aspectKey))
				|| getFunctionalRequirementScores().stream()
						.anyMatch(score -> score.getFunctionalRequirement().getKey().equals(aspectKey))
				|| getQualityAttributeRequirementScores().stream()
						.anyMatch(score -> score.getQualityAttributeRequirement().getKey().equals(aspectKey));
	}

	@Override
	public Set<FunctionalRequirementScore> getFunctionalRequirementScores() {
		return getAspects(FunctionalRequirementScore.class);
	}

	@Override
	public Set<QualityAttributeRequirementScore> getQualityAttributeRequirementScores() {
		return getAspects(QualityAttributeRequirementScore.class);
	}

	@Override
	public Set<ConstraintScore> getConstraintScores() {
		return getAspects(ConstraintScore.class);
	}

	@Override
	public Set<Documentation> getDocumentations(@NonNull final Optional<DocumentationType> documentationType) {
		return getAspects(Documentation.class).stream().filter(
				documentation -> documentationType.map(dt -> Objects.equals(dt, documentation.getType())).orElse(true))
				.collect(Collectors.toSet());
	}

	@Override
	public Set<SWOT> getSwots(@NonNull final Optional<SWOTType> swotType) {
		return getAspects(SWOT.class).stream()
				.filter(swot -> swotType.map(dt -> Objects.equals(dt, swot.getType())).orElse(true))
				.collect(Collectors.toSet());
	}

	@Override
	public Set<SWOT> getSwots(@NonNull final SWOTType swotType) {
		return getAspects(SWOT.class).stream().filter(swot -> swot.getType().equals(swotType))
				.collect(Collectors.toSet());
	}

	@Override
	public List<SWOTType> getSwotTypes() {
		return SWOTType.getSwotTypes();
	}

	@Override
	public Lifecycle getLifecycle() {
		return getLifecycle(KeyPointInTimeImpl.builder().date(LocalDate.now()).build());
	}

	@Override
	public Lifecycle getLifecycle(KeyPointInTime keyPointInTime) {
		return LifecycleImpl.builder().type(getLifecycleType(keyPointInTime)).description("").build();
	}

	private LifecyleType getLifecycleType(KeyPointInTime keyPointInTime) {
		if (isPhasingOut(keyPointInTime))
			return LifecyleType.PHASEOUT;
		if (isDecommissioned(keyPointInTime))
			return LifecyleType.DECOMMISSIONED;
		if (isActive(keyPointInTime))
			return LifecyleType.ACTIVE;
		if (isPlanned(keyPointInTime))
			return LifecyleType.PLANNED;
		return LifecyleType.UNKNOWN;
	}

	@Override
	public boolean isPlanned(KeyPointInTime keyPointInTime) {
		return Stream.of(FatherTimeType.CONCEIVED, FatherTimeType.BIRTH, FatherTimeType.GROWING)
				.anyMatch(t -> hasBecomeBefore(keyPointInTime, t)) && !isActive(keyPointInTime)
				&& !isPhasingOut(keyPointInTime) && !isDecommissioned(keyPointInTime);
	}

	@Override
	public boolean isActive(KeyPointInTime keyPointInTime) {
		final boolean active = hasBecomeBefore(keyPointInTime, FatherTimeType.ACTIVE)
				&& !isDecommissioned(keyPointInTime) && !isPhasingOut(keyPointInTime);
		log.debug("is concept {} active at point in time {}?: {}", this.getKey(), keyPointInTime, active);
		return active;
	}

	@Override
	public boolean isPhasingOut(KeyPointInTime keyPointInTime) {
		final boolean phasingOut = hasBecomeBefore(keyPointInTime, FatherTimeType.RETIRED)
				&& !isDecommissioned(keyPointInTime);
		log.debug("is concept {} phasing out at point in time {}?: {}", this.getKey(), keyPointInTime, phasingOut);
		return phasingOut;
	}

	@Override
	public boolean isDecommissioned(KeyPointInTime keyPointInTime) {
		final boolean decom = hasBecomeBefore(keyPointInTime, FatherTimeType.DEATH);
		log.debug("is concept {} decommissioned at point in time {}?: {}", this.getKey(), keyPointInTime, decom);
		return decom;
	}

	public boolean isEmptyBefore(@NonNull final KeyPointInTime keyPointInTime) {
		return beforeOrEqual(keyPointInTime).findAny().isEmpty();
	}

	public boolean isEmptyAfter(@NonNull final KeyPointInTime keyPointInTime) {
		return afterOrEqual(keyPointInTime).findAny().isEmpty();
	}

	@Override
	public boolean isUnknown(KeyPointInTime keyPointInTime) {
		if (getFatherTimes().isEmpty())
			return true;
		return isEmptyBefore(keyPointInTime) && !hasBecomeAfter(keyPointInTime, FatherTimeType.ACTIVE)
				&& hasBecomeAfter(keyPointInTime, FatherTimeType.DEATH);
	}

	public boolean hasBecomeBefore(@NonNull final KeyPointInTime keyPointInTime,
			@NonNull final FatherTimeType fatherTimeType) {
		return beforeOrEqual(keyPointInTime)
				.anyMatch(fatherTime -> fatherTime.getFatherTimeType().equals(fatherTimeType));
	}

	public boolean hasBecomeAfter(@NonNull final KeyPointInTime keyPointInTime,
			@NonNull final FatherTimeType fatherTimeType) {
		return afterOrEqual(keyPointInTime)
				.anyMatch(fatherTime -> fatherTime.getFatherTimeType().equals(fatherTimeType));
	}

	public Stream<FatherTime> beforeOrEqual(KeyPointInTime keyPointInTime) {
		return fatherTimeStream().filter(fatherTime -> fatherTime.isBeforeOrEqual(keyPointInTime));
	}

	public Stream<FatherTime> afterOrEqual(KeyPointInTime keyPointInTime) {
		return fatherTimeStream().filter(fatherTime -> fatherTime.isAfterOrEqual(keyPointInTime));
	}

	public Stream<FatherTime> between(KeyPointInTime first, KeyPointInTime last) {
		return fatherTimeStream().filter(fatherTime -> fatherTime.isBetween(first, last));
	}

	public Stream<FatherTime> fatherTimeStream() {
		return getFatherTimes().stream().sorted(Comparator.comparing(FatherTime::getPointInTime));
	}

	@Override
	public ArchitectureVerdict getArchitectureVerdict() {
		return architectureVerdict == null
				? ArchitectureVerdictImpl.builder().key(UUID.randomUUID().toString())
						.verdictType(ArchitectureVerdictType.UNKNOWN).description("").build()
				: architectureVerdict;
	}

	@Override
	public String toString() {
		return new org.apache.commons.lang3.builder.ToStringBuilder(this).append("key", getKey()).append("title", title)
				.append("description", description).toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;

		if (o == null || getClass() != o.getClass())
			return false;

		AbstractConceptImpl that = (AbstractConceptImpl) o;

		return new org.apache.commons.lang3.builder.EqualsBuilder().append(getKey(), that.getKey()).isEquals();
	}

	@Override
	public int hashCode() {
		return new org.apache.commons.lang3.builder.HashCodeBuilder(17, 37).append(getKey()).toHashCode();
	}

	@Override
	public int compareTo(Concept other) {
		if (other == null)
			return 1;
		if (getKey() == null)
			return -1;
		return getKey().compareTo(other.getKey());
	}

	@Override
	public Optional<ModelAspect> getAspect(String aspectKey) {
		return Optional.ofNullable(aspects.get(aspectKey));
	}

	public void removeAspect(@NonNull final String aspectKey) {
		Optional<ModelAspect> modelAspectOptional = getAspect(aspectKey);
		aspects.remove(aspectKey);
		removeDependentAspects(modelAspectOptional);
	}

	private void removeDependentAspects(@NonNull final Optional<ModelAspect> aspectOptional) {
		aspectOptional.ifPresent(modelAspect -> {
			Set<ModelAspect> dependentAspects = aspects.values().stream().filter(
					aspect -> aspect.getParent().map(ModelAspect::getKey).orElse("").equals(modelAspect.getKey()))
					.collect(Collectors.toSet());
			dependentAspects.forEach(aspect -> aspects.remove(aspect.getKey()));
		});
	}

	@Override
	public boolean existsAt(@NonNull final KeyPointInTime keyPointInTime) {
		boolean noFatherTimeKnown = getFatherTimes().isEmpty();
		boolean diedBefore = getFatherTimes().stream()
				.filter(fatherTime -> fatherTime.getFatherTimeType().equals(FatherTimeType.DEATH))
				.anyMatch(fatherTime -> fatherTime.getPointInTime().isBefore(keyPointInTime.getDate()));
		boolean becomesAlive = getFatherTimes().stream()
				.anyMatch(fatherTime -> fatherTime.getFatherTimeType().equals(FatherTimeType.BIRTH));
		boolean onlyAliveAfter = getFatherTimes().stream()
				.filter(fatherTime -> fatherTime.getFatherTimeType().equals(FatherTimeType.BIRTH))
				.allMatch(fatherTime -> fatherTime.getPointInTime().isAfter(keyPointInTime.getDate()));

		if (noFatherTimeKnown)
			return true;
		if (diedBefore)
			return false;
		if (becomesAlive && onlyAliveAfter)
			return false;
		return true;
	}

	public List<Integer> getCostFiscalYears() {
		return getCosts().stream().map(Cost::getFiscalYear).distinct().sorted().collect(Collectors.toList());
	}

	public List<CostType> getCostTypes(@NonNull final Integer fiscalYear) {
		return getCosts().stream().filter(cost -> fiscalYear.equals(cost.getFiscalYear())).map(Cost::getCostType)
				.distinct().sorted(Comparator.comparing(CostType::getLabel)).collect(Collectors.toList());
	}

	public List<Cost> getCosts(@NonNull final Integer fiscalYear, @NonNull final CostType costType) {
		return getCosts().stream().filter(cost -> fiscalYear.equals(cost.getFiscalYear()))
				.filter(cost -> costType.equals(cost.getCostType())).sorted(Comparator.comparing(Cost::getCategory))
				.collect(Collectors.toList());
	}

	public Double getTotalCost(@NonNull final Integer fiscalYear, @NonNull final CostType costType) {
		return getCosts(fiscalYear, costType).stream().mapToDouble(Cost::getTotalCost).sum();
	}

	@Override
	public int getMaximumTotalScore() {
		return getMaximumTotalConstraintScore() + getMaximumTotalFunctionalScore()
				+ getMaximumTotalQualityAttributeScore();
	}

	@Override
	public int getTotalScore() {
		return getTotalConstraintScore() + getTotalFunctionalScore() + getTotalQualityAttributeScore();
	}

	@Override
	public int getMaximumTotalFunctionalScore() {
		return getFunctionalRequirements().stream()
				.map(requirement -> requirement.getWeight().weight * ScoreWeight.EXCEEDS_EXPECTATIONS.weight)
				.reduce(0, Integer::sum);
	}

	@Override
	public int getMaximumTotalQualityAttributeScore() {
		return getQualityAttributes().stream()
				.map(requirement -> requirement.getWeight().weight * ScoreWeight.EXCEEDS_EXPECTATIONS.weight)
				.reduce(0, Integer::sum);
	}

	@Override
	public int getMaximumTotalConstraintScore() {
		return getConstraints().stream()
				.map(requirement -> requirement.getWeight().weight * ScoreWeight.EXCEEDS_EXPECTATIONS.weight)
				.reduce(0, Integer::sum);
	}

	@Override
	public int getTotalFunctionalScore() {
		return getFunctionalRequirementScores().stream()
				.map(score -> score.getWeight().weight * score.getFunctionalRequirement().getWeight().weight)
				.reduce(0, Integer::sum);
	}

	@Override
	public int getTotalQualityAttributeScore() {
		return getQualityAttributeRequirementScores().stream()
				.map(score -> score.getWeight().weight * score.getQualityAttributeRequirement().getWeight().weight)
				.reduce(0, Integer::sum);
	}

	@Override
	public int getTotalConstraintScore() {
		return getConstraintScores().stream()
				.map(score -> score.getWeight().weight * score.getConstraint().getWeight().weight)
				.reduce(0, Integer::sum);
	}

	@Override
	public int getTotalScoreAsPercentage() {
		if (getMaximumTotalScore() == 0)
			return 0;
		return getTotalScore() * 100 / getMaximumTotalScore();
	}

	@Override
	public ScoreWeight getFunctionalRequirementScore(String requirementKey) {
		return getFunctionalRequirementScores().stream()
				.filter(score -> score.getFunctionalRequirement().getKey().equals(requirementKey))
				.map(FunctionalRequirementScore::getWeight).findFirst().orElse(ScoreWeight.UNKNOWN);
	}

	@Override
	public ScoreWeight getQualityAttributeScore(String requirementKey) {
		return getQualityAttributeRequirementScores().stream()
				.filter(score -> score.getQualityAttributeRequirement().getKey().equals(requirementKey))
				.map(QualityAttributeRequirementScore::getWeight).findFirst().orElse(ScoreWeight.UNKNOWN);
	}

	@Override
	public ScoreWeight getConstraintScore(String requirementKey) {
		return getConstraintScores().stream().filter(score -> score.getConstraint().getKey().equals(requirementKey))
				.map(ConstraintScore::getWeight).findFirst().orElse(ScoreWeight.UNKNOWN);
	}
}
