package com.innovenso.townplan.domain;

import com.google.common.collect.Sets;
import com.innovenso.townplan.api.value.Element;
import com.innovenso.townplan.api.value.deployment.aws.AwsAvailabilityZone;
import com.innovenso.townplan.api.value.deployment.aws.AwsInstance;
import com.innovenso.townplan.api.value.deployment.aws.AwsRegion;
import com.innovenso.townplan.api.value.deployment.aws.AwsVpc;
import lombok.experimental.SuperBuilder;
import lombok.extern.log4j.Log4j2;

import java.util.Set;
import java.util.stream.Collectors;

@SuperBuilder
@Log4j2
public class AwsRegionImpl extends AbstractAwsElementImpl implements AwsRegion {

	@Override
	public Set<AwsInstance> getInstances() {
		return Sets.union(getChildren(AwsInstance.class),
				getVpcs().stream().flatMap(vpc -> vpc.getInstances().stream()).collect(Collectors.toSet()));
	}

	@Override
	public Set<AwsAvailabilityZone> getAvailabilityZones() {
		return getChildren(AwsAvailabilityZone.class);
	}

	@Override
	public Set<AwsVpc> getVpcs() {
		return getChildren(AwsVpc.class);
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedIncomingDependencies() {
		return Set.of();
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedOutgoingDependencies() {
		return Set.of();
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedChildren() {
		return Set.of(AwsVpcImpl.class, AwsAvailabilityZoneImpl.class, AwsInstanceImpl.class);
	}

	@Override
	public String getType() {
		return "Region";
	}

	@Override
	public String getCategory() {
		return "AWS";
	}
}
