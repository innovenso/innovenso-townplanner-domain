package com.innovenso.townplan.domain;

import com.google.common.collect.Sets;
import com.innovenso.townplan.api.value.Element;
import com.innovenso.townplan.api.value.Relationship;
import lombok.Builder;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@SuperBuilder
@Log4j2
public abstract class AbstractElementImpl extends AbstractConceptImpl implements Element {
	@Builder.Default
	private final Map<String, Relationship> relationships = new HashMap<>();
	@Builder.Default
	private final Map<String, Element> children = new HashMap<>();

	public Element getParentElement() {
		return parentElement;
	}

	public void setParentElement(Element parentElement) {
		this.parentElement = parentElement;
	}

	private Element parentElement;

	public final void addRelationship(@NonNull final Relationship relationship) {
		if (relationship.getSource().equals(this) && !acceptsOutgoingDependency(relationship.getTarget().getClass())) {
			log.error("outgoing dependency not allowed: {}", relationship);
			return;
		}
		if (relationship.getTarget().equals(this) && !acceptsIncomingDependency(relationship.getSource().getClass())) {
			log.error("incoming dependency not allowed: {}", relationship);
		}
		log.debug("adding relationship {} to {}", relationship.getKey(), this.getKey());
		this.relationships.put(relationship.getKey(), relationship);
	}

	public final void removeRelationship(@NonNull final Relationship relationship) {
		this.relationships.remove(relationship.getKey());
	}

	@Override
	public Set<Element> getDirectIncomingDependencies() {
		return getIncomingRelationships().stream().map(Relationship::getSource).filter(element -> !this.equals(element))
				.collect(Collectors.toSet());
	}

	@Override
	public Set<Element> getDirectOutgoingDependencies() {
		return getOutgoingRelationships().stream().map(Relationship::getTarget).filter(element -> !this.equals(element))
				.collect(Collectors.toSet());
	}

	@Override
	public Set<Relationship> getIncomingRelationships() {
		return getRelationships(relationship -> relationship.getTarget().getKey().equals(this.getKey()));
	}

	@Override
	public Set<Relationship> getOutgoingRelationships() {
		return getRelationships(relationship -> relationship.getSource().getKey().equals(this.getKey()));
	}

	private Set<Relationship> getRelationships(Predicate<? super Relationship> filter) {
		return relationships.values().stream().filter(filter).collect(Collectors.toSet());
	}

	@Override
	public Set<Relationship> getIncomingRelationships(Class<? extends Element> sourceClass) {
		return getRelationships(relationship -> relationship.getTarget().equals(this)
				&& sourceClass.isInstance(relationship.getSource()));
	}

	@Override
	public Set<Relationship> getOutgoingRelationships(Class<? extends Element> targetClass) {
		return getRelationships(relationship -> relationship.getSource().equals(this)
				&& targetClass.isInstance(relationship.getTarget()));
	}

	@Override
	public Set<Relationship> getRelationships(Class<? extends Element> otherClass) {
		return Sets.union(getIncomingRelationships(otherClass), getOutgoingRelationships(otherClass));
	}

	@Override
	public Set<Relationship> getRelationships() {
		return Sets.union(getIncomingRelationships(), getOutgoingRelationships());
	}

	@Override
	public Set<Element> getDirectDependencies() {
		Set<Element> targets = getDirectIncomingDependencies();
		Set<Element> sources = getDirectOutgoingDependencies();
		return Sets.union(sources, targets);
	}

	public Set<Element> getDirectDependencies(@NonNull final Class<? extends Element> elementClass) {
		return getDirectDependencies().stream().filter(element -> elementClass.isAssignableFrom(element.getClass()))
				.collect(Collectors.toSet());
	}

	@Override
	public Set<Element> getChildDependencies() {
		return getChildren().stream().flatMap(child -> getDirectDependencies().stream()).collect(Collectors.toSet());
	}

	@Override
	public Set<Element> getChildDependencies(@NonNull final Class<? extends Element> elementClass) {
		return getChildren().stream().flatMap(child -> child.getDirectDependencies(elementClass).stream())
				.collect(Collectors.toSet());
	}

	@Override
	public Set<Element> getChildren() {
		return Set.copyOf(children.values());
	}

	protected <T> Set<T> getChildren(Class<T> childClass) {
		return getChildren().stream().filter(childClass::isInstance).map(childClass::cast).collect(Collectors.toSet());
	}

	@Override
	public boolean acceptsIncomingDependency(@NonNull final Class<? extends Element> elementClass) {
		return getAcceptedIncomingDependencies().stream().anyMatch(accepted -> accepted.isAssignableFrom(elementClass));
	}

	@Override
	public boolean acceptsOutgoingDependency(@NonNull final Class<? extends Element> elementClass) {
		return getAcceptedOutgoingDependencies().stream().anyMatch(accepted -> accepted.isAssignableFrom(elementClass));
	}

	@Override
	public boolean acceptsChild(@NonNull final Class<? extends Element> childClass) {
		return getAcceptedChildren().stream().anyMatch(accepted -> accepted.isAssignableFrom(childClass));
	}

	@Override
	public Optional<Element> getParent() {
		return Optional.ofNullable(parentElement);
	}

	@Override
	public boolean hasChildren() {
		return !this.children.isEmpty();
	}

	void addChild(@NonNull final Element child) {
		if (acceptsChild(child.getClass())) {
			this.children.put(child.getKey(), child);
		}
	}

	void removeChild(@NonNull final Element child) {
		this.children.remove(child.getKey());
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("parentElement", parentElement).appendSuper(super.toString())
				.toString();
	}
}
