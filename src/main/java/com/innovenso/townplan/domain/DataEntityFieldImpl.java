package com.innovenso.townplan.domain;

import com.innovenso.townplan.api.value.Element;
import com.innovenso.townplan.api.value.it.DataEntity;
import com.innovenso.townplan.api.value.it.DataEntityField;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.Set;

@SuperBuilder
public class DataEntityFieldImpl extends AbstractElementImpl implements DataEntityField {
	@Getter
	@Setter
	boolean mandatory;
	@Getter
	@Setter
	boolean unique;
	@Getter
	@Setter
	String fieldConstraints;
	@Getter
	@Setter
	String defaultValue;
	@Getter
	@Setter
	String localization;
	@Getter
	@Setter
	String type;
	@Setter
	String sortKey;

	@Override
	public Set<Class<? extends Element>> getAcceptedIncomingDependencies() {
		return Set.of();
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedOutgoingDependencies() {
		return Set.of();
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedChildren() {
		return Set.of();
	}

	@Override
	public String getCategory() {
		return "DataEntityField";
	}

	@Override
	public DataEntity getDataEntity() {
		return getParent().filter(DataEntity.class::isInstance).map(DataEntity.class::cast).orElse(null);
	}

	@Override
	public String getSortKey() {
		return sortKey == null ? getKey() : sortKey;
	}
}
