package com.innovenso.townplan.domain;

import com.innovenso.townplan.api.value.Element;
import com.innovenso.townplan.api.value.view.FlowView;
import com.innovenso.townplan.api.value.view.Step;
import lombok.Builder;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@SuperBuilder
public class FlowViewImpl extends AbstractViewImpl implements FlowView {
	@Builder.Default
	private final List<StepImpl> steps = new ArrayList<>();

	@Override
	public boolean isShowStepCounter() {
		return showStepCounter;
	}

	public void setShowStepCounter(boolean showStepCounter) {
		this.showStepCounter = showStepCounter;
	}

	private boolean showStepCounter;

	@Override
	public List<Step> getSteps() {
		return steps.stream().map(step -> (Step) step).collect(Collectors.toList());
	}

	@Override
	public Set<Element> getElements() {
		final Set<Element> elements = new HashSet<>();
		steps.forEach(step -> {
			elements.add(step.getRelationship().getSource());
			elements.add(step.getRelationship().getTarget());
		});
		return elements;
	}

	@Override
	public <T extends Element> Set<T> getElements(@NonNull final Class<T> elementClass) {
		return getElements().stream().filter(elementClass::isInstance).map(elementClass::cast)
				.collect(Collectors.toSet());
	}

	void addStep(@NonNull final StepImpl step) {
		this.steps.add(step);
	}

	void clearSteps() {
		this.steps.clear();
	}
}
