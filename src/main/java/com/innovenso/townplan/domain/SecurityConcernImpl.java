package com.innovenso.townplan.domain;

import com.innovenso.townplan.api.value.aspects.SecurityConcern;
import com.innovenso.townplan.api.value.aspects.SecurityConcernType;
import lombok.Builder;

@Builder
public class SecurityConcernImpl implements SecurityConcern {
	private String key;
	private String title;
	private String description;

	@Override
	public String getSortKey() {
		return sortKey;
	}

	public void setSortKey(String sortKey) {
		this.sortKey = sortKey;
	}

	private String sortKey;

	@Override
	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	@Override
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public SecurityConcernType getConcernType() {
		return concernType;
	}

	public void setConcernType(SecurityConcernType concernType) {
		this.concernType = concernType;
	}

	private SecurityConcernType concernType;
}
