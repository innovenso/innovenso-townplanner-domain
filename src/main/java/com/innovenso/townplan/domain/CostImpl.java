package com.innovenso.townplan.domain;

import com.innovenso.townplan.api.value.aspects.Cost;
import com.innovenso.townplan.api.value.aspects.CostType;
import lombok.Builder;
import lombok.Value;

@Builder
@Value
public class CostImpl implements Cost {
	String key;
	String description;
	String title;
	String category;
	CostType costType;
	String unitOfMeasure;
	Double numberOfUnits;
	Double costPerUnit;
	Integer fiscalYear;
}
