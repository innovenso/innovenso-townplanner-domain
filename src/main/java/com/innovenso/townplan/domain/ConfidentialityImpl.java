package com.innovenso.townplan.domain;

import com.innovenso.townplan.api.value.aspects.Confidentiality;
import com.innovenso.townplan.api.value.aspects.ConfidentialityLevel;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class ConfidentialityImpl implements Confidentiality {
	String key;
	ConfidentialityLevel level;
	String description;
}
