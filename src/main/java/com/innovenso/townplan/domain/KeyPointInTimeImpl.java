package com.innovenso.townplan.domain;

import com.innovenso.townplan.api.KeyPointInTime;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

import java.time.LocalDate;
import java.util.Objects;

@Builder
@Value
public class KeyPointInTimeImpl implements KeyPointInTime {
	@NonNull
	LocalDate date;
	String title;
	boolean diagramsNeeded;

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		KeyPointInTimeImpl that = (KeyPointInTimeImpl) o;
		return date.equals(that.date);
	}

	@Override
	public int hashCode() {
		return Objects.hash(date);
	}

	@Override
	public boolean isToday() {
		return LocalDate.now().equals(date);
	}

	@Override
	public boolean isFuture() {
		return date.isAfter(LocalDate.now());
	}

	@Override
	public boolean isPast() {
		return date.isBefore(LocalDate.now());
	}
}
