package com.innovenso.townplan.domain;

import com.innovenso.townplan.api.value.deployment.aws.AwsElement;
import lombok.experimental.SuperBuilder;
import lombok.extern.log4j.Log4j2;

@SuperBuilder
@Log4j2
public abstract class AbstractAwsElementImpl extends AbstractElementImpl implements AwsElement {
}
