package com.innovenso.townplan.domain;

import com.innovenso.townplan.api.value.Element;
import com.innovenso.townplan.api.value.business.BusinessActor;
import com.innovenso.townplan.api.value.enums.TechnologyType;
import com.innovenso.townplan.api.value.it.*;
import com.innovenso.townplan.api.value.it.decision.Decision;
import com.innovenso.townplan.api.value.it.decision.DecisionOption;
import lombok.Builder;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@SuperBuilder
public class ItSystemImpl extends AbstractElementImpl implements ItSystem {
	@Builder.Default
	private Map<String, ItSystemIntegrationImpl> integrations = new HashMap<>();

	@Override
	public Set<ItContainer> getContainers() {
		return getChildren().stream().filter(child -> child instanceof ItContainer).map(child -> (ItContainer) child)
				.collect(Collectors.toSet());
	}

	@Override
	public Set<ItSystemIntegration> getIntegrations() {
		return Set.copyOf(integrations.values());
	}

	public void addIntegration(@NonNull final ItSystemIntegrationImpl integration) {
		this.integrations.put(integration.getKey(), integration);
	}

	public void removeIntegration(@NonNull final ItSystemIntegrationImpl integration) {
		this.integrations.remove(integration.getKey());
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedIncomingDependencies() {
		return Set.of(ItPlatform.class, ItSystem.class, ItContainer.class, BusinessActor.class,
				ItProjectMilestone.class, Decision.class, DecisionOption.class);
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedOutgoingDependencies() {
		return Set.of(ItPlatform.class, ItSystem.class, ItContainer.class, ArchitectureBuildingBlock.class,
				ItSystemIntegration.class, DataEntity.class);
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedChildren() {
		return Set.of(ItContainer.class, DataEntity.class);
	}

	@Override
	public String getCategory() {
		return "System";
	}

	@Override
	public Optional<ItPlatform> getPlatform() {
		return getParent().filter(ItPlatform.class::isInstance).map(ItPlatform.class::cast);
	}

	@Override
	public Set<Technology> getTechnologies(TechnologyType technologyType) {
		return getContainers().stream().flatMap(container -> container.getTechnologies(technologyType).stream())
				.collect(Collectors.toSet());
	}

	@Override
	public Set<Technology> getTechnologies() {
		return getContainers().stream().flatMap(container -> container.getTechnologies().stream())
				.collect(Collectors.toSet());
	}
}
