package com.innovenso.townplan.domain;

import com.google.common.collect.Sets;
import com.innovenso.townplan.api.value.Element;
import com.innovenso.townplan.api.value.Relationship;
import com.innovenso.townplan.api.value.RelationshipType;
import com.innovenso.townplan.api.value.business.BusinessActor;
import com.innovenso.townplan.api.value.it.*;
import com.innovenso.townplan.api.value.it.decision.Decision;
import com.innovenso.townplan.api.value.strategy.BusinessCapability;
import lombok.experimental.SuperBuilder;

import java.util.Set;
import java.util.stream.Collectors;

@SuperBuilder
public class ItProjectMilestoneImpl extends AbstractElementImpl implements ItProjectMilestone {
	@Override
	public ItProject getProject() {
		return getParent().filter(ItProject.class::isInstance).map(ItProject.class::cast).orElse(null);
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedIncomingDependencies() {
		return Set.of(BusinessActor.class, Decision.class);
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedOutgoingDependencies() {
		return Set.of(ItContainer.class, ItSystem.class, BusinessCapability.class, ArchitectureBuildingBlock.class,
				DataEntity.class, ItSystemIntegration.class, Decision.class);
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedChildren() {
		return Set.of();
	}

	@Override
	public String getSortKey() {
		return sortKey;
	}

	public void setSortKey(String sortKey) {
		this.sortKey = sortKey;
	}

	private String sortKey;

	@Override
	public Set<BusinessCapability> getImpactedBusinessCapabilities() {
		return getImpactedElements(BusinessCapability.class);
	}

	@Override
	public Set<BusinessCapability> getAddedBusinessCapabilities() {
		return getImpactedElements(BusinessCapability.class, RelationshipType.IMPACT_CREATE);
	}

	@Override
	public Set<BusinessCapability> getChangedBusinessCapabilities() {
		return getImpactedElements(BusinessCapability.class, RelationshipType.IMPACT_CHANGE);
	}

	@Override
	public Set<BusinessCapability> getRemovedBusinessCapabilities() {
		return getImpactedElements(BusinessCapability.class, RelationshipType.IMPACT_REMOVE);
	}

	@Override
	public Set<ArchitectureBuildingBlock> getImpactedBuildingBlocks() {
		return getImpactedElements(ArchitectureBuildingBlock.class);
	}

	@Override
	public Set<ArchitectureBuildingBlock> getAddedBuildingBlocks() {
		return getImpactedElements(ArchitectureBuildingBlock.class, RelationshipType.IMPACT_CREATE);
	}

	@Override
	public Set<ArchitectureBuildingBlock> getChangedBuildingBlocks() {
		return getImpactedElements(ArchitectureBuildingBlock.class, RelationshipType.IMPACT_CHANGE);
	}

	@Override
	public Set<ArchitectureBuildingBlock> getRemovedBuildingBlocks() {
		return getImpactedElements(ArchitectureBuildingBlock.class, RelationshipType.IMPACT_REMOVE);
	}

	@Override
	public Set<DataEntity> getImpactedDataEntities() {
		return getImpactedElements(DataEntity.class);
	}

	@Override
	public Set<DataEntity> getAddedDataEntities() {
		return getImpactedElements(DataEntity.class, RelationshipType.IMPACT_CREATE);
	}

	@Override
	public Set<DataEntity> getChangedDataEntities() {
		return getImpactedElements(DataEntity.class, RelationshipType.IMPACT_CHANGE);
	}

	@Override
	public Set<DataEntity> getRemovedDataEntities() {
		return getImpactedElements(DataEntity.class, RelationshipType.IMPACT_REMOVE);
	}

	@Override
	public Set<ItSystem> getImpactedItSystems() {
		return getImpactedElements(ItSystem.class);
	}

	@Override
	public Set<ItSystem> getAddedItSystems() {
		return getImpactedElements(ItSystem.class, RelationshipType.IMPACT_CREATE);
	}

	@Override
	public Set<ItSystem> getChangedItSystems() {
		return getImpactedElements(ItSystem.class, RelationshipType.IMPACT_CHANGE);
	}

	@Override
	public Set<ItSystem> getRemovedItSystems() {
		return getImpactedElements(ItSystem.class, RelationshipType.IMPACT_REMOVE);
	}

	@Override
	public Set<ItSystemIntegration> getImpactedItSystemIntegrations() {
		return getImpactedElements(ItSystemIntegration.class);
	}

	@Override
	public Set<ItSystemIntegration> getAddedItSystemIntegrations() {
		return getImpactedElements(ItSystemIntegration.class, RelationshipType.IMPACT_CREATE);
	}

	@Override
	public Set<ItSystemIntegration> getChangedItSystemIntegrations() {
		return getImpactedElements(ItSystemIntegration.class, RelationshipType.IMPACT_CHANGE);
	}

	@Override
	public Set<ItSystemIntegration> getRemovedItSystemIntegrations() {
		return getImpactedElements(ItSystemIntegration.class, RelationshipType.IMPACT_REMOVE);
	}

	@Override
	public Set<ItContainer> getImpactedItContainers() {
		return getImpactedElements(ItContainer.class);
	}

	@Override
	public Set<ItContainer> getAddedItContainers() {
		return getImpactedElements(ItContainer.class, RelationshipType.IMPACT_CREATE);
	}

	@Override
	public Set<ItContainer> getChangedItContainers() {
		return getImpactedElements(ItContainer.class, RelationshipType.IMPACT_CHANGE);
	}

	@Override
	public Set<ItContainer> getRemovedItContainers() {
		return getImpactedElements(ItContainer.class, RelationshipType.IMPACT_REMOVE);
	}

	@Override
	public Set<Relationship> getStakeholderRelationships() {
		return getIncomingRelationships(BusinessActor.class).stream()
				.filter(relationship -> relationship.getRelationshipType().equals(RelationshipType.STAKEHOLDER))
				.collect(Collectors.toSet());
	}

	private <T extends Element> Set<T> getImpactedElements(Class<T> elementClass, RelationshipType... impactTypes) {
		final Set<RelationshipType> impactsToCheck = impactTypes == null || impactTypes.length == 0
				? Set.of(RelationshipType.IMPACT_CHANGE, RelationshipType.IMPACT_CREATE, RelationshipType.IMPACT_REMOVE,
						RelationshipType.IMPACT_REMAINS_UNCHANGED)
				: Set.of(impactTypes);
		return getOutgoingRelationships(elementClass).stream()
				.filter(relationship -> impactsToCheck.stream()
						.anyMatch(relationshipType -> relationship.getRelationshipType().equals(relationshipType)))
				.map(Relationship::getTarget).filter(elementClass::isInstance).map(elementClass::cast)
				.collect(Collectors.toSet());
	}

	@Override
	public Set<BusinessActor> getResponsibles() {
		return getActors(RelationshipType.RESPONSIBLE);
	}

	private Set<BusinessActor> getActors(RelationshipType relationshipType) {
		return getIncomingRelationships().stream()
				.filter(relationship -> relationship.getRelationshipType().equals(relationshipType))
				.map(Relationship::getSource).filter(BusinessActor.class::isInstance).map(BusinessActor.class::cast)
				.collect(Collectors.toSet());
	}

	@Override
	public Set<BusinessActor> getAccountables() {
		return getActors(RelationshipType.ACCOUNTABLE);
	}

	@Override
	public Set<BusinessActor> getConsulted() {
		return getActors(RelationshipType.HAS_BEEN_CONSULTED);
	}

	@Override
	public Set<BusinessActor> getInformed() {
		return getActors(RelationshipType.HAS_BEEN_INFORMED);
	}

	@Override
	public Set<BusinessActor> getToBeConsulted() {
		return getActors(RelationshipType.TO_BE_CONSULTED);
	}

	@Override
	public Set<BusinessActor> getToBeInformed() {
		return getActors(RelationshipType.TO_BE_INFORMED);
	}

	@Override
	public Set<BusinessActor> getChickens() {
		return Sets.union(Sets.union(getToBeInformed(), getInformed()), Sets.union(getToBeConsulted(), getConsulted()));
	}

	@Override
	public Set<BusinessActor> getPigs() {
		return Sets.union(getAccountables(), getResponsibles());
	}

	@Override
	public Set<BusinessActor> getRaci() {
		return Sets.union(getChickens(), getPigs());
	}
}
