package com.innovenso.townplan.domain;

import com.innovenso.townplan.api.value.aspects.Documentation;
import com.innovenso.townplan.api.value.aspects.DocumentationType;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@Builder
public class DocumentationImpl implements Documentation {
	@Getter
	@Setter
	private DocumentationType type;

	@Getter
	@Setter
	private String description;

	@Getter
	@Setter
	private String sortKey;

	@Override
	public String getKey() {
		final String keyString = type.label + "_" + description;
		return DigestUtils.sha1Hex(keyString.getBytes());
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("documentationType", type).append("description", description)
				.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;

		if (o == null || getClass() != o.getClass())
			return false;

		DocumentationImpl that = (DocumentationImpl) o;

		return new EqualsBuilder().append(type, that.type).append(description, that.description).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37).append(type).append(description).toHashCode();
	}
}
