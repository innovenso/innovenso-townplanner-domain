package com.innovenso.townplan.domain;

import com.innovenso.townplan.api.value.ModelComponent;
import com.innovenso.townplan.api.value.aspects.ContentDistribution;
import com.innovenso.townplan.api.value.aspects.ContentOutputType;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.experimental.SuperBuilder;
import lombok.extern.log4j.Log4j2;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Log4j2
@SuperBuilder
public class AbstractModelComponentImpl implements ModelComponent {
	private String key;
	@Builder.Default
	private Set<ContentDistribution> contentDistributions = new HashSet<>();

	@Override
	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public void addContentDistribution(@NonNull final ContentDistribution contentDistribution) {
		this.contentDistributions.add(contentDistribution);
	}

	@Override
	public Set<ContentDistribution> getContentDistributions() {
		return Set.copyOf(contentDistributions);
	}

	@Override
	public Set<ContentDistribution> getContentDistributions(ContentOutputType contentOutputType) {
		if (contentOutputType == null)
			return getContentDistributions();
		return contentDistributions.stream()
				.filter(contentDistribution -> contentOutputType.equals(contentDistribution.getOutputType()))
				.collect(Collectors.toSet());
	}

	@Builder.Default
	private final Map<OutputFileKey, File> outputFiles = new HashMap<>();

	@Override
	public File getOutputFile(@NonNull final String provider, @NonNull final String format,
			@NonNull final String view) {
		log.debug("requesting outputFile for element {} {} {} {}", this.key, provider, format, view);
		outputFiles.keySet().forEach(key -> log.debug(key));
		return outputFiles.get(OutputFileKey.builder().provider(provider).format(format).view(view).build());
	}

	@Override
	public void setOutputFile(@NonNull final String provider, @NonNull final String format, @NonNull final String view,
			@NonNull final File outputFile) {
		outputFiles.put(OutputFileKey.builder().provider(provider).format(format).view(view).build(), outputFile);
	}

	@Override
	public Set<File> getOutputFiles() {
		return Set.copyOf(outputFiles.values());
	}

	@Value
	@Builder
	public static class OutputFileKey {
		String provider;
		String format;
		String view;
	}
}
