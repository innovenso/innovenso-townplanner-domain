package com.innovenso.townplan.domain;

import com.innovenso.townplan.api.value.aspects.SWOT;
import com.innovenso.townplan.api.value.aspects.SWOTType;
import lombok.Builder;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;

@Builder
public class SWOTImpl implements SWOT {
	public void setType(SWOTType type) {
		this.type = type;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	private SWOTType type;
	private String description;

	@Override
	public SWOTType getType() {
		return type;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("type", type).append("description", description).toString();
	}

	@Override
	public String getKey() {
		final String keyString = type.label + "_" + description;
		return DigestUtils.sha1Hex(keyString.getBytes());
	}
}
