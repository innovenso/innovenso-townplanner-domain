package com.innovenso.townplan.domain;

import com.innovenso.townplan.api.value.Element;
import com.innovenso.townplan.api.value.Relationship;
import com.innovenso.townplan.api.value.strategy.BusinessCapability;
import com.innovenso.townplan.api.value.view.ImpactBusinessCapability;
import com.innovenso.townplan.api.value.view.ImpactView;
import lombok.Builder;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;

import java.util.*;
import java.util.stream.Collectors;

@SuperBuilder
public class ImpactViewImpl extends AbstractViewImpl implements ImpactView {
	@Builder.Default
	private final Map<String, ImpactBusinessCapability> capabilities = new HashMap<>();

	@Override
	public List<ImpactBusinessCapability> getCapabilities() {
		return capabilities.values().stream()
				.sorted(Comparator.comparing(capability -> capability.getCapability().getTitle()))
				.collect(Collectors.toList());
	}

	@Override
	public Set<Element> getElements() {
		return capabilities.values().stream().map(ImpactBusinessCapability::getCapability).collect(Collectors.toSet());
	}

	@Override
	public <T extends Element> Set<T> getElements(Class<T> elementClass) {
		return getElements().stream().filter(elementClass::isInstance).map(elementClass::cast)
				.collect(Collectors.toSet());
	}

	@Override
	public Set<Relationship> getRelationships() {
		return Set.of();
	}

	public void addCapability(final @NonNull BusinessCapability capability, boolean impacted) {
		this.capabilities.put(capability.getKey(),
				ImpactBusinessCapabilityImpl.builder().capability(capability).impacted(impacted).build());
	}

	@Override
	public Optional<ImpactBusinessCapability> getCapability(@NonNull final String key) {
		return Optional.ofNullable(capabilities.get(key));
	}
}
