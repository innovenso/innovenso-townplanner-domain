package com.innovenso.townplan.domain;

import com.google.common.collect.Sets;
import com.innovenso.eventsourcing.api.exception.EntityCommandFailedException;
import com.innovenso.eventsourcing.api.id.EntityId;
import com.innovenso.eventsourcing.api.id.RevisionId;
import com.innovenso.eventsourcing.domain.entity.AbstractEntity;
import com.innovenso.townplan.api.KeyPointInTime;
import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.command.*;
import com.innovenso.townplan.api.event.*;
import com.innovenso.townplan.api.value.*;
import com.innovenso.townplan.api.value.aspects.Documentation;
import com.innovenso.townplan.api.value.aspects.SWOT;
import com.innovenso.townplan.api.value.business.BusinessActor;
import com.innovenso.townplan.api.value.deployment.aws.*;
import com.innovenso.townplan.api.value.it.*;
import com.innovenso.townplan.api.value.strategy.BusinessCapability;
import com.innovenso.townplan.api.value.view.FlowView;
import com.innovenso.townplan.api.value.view.ImpactView;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.tuple.Pair;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.LocalDate;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Log4j2
public class TownPlanImpl extends AbstractEntity<TownPlanCommand, TownPlanEvent> implements TownPlan {
	private final Map<String, AbstractElementImpl> elements = new HashMap<>();
	private final Map<String, RelationshipImpl> relationships = new HashMap<>();
	private final Map<String, FlowViewImpl> flowViews = new HashMap<>();
	private final Map<String, ImpactViewImpl> impactViews = new HashMap<>();
	private final Map<LocalDate, KeyPointInTimeImpl> keyPointsInTime = new HashMap<>();

	public TownPlanImpl() {
		super();
	}

	public TownPlanImpl(@NonNull EntityId entityId) {
		super(entityId);
	}

	public RevisionId execute(@NonNull final AddKeyPointInTimeCommand command) {
		return conditionallyApplyAndRegisterEvent(() -> !keyPointsInTime.containsKey(command.getDate()),
				() -> KeyPointInTimeAddedEvent.builder().id(createNewEventId()).date(command.getDate())
						.title(command.getTitle()).diagramsNeeded(command.isDiagramsNeeded()).build());
	}

	public RevisionId execute(@NonNull final DeleteKeyPointInTimeCommand command) {
		return conditionallyApplyAndRegisterEvent(() -> keyPointsInTime.containsKey(command.getDate()),
				() -> KeyPointInTimeDeletedEvent.builder().id(createNewEventId()).date(command.getDate()).build());
	}

	public RevisionId execute(@NonNull final AddEnterpriseCommand command) throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(
				() -> isNewOrUpdated(EnterpriseImpl.class, command,
						enterprise -> !Objects.equals(enterprise.getTitle(), command.getTitle())
								|| !Objects.equals(enterprise.getDescription(), command.getDescription())),
				() -> EnterpriseAddedEvent.builder().id(createNewEventId()).key(command.getKey())
						.title(command.getTitle()).description(command.getDescription()).build());
	}

	public RevisionId execute(@NonNull final AddPrincipleCommand command) throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(
				() -> getElement(PrincipleImpl.class, command.getKey()).isEmpty()
						&& getElement(EnterpriseImpl.class, command.getEnterprise()).isPresent(),
				() -> PrincipleAddedEvent.builder().id(createNewEventId()).key(command.getKey())
						.title(command.getTitle()).description(command.getDescription()).type(command.getType())
						.enterprise(command.getEnterprise()).sortKey(command.getSortKey()).build());
	}

	public RevisionId execute(@NonNull final UpdatePrincipleCommand command) throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(() -> getElement(PrincipleImpl.class, command.getKey()).isPresent(),
				() -> PrincipleUpdatedEvent.builder().id(createNewEventId()).key(command.getKey())
						.title(command.getTitle()).description(command.getDescription()).type(command.getType())
						.sortKey(command.getSortKey()).build());
	}

	public RevisionId execute(@NonNull final AddDecisionCommand command) throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(
				() -> getElement(DecisionImpl.class, command.getKey()).isEmpty()
						&& getElement(EnterpriseImpl.class, command.getEnterprise()).isPresent(),
				() -> DecisionAddedEvent.builder().id(createNewEventId()).key(command.getKey())
						.title(command.getTitle()).description(command.getDescription()).outcome(command.getOutcome())
						.status(command.getStatus()).enterprise(command.getEnterprise()).build());
	}

	public RevisionId execute(@NonNull final UpdateDecisionCommand command) throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(() -> getElement(DecisionImpl.class, command.getKey()).isPresent(),
				() -> DecisionUpdatedEvent.builder().id(createNewEventId()).key(command.getKey())
						.title(command.getTitle()).description(command.getDescription()).outcome(command.getOutcome())
						.status(command.getStatus()).build());
	}

	public RevisionId execute(@NonNull final AddDecisionContextCommand command) throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(
				() -> getElement(DecisionImpl.class, command.getDecision()).isPresent()
						&& getElement(DecisionContextImpl.class, command.getKey()).isEmpty(),
				() -> DecisionContextAddedEvent.builder().id(createNewEventId()).key(command.getKey())
						.title(command.getTitle()).decision(command.getDecision()).description(command.getDescription())
						.sortKey(command.getSortKey()).contextType(command.getContextType()).build());
	}

	public RevisionId execute(@NonNull final UpdateDecisionContextCommand command) throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(
				() -> getElement(DecisionContextImpl.class, command.getKey()).isPresent(),
				() -> DecisionContextUpdatedEvent.builder().id(createNewEventId()).key(command.getKey())
						.sortKey(command.getSortKey()).description(command.getDescription()).title(command.getTitle())
						.contextType(command.getContextType()).build());
	}

	public RevisionId execute(@NonNull final AddDecisionOptionCommand command) throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(
				() -> getElement(DecisionImpl.class, command.getDecision()).isPresent()
						&& getElement(DecisionOptionImpl.class, command.getKey()).isEmpty(),
				() -> DecisionOptionAddedEvent.builder().id(createNewEventId()).key(command.getKey())
						.title(command.getTitle()).decision(command.getDecision()).description(command.getDescription())
						.sortKey(command.getSortKey()).verdict(command.getVerdict()).build());
	}

	public RevisionId execute(@NonNull final UpdateDecisionOptionCommand command) throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(
				() -> getElement(DecisionOptionImpl.class, command.getKey()).isPresent(),
				() -> DecisionOptionUpdatedEvent.builder().id(createNewEventId()).key(command.getKey())
						.sortKey(command.getSortKey()).description(command.getDescription()).title(command.getTitle())
						.verdict(command.getVerdict()).build());
	}

	public RevisionId execute(@NonNull final UpdateEnterpriseCommand command) throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(() -> getElement(EnterpriseImpl.class, command.getKey()).isPresent(),
				() -> EnterpriseUpdatedEvent.builder().id(createNewEventId()).key(command.getKey())
						.title(command.getTitle()).description(command.getDescription()).build());
	}

	public RevisionId execute(@NonNull final DeleteElementCommand command) throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(() -> canDeleteElement(command),
				() -> ElementDeletedEvent.builder().id(createNewEventId()).key(command.getKey()).build());
	}

	private boolean canDeleteElement(@NonNull final DeleteElementCommand command) {
		return Stream.of(ItPlatform.class, ItSystemImpl.class, ItContainerImpl.class, BusinessActorImpl.class,
				ItSystemIntegrationImpl.class, DecisionImpl.class, DecisionContextImpl.class, DecisionOptionImpl.class)
				.anyMatch(elementClass -> getElement(elementClass, command.getKey()).isPresent());
	}

	public RevisionId execute(@NonNull final AddItProjectCommand command) throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(
				() -> isNewOrUpdated(ItProjectImpl.class, command,
						project -> !Objects.equals(project.getTitle(), command.getTitle())
								|| !Objects.equals(project.getDescription(), command.getDescription())
								|| !Objects.equals(project.getEnterprise().getKey(), command.getEnterprise())),
				() -> ItProjectAddedEvent.builder().id(createNewEventId()).key(command.getKey())
						.title(command.getTitle()).type(command.getType()).description(command.getDescription())
						.enterprise(command.getEnterprise()).build());
	}

	public RevisionId execute(@NonNull final UpdateItProjectCommand command) {
		return conditionallyApplyAndRegisterEvent(() -> getElement(ItProjectImpl.class, command.getKey()).isPresent(),
				() -> ItProjectUpdatedEvent.builder().id(createNewEventId()).key(command.getKey())
						.enterprise(command.getEnterprise()).title(command.getTitle())
						.description(command.getDescription()).type(command.getType()).build());
	}

	public RevisionId execute(@NonNull final AddItProjectMilestoneCommand command) {
		return conditionallyApplyAndRegisterEvent(
				() -> getElement(Element.class, command.getKey()).isEmpty()
						&& getElement(ItProjectImpl.class, command.getProject()).isPresent(),
				() -> ItProjectMilestoneAddedEvent.builder().id(createNewEventId()).key(command.getKey())
						.description(command.getDescription()).project(command.getProject()).title(command.getTitle())
						.sortKey(command.getSortKey()).build());
	}

	public RevisionId execute(@NonNull final UpdateItProjectMilestoneCommand command) {
		return conditionallyApplyAndRegisterEvent(
				() -> getElement(ItProjectMilestoneImpl.class, command.getKey()).isPresent(),
				() -> ItProjectMilestoneUpdatedEvent.builder().id(createNewEventId()).key(command.getKey())
						.description(command.getDescription()).project(command.getProject()).title(command.getTitle())
						.sortKey(command.getSortKey()).build());
	}

	private <T extends Element> boolean isNewOrUpdated(Class<T> elementClass, ElementCommand command,
			Function<T, Boolean> booleanSupplier) {
		Optional<T> existingOfElementClass = getElement(elementClass, command.getKey());
		log.debug("existing {}: {}", elementClass.getName(),
				existingOfElementClass.map(Element::getKey).orElse("none"));
		Optional<Element> existingOfOtherClass = getElement(Element.class, command.getKey())
				.filter(element -> !elementClass.isInstance(element));
		log.debug("existing element of other class: {}", existingOfOtherClass.map(Element::getKey).orElse("none"));
		if (existingOfElementClass.isEmpty() && existingOfOtherClass.isEmpty()) {
			log.debug("no elements found with key {}, so proceed", command.getKey());
			return true;
		}
		if (existingOfOtherClass.isPresent()) {
			log.debug("another element with key {} found, aborting", command.getKey());
			return false;
		}
		log.debug("element with key {} exists, let's see if it needs updating.", command.getKey());
		return booleanSupplier.apply(existingOfElementClass.get());
	}

	public RevisionId execute(@NonNull final AddCapabilityCommand command) throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(
				() -> isNewOrUpdated(BusinessCapabilityImpl.class, command,
						businessCapability -> changed(businessCapability.getTitle(), command.getTitle())
								|| changed(businessCapability.getDescription(), command.getDescription())
								|| changed(businessCapability.getEnterprise().getKey(), command.getEnterprise())
								|| changed(businessCapability.getSortKey(), command.getSortKey())
								|| changed(businessCapability.getParent().map(Concept::getKey).orElse(""),
										command.getParent())),
				() -> CapabilityAddedEvent.builder().id(createNewEventId()).key(command.getKey())
						.title(command.getTitle()).description(command.getDescription())
						.enterprise(command.getEnterprise()).parent(command.getParent()).sortKey(command.getSortKey())
						.build());
	}

	public RevisionId execute(@NonNull final UpdateCapabilityCommand command) throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(
				() -> getElement(BusinessCapabilityImpl.class, command.getKey()).isPresent(),
				() -> CapabilityUpdatedEvent.builder().id(createNewEventId()).key(command.getKey())
						.description(command.getDescription()).enterprise(command.getEnterprise())
						.parent(command.getParent()).sortKey(command.getSortKey()).title(command.getTitle()).build());
	}

	private boolean changed(Object existing, Object updated) {
		log.debug("Comparing {} to {} ", existing, updated);
		return !Objects.equals(existing, updated);
	}

	public RevisionId execute(@NonNull final AddBuildingBlockCommand command) throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(
				() -> isNewOrUpdated(ArchitectureBuildingBlock.class, command,
						architectureBuildingBlock -> changed(architectureBuildingBlock.getTitle(), command.getTitle())
								|| changed(architectureBuildingBlock.getDescription(), command.getDescription())
								|| changed(architectureBuildingBlock.getEnterprise().getKey(),
										command.getEnterprise())),
				() -> BuildingBlockAddedEvent.builder().id(createNewEventId()).key(command.getKey())
						.title(command.getTitle()).description(command.getDescription())
						.enterprise(command.getEnterprise()).build());
	}

	public RevisionId execute(@NonNull final UpdateBuildingBlockCommand command) throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(
				() -> getElement(ArchitectureBuildingBlockImpl.class, command.getKey()).isPresent(),
				() -> BuildingBlockUpdatedEvent.builder().id(createNewEventId()).description(command.getDescription())
						.enterprise(command.getEnterprise()).title(command.getTitle()).key(command.getKey()).build());
	}

	public RevisionId execute(@NonNull final AddActorCommand command) throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(() -> getElement(Element.class, command.getKey()).isEmpty(),
				() -> ActorAddedEvent.builder().id(createNewEventId()).key(command.getKey()).title(command.getTitle())
						.description(command.getDescription()).enterprise(command.getEnterprise())
						.type(command.getType()).build());
	}

	public RevisionId execute(@NonNull final UpdateActorCommand command) throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(
				() -> getElement(BusinessActorImpl.class, command.getKey()).isPresent(),
				() -> ActorUpdatedEvent.builder().id(createNewEventId()).key(command.getKey())
						.description(command.getDescription()).title(command.getTitle())
						.enterprise(command.getEnterprise()).type(command.getType()).build());
	}

	public RevisionId execute(@NonNull final AddTechnologyCommand command) throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(() -> getElement(Element.class, command.getKey()).isEmpty(),
				() -> TechnologyAddedEvent.builder().id(createNewEventId()).key(command.getKey())
						.title(command.getTitle()).type(command.getType()).technologyType(command.getTechnologyType())
						.recommendation(command.getRecommendation()).description(command.getDescription()).build());
	}

	public RevisionId execute(@NonNull final AddITPlatformCommand command) throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(() -> getElement(Element.class, command.getKey()).isEmpty(),
				() -> ITPlatformAddedEvent.builder().id(createNewEventId()).key(command.getKey())
						.title(command.getTitle()).description(command.getDescription()).build());
	}

	public RevisionId execute(@NonNull final UpdateITPlatformCommand command) throws EntityCommandFailedException {
		log.debug("updating platform with command");
		return conditionallyApplyAndRegisterEvent(() -> getElement(ItPlatformImpl.class, command.getKey()).isPresent(),
				() -> ITPlatformUpdatedEvent.builder().id(createNewEventId()).key(command.getKey())
						.title(command.getTitle()).description(command.getDescription()).build());
	}

	public RevisionId execute(@NonNull final AddITSystemCommand command) throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(
				() -> isNewOrUpdated(ItSystem.class, command,
						system -> changed(system.getTitle(), command.getTitle())
								|| changed(system.getDescription(), command.getDescription())),
				() -> ITSystemAddedEvent.builder().id(createNewEventId()).key(command.getKey())
						.title(command.getTitle()).description(command.getDescription()).platform(command.getPlatform())
						.build());
	}

	public RevisionId execute(@NonNull final UpdateITSystemCommand command) throws EntityCommandFailedException {
		log.debug("updating system with command");
		return conditionallyApplyAndRegisterEvent(() -> getElement(ItSystemImpl.class, command.getKey()).isPresent(),
				() -> ITSystemUpdatedEvent.builder().id(createNewEventId()).key(command.getKey())
						.title(command.getTitle()).description(command.getDescription()).platform(command.getPlatform())
						.build());
	}

	public RevisionId execute(@NonNull final AddITSystemIntegrationCommand command)
			throws EntityCommandFailedException {
		log.debug("Adding IT System Integration between {} and {}", command.getSourceSystem(),
				command.getTargetSystem());
		return conditionallyApplyAndRegisterEvent(() -> canAddIntegration(command),
				() -> ITSystemIntegrationAddedEvent.builder().id(createNewEventId()).key(command.getKey())
						.title(command.getTitle()).description(command.getDescription())
						.sourceSystem(command.getSourceSystem()).targetSystem(command.getTargetSystem())
						.criticality(command.getCriticality()).frequency(command.getFrequency())
						.resilience(command.getResilience()).criticalityDescription(command.getCriticalityDescription())
						.volume(command.getVolume()).build());
	}

	private boolean canAddIntegration(@NonNull final AddITSystemIntegrationCommand command) {
		return getElement(Element.class, command.getKey()).isEmpty()
				&& getElement(ItSystem.class, command.getSourceSystem()).isPresent()
				&& getElement(ItSystem.class, command.getTargetSystem()).isPresent();
	}

	public RevisionId execute(@NonNull final UpdateITSystemIntegrationCommand command)
			throws EntityCommandFailedException {
		log.debug("Updating IT System Integration {}", command.getKey());
		return conditionallyApplyAndRegisterEvent(
				() -> getElement(ItSystemIntegration.class, command.getKey()).isPresent(),
				() -> ITSystemIntegrationUpdatedEvent.builder().id(createNewEventId()).key(command.getKey())
						.title(command.getTitle()).description(command.getDescription())
						.criticality(command.getCriticality()).volume(command.getVolume())
						.frequency(command.getFrequency()).resilience(command.getResilience())
						.criticalityDescription(command.getCriticalityDescription()).build());
	}

	public RevisionId execute(@NonNull final AddITContainerCommand command) throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(() -> getElement(Element.class, command.getKey()).isEmpty(),
				() -> ITContainerAddedEvent.builder().id(createNewEventId()).key(command.getKey())
						.title(command.getTitle()).description(command.getDescription()).system(command.getSystem())
						.type(command.getType()).build());
	}

	public RevisionId execute(@NonNull final UpdateITContainerCommand command) throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(() -> getElement(ItContainerImpl.class, command.getKey()).isPresent(),
				() -> ITContainerUpdatedEvent.builder().id(createNewEventId()).key(command.getKey())
						.title(command.getTitle()).description(command.getDescription()).type(command.getType())
						.system(command.getSystem()).build());
	}

	public RevisionId execute(@NonNull final AddAwsRegionCommand command) throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(() -> true, () -> AwsRegionAddedEvent.builder().id(createNewEventId())
				.key(command.getKey()).title(command.getTitle()).description(command.getDescription()).build());
	}

	public RevisionId execute(@NonNull final AddAwsVpcCommand command) throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(() -> isNewOrUpdatedAndHasRegion(command),
				() -> AwsVpcAddedEvent.builder().id(createNewEventId()).key(command.getKey()).title(command.getTitle())
						.description(command.getDescription()).region(command.getRegion()).account(command.getAccount())
						.networkMask(command.getNetworkMask()).build());
	}

	private boolean isNewOrUpdatedAndHasRegion(AddAwsVpcCommand command) {

		return elements.get(command.getRegion()) instanceof AwsRegionImpl;
	}

	public RevisionId execute(@NonNull final AddAwsAvailabilityZoneCommand command)
			throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(() -> isNewOrUpdatedAndHasRegion(command),
				() -> AwsAvailabilityZoneAddedEvent.builder().id(createNewEventId()).key(command.getKey())
						.title(command.getTitle()).region(command.getRegion()).description(command.getDescription())
						.build());
	}

	private boolean isNewOrUpdatedAndHasRegion(AddAwsAvailabilityZoneCommand command) {

		return elements.get(command.getRegion()) instanceof AwsRegionImpl;
	}

	public RevisionId execute(@NonNull final AddAwsNetworkAclCommand command) throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(() -> isNewOrUpdatedAndHasVpc(command),
				() -> AwsNetworkAclAddedEvent.builder().id(createNewEventId()).key(command.getKey())
						.title(command.getTitle()).vpc(command.getVpc()).description(command.getDescription()).build());
	}

	public RevisionId execute(@NonNull final AddAwsAutoScalingGroupCommand command)
			throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(() -> isNewOrUpdated(command),
				() -> AwsAutoscalingGroupAddedEvent.builder().id(createNewEventId()).key(command.getKey())
						.title(command.getTitle()).vpc(command.getVpc()).description(command.getDescription()).build());
	}

	private boolean isNewOrUpdated(AddAwsAutoScalingGroupCommand command) {
		return elements.get(command.getVpc()) instanceof AwsVpcImpl;
	}

	private boolean isNewOrUpdatedAndHasVpc(AddAwsNetworkAclCommand command) {

		return elements.get(command.getVpc()) instanceof AwsVpcImpl;
	}

	public RevisionId execute(@NonNull final AddAwsRouteTableCommand command) throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(() -> isNewOrUpdatedAndHasVpc(command),
				() -> AwsRouteTableAddedEvent.builder().id(createNewEventId()).key(command.getKey())
						.title(command.getTitle()).vpc(command.getVpc()).description(command.getDescription()).build());
	}

	private boolean isNewOrUpdatedAndHasVpc(AddAwsRouteTableCommand command) {

		return elements.get(command.getVpc()) instanceof AwsVpcImpl;
	}

	public RevisionId execute(@NonNull final AddAwsSubnetCommand command) throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(() -> isNewOrUpdatedAndHasDependencies(command),
				() -> AwsSubnetAddedEvent.builder().id(createNewEventId()).key(command.getKey())
						.title(command.getTitle()).description(command.getDescription())
						.availabilityZone(command.getAvailabilityZone()).vpc(command.getVpc())
						.subnetMask(command.getSubnetMask()).publicSubnet(command.isPublicSubnet()).build());
	}

	private boolean isNewOrUpdatedAndHasDependencies(AddAwsSubnetCommand command) {
		boolean hasVpc = hasConcept(AwsVpcImpl.class, command.getVpc());
		boolean hasAz = hasConcept(AwsAvailabilityZoneImpl.class, command.getAvailabilityZone());

		return hasVpc && hasAz;
	}

	public RevisionId execute(@NonNull final AddAwsSecurityGroupCommand command) throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(() -> true,
				() -> AwsSecurityGroupAddedEvent.builder().id(createNewEventId()).key(command.getKey())
						.title(command.getTitle()).vpc(command.getVpc()).description(command.getDescription()).build());
	}

	public RevisionId execute(@NonNull final AddAwsInstanceCommand command) throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(() -> true,
				() -> AwsInstanceAddedEvent.builder().id(createNewEventId()).key(command.getKey())
						.title(command.getTitle()).account(command.getAccount()).vpc(command.getVpc())
						.region(command.getRegion()).instanceType(command.getInstanceType())
						.description(command.getDescription()).build());
	}

	public RevisionId execute(@NonNull final AssignSecurityGroupToInstanceCommand command)
			throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(() -> canExecute(command),
				() -> SecurityGroupAssignedToInstanceEvent.builder().id(createNewEventId())
						.securityGroupKey(command.getSecurityGroupKey()).instanceKey(command.getInstanceKey()).build());
	}

	private boolean canExecute(AssignSecurityGroupToInstanceCommand command) {

		return hasConcept(AwsSecurityGroupImpl.class, command.getSecurityGroupKey())
				&& hasConcept(AwsInstanceImpl.class, command.getInstanceKey());
	}

	public RevisionId execute(@NonNull final AssignSubnetToInstanceCommand command)
			throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(() -> canExecute(command),
				() -> SubnetAssignedToInstanceEvent.builder().id(createNewEventId()).subnetKey(command.getSubnetKey())
						.instanceKey(command.getInstanceKey()).build());
	}

	private boolean canExecute(AssignSubnetToInstanceCommand command) {

		return hasConcept(AwsSubnetImpl.class, command.getSubnetKey())
				&& hasConcept(AwsInstanceImpl.class, command.getInstanceKey());
	}

	public RevisionId execute(@NonNull final AddAwsAccountCommand command) throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(() -> true,
				() -> AwsAccountAddedEvent.builder().id(createNewEventId()).key(command.getKey())
						.title(command.getTitle()).accountId(command.getAccountId())
						.description(command.getDescription()).build());
	}

	public RevisionId execute(@NonNull final AssignAvailabilityZoneToAutoScalingGroupCommand command)
			throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(() -> canExecute(command),
				() -> AvailabilityZoneAssignedToAutoScalingGroupEvent.builder().id(createNewEventId())
						.availabilityZone(command.getAvailabilityZone()).autoScalingGroup(command.getAutoScalingGroup())
						.build());
	}

	private boolean canExecute(AssignAvailabilityZoneToAutoScalingGroupCommand command) {
		return getElement(AwsAutoscalingGroupImpl.class, command.getAutoScalingGroup()).isPresent()
				&& getElement(AwsAvailabilityZoneImpl.class, command.getAvailabilityZone()).isPresent();
	}

	public RevisionId execute(@NonNull final AssignSubnetToAutoScalingGroupCommand command)
			throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(() -> canExecute(command),
				() -> SubnetAssignedToAutoScalingGroupEvent.builder().id(createNewEventId()).subnet(command.getSubnet())
						.autoScalingGroup(command.getAutoscalingGroup()).build());
	}

	private boolean canExecute(AssignSubnetToAutoScalingGroupCommand command) {
		return getElement(AwsAutoscalingGroupImpl.class, command.getAutoscalingGroup()).isPresent()
				&& getElement(AwsSubnet.class, command.getSubnet()).isPresent();
	}

	public RevisionId execute(@NonNull final AssignInstanceToAutoScalingGroupCommand command)
			throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(() -> canExecute(command),
				() -> InstanceAssignedToAutoScalingGroupEvent.builder().id(createNewEventId())
						.instance(command.getInstance()).autoscalingGroup(command.getAutoscalingGroup()).build());
	}

	private boolean canExecute(AssignInstanceToAutoScalingGroupCommand command) {
		return getElement(AwsAutoscalingGroupImpl.class, command.getAutoscalingGroup()).isPresent()
				&& getElement(AwsInstance.class, command.getInstance()).isPresent();
	}

	private boolean hasConcept(@NonNull final Class<? extends Concept> conceptClass, @NonNull final String key) {
		return conceptClass.isInstance(elements.get(key));
	}

	public RevisionId execute(@NonNull final AddDataEntityCommand command) throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(() -> isNewOrUpdated(command),
				() -> DataEntityAddedEvent.builder().id(createNewEventId()).key(command.getKey())
						.title(command.getTitle()).description(command.getDescription())
						.entityType(command.getEntityType()).commercialValue(command.getCommercialValue())
						.sensitivity(command.getSensitivity()).consentNeeded(command.isConsentNeeded())
						.masterSystem(command.getMasterSystem()).enterprise(command.getEnterprise())
						.businessCapability(command.getBusinessCapability()).build());
	}

	public RevisionId execute(@NonNull final AddDataEntityFieldCommand command) throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(() -> getConcept(command.getKey()).isEmpty(),
				() -> DataEntityFieldAddedEvent.builder().id(createNewEventId()).key(command.getKey())
						.title(command.getTitle()).description(command.getDescription()).type(command.getType())
						.dataEntityKey(command.getDataEntityKey()).defaultValue(command.getDefaultValue())
						.fieldConstraints(command.getFieldConstraints()).localization(command.getLocalization())
						.mandatory(command.isMandatory()).unique(command.isUnique()).sortKey(command.getSortKey())
						.build());
	}

	public RevisionId execute(@NonNull final UpdateDataEntityCommand command) throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(() -> getElement(DataEntityImpl.class, command.getKey()).isPresent(),
				() -> DataEntityUpdatedEvent.builder().id(createNewEventId()).key(command.getKey())
						.title(command.getTitle()).description(command.getDescription())
						.entityType(command.getEntityType()).commercialValue(command.getCommercialValue())
						.sensitivity(command.getSensitivity()).consentNeeded(command.isConsentNeeded())
						.masterSystem(command.getMasterSystem()).enterprise(command.getEnterprise())
						.businessCapability(command.getBusinessCapability()).build());
	}

	public RevisionId execute(@NonNull final UpdateDataEntityFieldCommand command) throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(() -> getConcept(command.getKey()).isPresent(),
				() -> DataEntityFieldUpdatedEvent.builder().id(createNewEventId()).key(command.getKey())
						.title(command.getTitle()).description(command.getDescription()).type(command.getType())
						.defaultValue(command.getDefaultValue()).fieldConstraints(command.getFieldConstraints())
						.localization(command.getLocalization()).mandatory(command.isMandatory())
						.unique(command.isUnique()).sortKey(command.getSortKey()).build());
	}

	public RevisionId execute(@NonNull final AddAwsElasticIpAddressCommand command)
			throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(() -> true,
				() -> AwsElasticIpAddressAddedEvent.builder().id(createNewEventId()).key(command.getKey())
						.title(command.getTitle()).description(command.getDescription())
						.allocationId(command.getAllocationId()).associationId(command.getAssociationId())
						.carrierIp(command.getCarrierIp()).customerOwnedIp(command.getCustomerOwnedIp())
						.customerOwnedIpv4Pool(command.getCustomerOwnedIpv4Pool()).domain(command.getDomain())
						.instanceId(command.getInstanceId()).networkBorderGroup(command.getNetworkBorderGroup())
						.networkInterfaceId(command.getNetworkInterfaceId())
						.networkInterfaceOwnerId(command.getNetworkInterfaceOwnerId())
						.privateIpAddress(command.getPrivateIpAddress()).publicIpAddress(command.getPublicIpAddress())
						.publicIpv4Pool(command.getPublicIpv4Pool()).region(command.getRegion())
						.account(command.getAccount()).instance(command.getInstance()).build());
	}

	private boolean isNewOrUpdated(AddDataEntityCommand command) {
		Optional<DataEntityImpl> existing = getElement(DataEntityImpl.class, command.getKey());
		if (existing.isEmpty())
			return true;
		else {
			DataEntityImpl dataEntity = existing.get();
			return !Objects.equals(dataEntity.getTitle(), command.getTitle())
					&& !Objects.equals(dataEntity.getDescription(), command.getDescription())
					&& !Objects.equals(dataEntity.getEntityType(), command.getEntityType())
					&& !Objects.equals(dataEntity.getCommercialValue(), command.getCommercialValue())
					&& !Objects.equals(dataEntity.getSensitivity(), command.getSensitivity())
					&& !Objects.equals(dataEntity.isConsentNeeded(), command.isConsentNeeded())
					&& !Objects.equals(dataEntity.getMasterSystem().getKey(), command.getMasterSystem())
					&& !Objects.equals(dataEntity.getEnterprise().getKey(), command.getEnterprise())
					&& !Objects.equals(dataEntity.getBusinessCapability().getKey(), command.getBusinessCapability());
		}
	}

	public RevisionId execute(@NonNull final AddITContainerTechnologyCommand command)
			throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(
				() -> getElement(ItContainer.class, command.getContainer()).isPresent()
						&& getElement(Technology.class, command.getTechnology()).isPresent()
						&& !getElement(ItContainer.class, command.getContainer()).get()
								.hasTechnology(command.getTechnology()),
				() -> ITContainerTechnologyAddedEvent.builder().id(createNewEventId()).container(command.getContainer())
						.technology(command.getTechnology()).build());
	}

	public RevisionId execute(@NonNull final AddRelationshipTechnologyCommand command)
			throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(
				() -> getRelationship(command.getRelationship()).isPresent()
						&& getElement(Technology.class, command.getTechnology()).isPresent()
						&& !getRelationship(command.getRelationship()).get().hasTechnology(command.getTechnology()),
				() -> RelationshipTechnologyAddedEvent.builder().id(createNewEventId())
						.relationship(command.getRelationship()).technology(command.getTechnology()).build());
	}

	@Override
	public <T extends Element> Optional<T> getElement(@NonNull final Class<T> elementClass, final String key) {
		if (key == null || key.isEmpty())
			return Optional.empty();
		if (!this.elements.containsKey(key))
			return Optional.empty();
		final Element element = elements.get(key);
		if (elementClass.isInstance(element))
			return Optional.of(elementClass.cast(element));
		else
			return Optional.empty();
	}

	public RevisionId execute(@NonNull final AddRelationshipCommand command) throws EntityCommandFailedException {
		log.debug("execute relationship command {}", command);
		return conditionallyApplyAndRegisterEvent(() -> true,
				() -> RelationshipAddedEvent.builder().id(createNewEventId()).source(command.getSource())
						.target(command.getTarget()).title(command.getTitle()).description(command.getDescription())
						.bidirectional(command.isBidirectional()).influence(command.getInfluence())
						.interest(command.getInterest()).integrationIdentifier(command.getIntegrationIdentifier())
						.type(command.getType()).build());
	}

	public RevisionId execute(@NonNull final UpdateRelationshipCommand command) {
		return conditionallyApplyAndRegisterEvent(
				() -> getRelationship(command.getSource(), command.getTarget(), command.getType()).isPresent(),
				() -> RelationshipUpdatedEvent.builder().id(createNewEventId()).bidirectional(command.isBidirectional())
						.influence(command.getInfluence()).interest(command.getInterest()).source(command.getSource())
						.target(command.getTarget()).title(command.getTitle()).description(command.getDescription())
						.integrationIdentifier(command.getIntegrationIdentifier()).type(command.getType()).build());
	}

	public RevisionId execute(@NonNull final DeleteRelationshipCommand command) throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(
				() -> getRelationship(command.getSource(), command.getTarget(), command.getType()).isPresent(),
				() -> RelationshipDeletedEvent.builder().id(createNewEventId()).source(command.getSource())
						.target(command.getTarget()).type(command.getType()).build());
	}

	public RevisionId execute(@NonNull final AddImpactViewCommand command) throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(() -> !impactViews.containsKey(command.getKey()),
				() -> ImpactViewAddedEvent.builder().id(createNewEventId()).key(command.getKey())
						.title(command.getTitle()).description(command.getDescription()).build());
	}

	public RevisionId execute(@NonNull final AddImpactViewCapabilityCommand command)
			throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(
				() -> impactViews.containsKey(command.getView()) && getElements(BusinessCapability.class).stream()
						.anyMatch(businessCapability -> businessCapability.getKey().equals(command.getCapability())),
				() -> ImpactViewCapabilityAddedEvent.builder().id(createNewEventId()).view(command.getView())
						.capability(command.getCapability()).impacted(command.isImpacted()).build());
	}

	public RevisionId execute(@NonNull final AddFlowViewCommand command) throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(() -> !flowViews.containsKey(command.getKey()),
				() -> FlowViewAddedEvent.builder().id(createNewEventId()).key(command.getKey())
						.title(command.getTitle()).description(command.getDescription())
						.showStepCounter(command.isShowStepCounter()).build());
	}

	public RevisionId execute(@NonNull final UpdateFlowViewCommand command) throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(() -> shouldUpdate(command),
				() -> FlowViewUpdatedEvent.builder().id(createNewEventId()).key(command.getKey())
						.description(command.getDescription()).title(command.getTitle())
						.showStepCounter(command.isShowStepCounter()).build());
	}

	private boolean shouldUpdate(UpdateFlowViewCommand command) {
		return getFlowView(command.getKey())
				.map(flowView -> !Objects.equals(command.getDescription(), flowView.getDescription())
						|| !Objects.equals(command.getTitle(), flowView.getTitle())
						|| !Objects.equals(command.isShowStepCounter(), flowView.isShowStepCounter()))
				.orElse(false);
	}

	public RevisionId execute(@NonNull final DeleteFlowViewCommand command) throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(() -> getFlowView(command.getKey()).isPresent(),
				() -> FlowViewDeletedEvent.builder().id(createNewEventId()).key(command.getKey()).build());
	}

	public RevisionId execute(@NonNull final ClearFlowViewStepsCommand command) throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(() -> flowViews.containsKey(command.getView()),
				() -> FlowViewStepsClearedEvent.builder().id(createNewEventId()).view(command.getView()).build());
	}

	public RevisionId execute(@NonNull final AddFlowViewStepCommand command) throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(
				() -> flowViews.containsKey(command.getView()) && relationships.containsKey(command.getRelationship()),
				() -> FlowViewStepAddedEvent.builder().id(createNewEventId()).view(command.getView())
						.title(command.getTitle()).relationship(command.getRelationship())
						.response(command.isResponse()).build());
	}

	public RevisionId execute(@NonNull final SetFlowViewStepsCommand command) throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(() -> flowViews.containsKey(command.getView()),
				() -> FlowViewStepsSetEvent.builder().id(createNewEventId()).view(command.getView())
						.steps(command.getSteps()).build());
	}

	public RevisionId execute(@NonNull final SetSystemIntegrationStepsCommand command)
			throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(
				() -> getElement(ItSystemIntegrationImpl.class, command.getKey()).isPresent(),
				() -> SystemIntegrationStepsSetEvent.builder().id(createNewEventId()).key(command.getKey())
						.steps(command.getSteps()).build());
	}

	public RevisionId execute(@NonNull final DeleteDocumentationAspectCommand command)
			throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(
				() -> getElement(AbstractElementImpl.class, command.getConceptKey())
						.map(element -> element.getDocumentations().stream()
								.anyMatch(doc -> doc.getKey().equals(command.getDocumentationKey())))
						.orElse(false),
				() -> DocumentationAspectDeletedEvent.builder().id(createNewEventId())
						.conceptKey(command.getConceptKey()).documentationKey(command.getDocumentationKey()).build());
	}

	public RevisionId execute(@NonNull final AddDocumentationAspectCommand command)
			throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(() -> isNewOrUpdatedAndCanAdd(command),
				() -> DocumentationAspectAddedEvent.builder().id(createNewEventId()).conceptKey(command.getConceptKey())
						.sortKey(command.getSortKey()).type(command.getType()).description(command.getDescription())
						.build());
	}

	private boolean isNewOrUpdatedAndCanAdd(@NonNull final AddDocumentationAspectCommand command) {
		if (!elements.containsKey(command.getConceptKey()) && !relationships.containsKey(command.getConceptKey()))
			return false;
		final Element element = elements.get(command.getConceptKey());
		final Relationship relationship = relationships.get(command.getConceptKey());
		final Concept concept = element == null ? relationship : element;
		Set<Documentation> documentations = concept.getDocumentations(Optional.of(command.getType()));
		return documentations.stream()
				.noneMatch(documentation -> documentation.getDescription().equals(command.getDescription()));
	}

	public RevisionId execute(@NonNull final AddExternalIdAspectCommand command) throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(() -> getConcept(command.getConceptKey()).isPresent(),
				() -> ExternalIdAspectAddedEvent.builder().conceptKey(command.getConceptKey()).id(createNewEventId())
						.value(command.getValue()).type(command.getType()).build());
	}

	public RevisionId execute(@NonNull final DeleteExternalIdAspectCommand command)
			throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(
				() -> getConcept(command.getConceptKey()).map(concept -> concept.getExternalIds().stream()
						.anyMatch(externalId -> Objects.equals(externalId.getExternalIdType(), command.getType())
								&& Objects.equals(externalId.getValue(), command.getValue())))
						.orElse(false),
				() -> ExternalIdAspectDeletedEvent.builder().conceptKey(command.getConceptKey()).id(createNewEventId())
						.value(command.getValue()).type(command.getType()).build());
	}

	public RevisionId execute(@NonNull final AddConstraintAspectCommand command) throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(() -> getModelComponent(command.getModelComponentKey()).isPresent(),
				() -> ConstraintAspectAddedEvent.builder().modelComponentKey(command.getModelComponentKey())
						.description(command.getDescription()).id(createNewEventId()).title(command.getTitle())
						.sortKey(command.getSortKey()).weight(command.getWeight()).key(command.getKey()).build());
	}

	public RevisionId execute(@NonNull final AddSecurityConcernAspectCommand command)
			throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(() -> getModelComponent(command.getModelComponentKey()).isPresent(),
				() -> SecurityConcernAspectAddedEvent.builder().modelComponentKey(command.getModelComponentKey())
						.description(command.getDescription()).id(createNewEventId())
						.concernType(command.getConcernType()).title(command.getTitle()).sortKey(command.getSortKey())
						.build());
	}

	public RevisionId execute(@NonNull final AddSecurityImpactAspectCommand command)
			throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(() -> getModelComponent(command.getModelComponentKey()).isPresent(),
				() -> SecurityImpactAspectAddedEvent.builder().modelComponentKey(command.getModelComponentKey())
						.description(command.getDescription()).id(createNewEventId())
						.impactLevel(command.getImpactLevel()).impactType(command.getImpactType()).build());
	}

	public RevisionId execute(@NonNull final AddFunctionalRequirementAspectCommand command)
			throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(() -> getModelComponent(command.getModelComponentKey()).isPresent(),
				() -> FunctionalRequirementAspectAddedEvent.builder().modelComponentKey(command.getModelComponentKey())
						.description(command.getDescription()).id(createNewEventId()).title(command.getTitle())
						.sortKey(command.getSortKey()).weight(command.getWeight()).key(command.getKey()).build());
	}

	public RevisionId execute(@NonNull final AddFunctionalRequirementScoreAspectCommand command)
			throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(() -> canAddScore(command),
				() -> FunctionalRequirementScoreAspectAddedEvent.builder().id(createNewEventId())
						.description(command.getDescription())
						.functionalRequirementKey(command.getFunctionalRequirementKey())
						.modelComponentKey(command.getModelComponentKey()).weight(command.getWeight())
						.key(command.getKey()).build());
	}

	private boolean canAddScore(AddFunctionalRequirementScoreAspectCommand command) {
		log.debug("let's check if we can add");
		boolean canAdd = getConceptImpl(command.getModelComponentKey()).map(concept -> {
			Optional<FunctionalRequirementImpl> requirement = concept
					.getFunctionalRequirement(command.getFunctionalRequirementKey());
			log.debug(requirement);
			boolean alreadyHasScore = concept.hasScore(command.getFunctionalRequirementKey());
			log.debug("has score already?: {}", alreadyHasScore);
			return requirement.isPresent() && !alreadyHasScore;
		}).orElse(false);
		log.debug("Can add score?: {}", canAdd);
		return canAdd;
	}

	public RevisionId execute(@NonNull final AddQualityAttributeRequirementScoreAspectCommand command)
			throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(
				() -> getConceptImpl(command.getModelComponentKey()).map(
						concept -> concept.getQualityAttributeRequirement(command.getQualityAttributeRequirementKey())
								.isPresent() && !concept.hasScore(command.getQualityAttributeRequirementKey()))
						.orElse(false),
				() -> QualityAttributeRequirementScoreAspectAddedEvent.builder().id(createNewEventId())
						.description(command.getDescription())
						.qualityAttributeRequirementKey(command.getQualityAttributeRequirementKey())
						.modelComponentKey(command.getModelComponentKey()).weight(command.getWeight())
						.key(command.getKey()).build());
	}

	public RevisionId execute(@NonNull final AddConstraintScoreAspectCommand command)
			throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(
				() -> getConceptImpl(command.getModelComponentKey())
						.map(concept -> concept.getConstraint(command.getConstraintKey()).isPresent()
								&& !concept.hasScore(command.getConstraintKey()))
						.orElse(false),
				() -> ConstraintScoreAspectAddedEvent.builder().id(createNewEventId())
						.description(command.getDescription()).constraintKey(command.getConstraintKey())
						.modelComponentKey(command.getModelComponentKey()).weight(command.getWeight())
						.key(command.getKey()).build());
	}

	public RevisionId execute(@NonNull final AddQualityAttributeRequirementAspectCommand command)
			throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(() -> getModelComponent(command.getModelComponentKey()).isPresent(),
				() -> QualityAttributeRequirementAspectAddedEvent.builder()
						.modelComponentKey(command.getModelComponentKey()).id(createNewEventId())
						.title(command.getTitle()).environment(command.getEnvironment()).response(command.getResponse())
						.responseMeasure(command.getResponseMeasure()).sourceOfStimulus(command.getSourceOfStimulus())
						.stimulus(command.getStimulus()).sortKey(command.getSortKey()).weight(command.getWeight())
						.key(command.getKey()).build());
	}

	public RevisionId execute(@NonNull final AddContentDistributionAspectCommand command)
			throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(() -> isNewAndCanAdd(command),
				() -> ContentDistributionAspectAddedEvent.builder().id(createNewEventId())
						.modelComponentKey(command.getModelComponentKey()).type(command.getType()).url(command.getUrl())
						.title(command.getTitle()).build());
	}

	private boolean isNewAndCanAdd(@NonNull final AddContentDistributionAspectCommand command) {
		return getModelComponent(command.getModelComponentKey())
				.map(modelComponent -> isNewContentDistributionForModelComponent(modelComponent, command))
				.orElse(false);
	}

	private boolean isNewContentDistributionForModelComponent(ModelComponent component,
			AddContentDistributionAspectCommand command) {
		return component.getContentDistributions().stream()
				.noneMatch(contentDistribution -> Objects.equals(command.getType(), contentDistribution.getOutputType())
						&& Objects.equals(command.getUrl(), contentDistribution.getUrl()));
	}

	public RevisionId execute(@NonNull final AddSwotAspectCommand command) throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(() -> isNewOrUpdatedAndCanAdd(command),
				() -> SwotAspectAddedEvent.builder().id(createNewEventId()).conceptKey(command.getConceptKey())
						.description(command.getDescription()).type(command.getType()).build());
	}

	private boolean isNewOrUpdatedAndCanAdd(@NonNull final AddSwotAspectCommand command) {
		if (!elements.containsKey(command.getConceptKey()) && !relationships.containsKey(command.getConceptKey()))
			return false;
		final Element element = elements.get(command.getConceptKey());
		final Relationship relationship = relationships.get(command.getConceptKey());
		final Concept concept = element == null ? relationship : element;
		Set<SWOT> swots = concept.getSwots(Optional.of(command.getType()));
		return swots.stream().noneMatch(swot -> swot.getDescription().equals(command.getDescription()));
	}

	public RevisionId execute(@NonNull final DeleteSwotAspectCommand command) throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(
				() -> getElement(Element.class, command.getConceptKey()).map(element -> element.getSwots().stream()
						.anyMatch(swot -> swot.getKey().equals(command.getSwotKey()))).orElse(false),
				() -> SwotAspectDeletedEvent.builder().id(createNewEventId()).conceptKey(command.getConceptKey())
						.swotKey(command.getSwotKey()).build());
	}

	public RevisionId execute(@NonNull final DeleteITContainerTechnologyCommand command)
			throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(
				() -> getElement(ItContainerImpl.class, command.getContainer())
						.map(container -> container.getTechnologies().stream()
								.anyMatch(technology -> technology.getKey().equals(command.getTechnology())))
						.orElse(false),
				() -> ITContainerTechnologyDeletedEvent.builder().id(createNewEventId())
						.container(command.getContainer()).technology(command.getTechnology()).build());
	}

	public RevisionId execute(@NonNull final SetLifecycleAspectCommand command) throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(() -> canSet(command),
				() -> LifecycleAspectSetEvent.builder().id(createNewEventId()).conceptKey(command.getConceptKey())
						.type(command.getLifecyleType()).description(command.getDescription()).build());
	}

	public RevisionId execute(@NonNull final SetArchitectureVerdictAspectCommand command)
			throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(() -> getConcept(command.getConceptKey()).isPresent(),
				() -> ArchitectureVerdictAspectSetEvent.builder().id(createNewEventId())
						.conceptKey(command.getConceptKey()).verdictType(command.getVerdictType())
						.description(command.getDescription()).build());
	}

	public RevisionId execute(@NonNull final DeleteAspectCommand command) throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(() -> canDeleteAspect(command), () -> AspectDeletedEvent.builder()
				.id(createNewEventId()).conceptKey(command.getConceptKey()).aspectKey(command.getAspectKey()).build());
	}

	private boolean canDeleteAspect(@NonNull final DeleteAspectCommand command) {
		return getConcept(command.getConceptKey()).map(concept -> concept.getAspect(command.getAspectKey()).isPresent())
				.orElse(false);
	}

	public RevisionId execute(@NonNull final AddAttachmentAspectCommand command) throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(() -> getConcept(command.getConceptKey()).isPresent(),
				() -> AttachmentAspectAddedEvent.builder().id(createNewEventId()).conceptKey(command.getConceptKey())
						.cdnContentUrl(command.getCdnContentUrl()).description(command.getDescription())
						.title(command.getTitle()).key(command.getKey()).build());
	}

	public RevisionId execute(@NonNull final AddCostAspectCommand command) throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(() -> getConcept(command.getConceptKey()).isPresent(),
				() -> CostAspectAddedEvent.builder().id(createNewEventId()).conceptKey(command.getConceptKey())
						.key(command.getKey()).category(command.getCategory()).costPerUnit(command.getCostPerUnit())
						.costType(command.getCostType()).fiscalYear(command.getFiscalYear())
						.numberOfUnits(command.getNumberOfUnits()).unitOfMeasure(command.getUnitOfMeasure())
						.title(command.getTitle()).description(command.getDescription()).build());
	}

	public RevisionId execute(@NonNull final AddFatherTimeAspectCommand command) throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(() -> getConcept(command.getConceptKey()).isPresent(),
				() -> FatherTimeAspectAddedEvent.builder().id(createNewEventId()).key(command.getKey())
						.conceptKey(command.getConceptKey()).pointInTime(command.getPointInTime())
						.title(command.getTitle()).description(command.getDescription())
						.fatherTimeType(command.getFatherTimeType()).build());
	}

	private boolean canSet(@NonNull final SetLifecycleAspectCommand command) {
		return elements.containsKey(command.getConceptKey()) || relationships.containsKey(command.getConceptKey());
	}

	public RevisionId execute(@NonNull final ClearAwsElementCommand command) throws EntityCommandFailedException {
		return conditionallyApplyAndRegisterEvent(() -> true,
				() -> AwsElementClearedEvent.builder().id(createNewEventId()).build());
	}

	protected TownPlanEvent apply(@NonNull final TownPlanEvent event) {
		log.debug("Applying event {}", event);
		final Set<Class<? extends TownPlanEvent>> supportedEvents = Set.of(EnterpriseAddedEvent.class,
				CapabilityAddedEvent.class, BuildingBlockAddedEvent.class, ActorAddedEvent.class,
				TechnologyAddedEvent.class, ITSystemAddedEvent.class, ITContainerAddedEvent.class,
				DataEntityAddedEvent.class, ITContainerTechnologyAddedEvent.class, RelationshipAddedEvent.class,
				FlowViewAddedEvent.class, FlowViewStepAddedEvent.class, FlowViewStepsSetEvent.class,
				FlowViewStepsClearedEvent.class, SwotAspectAddedEvent.class, LifecycleAspectSetEvent.class,
				DocumentationAspectAddedEvent.class, ImpactViewAddedEvent.class, ImpactViewCapabilityAddedEvent.class,
				AwsRegionAddedEvent.class, AwsVpcAddedEvent.class, AwsAvailabilityZoneAddedEvent.class,
				AwsRouteTableAddedEvent.class, AwsNetworkAclAddedEvent.class, AwsSubnetAddedEvent.class,
				AwsSecurityGroupAddedEvent.class, AwsInstanceAddedEvent.class,
				SecurityGroupAssignedToInstanceEvent.class, AwsAccountAddedEvent.class,
				SubnetAssignedToInstanceEvent.class, AwsAutoscalingGroupAddedEvent.class,
				AvailabilityZoneAssignedToAutoScalingGroupEvent.class, SubnetAssignedToAutoScalingGroupEvent.class,
				InstanceAssignedToAutoScalingGroupEvent.class, AwsElasticIpAddressAddedEvent.class,
				AwsElementClearedEvent.class, ItProjectAddedEvent.class, ITSystemUpdatedEvent.class,
				ITContainerUpdatedEvent.class, RelationshipDeletedEvent.class, ActorUpdatedEvent.class,
				EnterpriseUpdatedEvent.class, CapabilityUpdatedEvent.class, BuildingBlockUpdatedEvent.class,
				ItProjectUpdatedEvent.class, RelationshipUpdatedEvent.class, ItProjectMilestoneAddedEvent.class,
				ItProjectMilestoneUpdatedEvent.class, FlowViewUpdatedEvent.class, FlowViewDeletedEvent.class,
				RelationshipTechnologyAddedEvent.class, ElementDeletedEvent.class,
				ITContainerTechnologyDeletedEvent.class, SwotAspectDeletedEvent.class,
				DocumentationAspectDeletedEvent.class, ContentDistributionAspectAddedEvent.class,
				ITSystemIntegrationAddedEvent.class, ITSystemIntegrationUpdatedEvent.class,
				SystemIntegrationStepsSetEvent.class, DataEntityUpdatedEvent.class, ConstraintAspectAddedEvent.class,
				FunctionalRequirementAspectAddedEvent.class, QualityAttributeRequirementAspectAddedEvent.class,
				SecurityConcernAspectAddedEvent.class, SecurityImpactAspectAddedEvent.class,
				ExternalIdAspectDeletedEvent.class, ExternalIdAspectAddedEvent.class,
				ArchitectureVerdictAspectSetEvent.class, AspectDeletedEvent.class, CostAspectAddedEvent.class,
				AttachmentAspectAddedEvent.class, PrincipleAddedEvent.class, PrincipleUpdatedEvent.class,
				DecisionAddedEvent.class, DecisionUpdatedEvent.class, DecisionContextAddedEvent.class,
				DecisionContextUpdatedEvent.class, DecisionOptionAddedEvent.class, DecisionOptionUpdatedEvent.class,
				FatherTimeAspectAddedEvent.class, KeyPointInTimeAddedEvent.class, KeyPointInTimeDeletedEvent.class,
				ITPlatformAddedEvent.class, ITPlatformUpdatedEvent.class, DataEntityFieldAddedEvent.class,
				DataEntityFieldUpdatedEvent.class, ConstraintScoreAspectAddedEvent.class,
				QualityAttributeRequirementScoreAspectAddedEvent.class,
				FunctionalRequirementScoreAspectAddedEvent.class);

		return supportedEvents.stream().filter(eventClass -> eventClass.isInstance(event)).findFirst()
				.map(eventClass -> Pair.of(eventClass, eventClass.cast(event))).map(classAndEvent -> {
					try {
						Method applyMethod = this.getClass().getDeclaredMethod("apply", classAndEvent.getLeft());
						applyMethod.invoke(this, classAndEvent.getRight());
						return classAndEvent.getRight();
					} catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
						log.error("Can't apply event as its apply method does not exist");
						e.printStackTrace();
						return null;
					}
				}).orElse(null);
	}

	private TownPlanEvent apply(@NonNull final ElementDeletedEvent event) {
		return withEntityEvent(event, () -> removeElement(event.getKey()));
	}

	private void removeElement(String elementKey) {
		getElement(Element.class, elementKey).ifPresent(element -> {
			elements.remove(element.getKey());
			element.getIncomingRelationships().forEach(
					relationship -> removeRelationShip(relationship.getSource().getKey(), relationship.getKey()));
			element.getOutgoingRelationships().forEach(
					relationship -> removeRelationShip(relationship.getTarget().getKey(), relationship.getKey()));
			element.getRelationships().forEach(relationship -> this.relationships.remove(relationship.getKey()));
			element.getParent().filter(AbstractElementImpl.class::isInstance).map(AbstractElementImpl.class::cast)
					.ifPresent(parent -> parent.removeChild(element));
			element.getChildren().forEach(child -> removeElement(child.getKey()));
			Optional.of(element).filter(TechnologyImpl.class::isInstance).map(TechnologyImpl.class::cast)
					.ifPresent(technology -> getElements(ItContainerImpl.class)
							.forEach(container -> container.deleteTechnology(technology.getKey())));
			Optional.of(element).filter(ItSystemIntegrationImpl.class::isInstance)
					.map(ItSystemIntegrationImpl.class::cast).ifPresent(integration -> {
						getElement(ItSystemImpl.class, integration.getSourceSystem().getKey())
								.ifPresent(itSystem -> itSystem.removeIntegration(integration));
						getElement(ItSystemImpl.class, integration.getTargetSystem().getKey())
								.ifPresent(itSystem -> itSystem.removeIntegration(integration));
						integration.getIntegrationRelationships().stream().filter(RelationshipImpl.class::isInstance)
								.map(RelationshipImpl.class::cast)
								.forEach(relationship -> relationship.setSystemIntegration(null));
					});
			Optional.of(element).filter(ItSystemImpl.class::isInstance).map(ItSystemImpl.class::cast).ifPresent(
					itSystem -> itSystem.getIntegrations().forEach(integration -> removeElement(integration.getKey())));
		});
	}

	private void removeRelationShip(String elementKey, String relationshipKey) {
		RelationshipImpl relationship = relationships.get(relationshipKey);
		if (relationship != null) {
			getElement(AbstractElementImpl.class, elementKey)
					.ifPresent(element -> element.removeRelationship(relationship));
		}
	}

	private TownPlanEvent apply(@NonNull final KeyPointInTimeAddedEvent event) {
		return withEntityEvent(event, () -> {
			KeyPointInTimeImpl point = KeyPointInTimeImpl.builder().date(event.getDate()).title(event.getTitle())
					.diagramsNeeded(event.isDiagramsNeeded()).build();
			this.keyPointsInTime.put(event.getDate(), point);
		});
	}

	private TownPlanEvent apply(@NonNull final KeyPointInTimeDeletedEvent event) {
		return withEntityEvent(event, () -> {
			this.keyPointsInTime.remove(event.getDate());
		});
	}

	private TownPlanEvent apply(@NonNull final EnterpriseAddedEvent event) {
		return withEntityEvent(event, () -> {
			EnterpriseImpl enterprise = EnterpriseImpl.builder().key(event.getKey()).title(event.getTitle())
					.description(event.getDescription()).build();
			this.elements.put(enterprise.getKey(), enterprise);
		});
	}

	private TownPlanEvent apply(@NonNull final PrincipleAddedEvent event) {
		return withEntityEvent(event,
				() -> getElement(EnterpriseImpl.class, event.getEnterprise()).ifPresent(enterprise -> {
					PrincipleImpl principle = PrincipleImpl.builder().key(event.getKey()).type(event.getType())
							.enterprise(enterprise).title(event.getTitle()).description(event.getDescription())
							.sortKey(event.getSortKey()).build();
					this.elements.put(principle.getKey(), principle);
					enterprise.addChild(principle);
				}));
	}

	private TownPlanEvent apply(@NonNull final DecisionAddedEvent event) {
		return withEntityEvent(event,
				() -> getElement(EnterpriseImpl.class, event.getEnterprise()).ifPresent(enterprise -> {
					DecisionImpl decision = DecisionImpl.builder().key(event.getKey()).title(event.getTitle())
							.enterprise(enterprise).description(event.getDescription()).outcome(event.getOutcome())
							.status(event.getStatus()).build();
					this.elements.put(decision.getKey(), decision);
					enterprise.addChild(decision);
				}));
	}

	private TownPlanEvent apply(@NonNull final DecisionContextAddedEvent event) {
		return withEntityEvent(event, () -> getElement(DecisionImpl.class, event.getDecision()).ifPresent(decision -> {
			DecisionContextImpl context = DecisionContextImpl.builder().parentElement(decision).key(event.getKey())
					.sortKey(event.getSortKey()).title(event.getTitle()).description(event.getDescription())
					.contextType(event.getContextType()).build();
			this.elements.put(context.getKey(), context);
			decision.addChild(context);
		}));
	}

	private TownPlanEvent apply(@NonNull final DecisionOptionAddedEvent event) {
		return withEntityEvent(event, () -> getElement(DecisionImpl.class, event.getDecision()).ifPresent(decision -> {
			DecisionOptionImpl option = DecisionOptionImpl.builder().parentElement(decision).key(event.getKey())
					.sortKey(event.getSortKey()).title(event.getTitle()).description(event.getDescription())
					.verdict(event.getVerdict()).build();
			this.elements.put(option.getKey(), option);
			decision.addChild(option);
		}));
	}

	private TownPlanEvent apply(@NonNull final PrincipleUpdatedEvent event) {
		return withEntityEvent(event, () -> getElement(PrincipleImpl.class, event.getKey()).ifPresent(principle -> {
			principle.setType(event.getType());
			principle.setTitle(event.getTitle());
			principle.setDescription(event.getDescription());
			principle.setSortKey(event.getSortKey());
		}));
	}

	private TownPlanEvent apply(@NonNull final DecisionUpdatedEvent event) {
		return withEntityEvent(event, () -> getElement(DecisionImpl.class, event.getKey()).ifPresent(decision -> {
			decision.setOutcome(event.getOutcome());
			decision.setStatus(event.getStatus());
			decision.setTitle(event.getTitle());
			decision.setDescription(event.getDescription());
		}));
	}

	private TownPlanEvent apply(@NonNull final DecisionContextUpdatedEvent event) {
		return withEntityEvent(event, () -> getElement(DecisionContextImpl.class, event.getKey()).ifPresent(context -> {
			context.setSortKey(event.getSortKey());
			context.setDescription(event.getDescription());
			context.setTitle(event.getTitle());
			context.setContextType(event.getContextType());
		}));
	}

	private TownPlanEvent apply(@NonNull final DecisionOptionUpdatedEvent event) {
		return withEntityEvent(event, () -> getElement(DecisionOptionImpl.class, event.getKey()).ifPresent(option -> {
			option.setSortKey(event.getSortKey());
			option.setDescription(event.getDescription());
			option.setTitle(event.getTitle());
			option.setVerdict(event.getVerdict());
		}));
	}

	private TownPlanEvent apply(@NonNull final EnterpriseUpdatedEvent event) {
		return withEntityEvent(event, () -> getElement(EnterpriseImpl.class, event.getKey()).ifPresent(enterprise -> {
			enterprise.setTitle(event.getTitle());
			enterprise.setDescription(event.getDescription());
		}));
	}

	private TownPlanEvent apply(@NonNull final AspectDeletedEvent event) {
		return withEntityEvent(event, () -> getConceptImpl(event.getConceptKey())
				.ifPresent(concept -> concept.removeAspect(event.getAspectKey())));
	}

	private TownPlanEvent apply(@NonNull final AttachmentAspectAddedEvent event) {
		return withEntityEvent(event,
				() -> getConceptImpl(event.getConceptKey()).ifPresent(concept -> concept.addAspect(AttachmentImpl
						.builder().cdnContentUrl(event.getCdnContentUrl()).description(event.getDescription())
						.key(event.getKey()).title(event.getTitle()).build())));
	}

	private TownPlanEvent apply(@NonNull final CostAspectAddedEvent event) {
		return withEntityEvent(event,
				() -> getConceptImpl(event.getConceptKey()).ifPresent(concept -> concept.addAspect(CostImpl.builder()
						.category(event.getCategory()).costPerUnit(event.getCostPerUnit()).costType(event.getCostType())
						.description(event.getDescription()).fiscalYear(event.getFiscalYear()).key(event.getKey())
						.numberOfUnits(event.getNumberOfUnits()).title(event.getTitle())
						.unitOfMeasure(event.getUnitOfMeasure()).build())));
	}

	private TownPlanEvent apply(@NonNull final FatherTimeAspectAddedEvent event) {
		return withEntityEvent(event,
				() -> getConceptImpl(event.getConceptKey()).ifPresent(
						concept -> concept.addAspect(FatherTimeImpl.builder().fatherTimeType(event.getFatherTimeType())
								.key(event.getKey()).description(event.getDescription())
								.pointInTime(event.getPointInTime()).title(event.getTitle()).build())));
	}

	private TownPlanEvent apply(@NonNull final ItProjectAddedEvent event) {
		return withEntityEvent(event, () -> {
			Optional<EnterpriseImpl> enterprise = getElement(EnterpriseImpl.class, event.getEnterprise());
			ItProjectImpl project = ItProjectImpl.builder().key(event.getKey()).title(event.getTitle())
					.description(event.getDescription()).parentElement(enterprise.orElse(null)).type(event.getType())
					.build();
			this.elements.put(project.getKey(), project);
			enterprise.ifPresent(e -> e.addChild(project));
		});
	}

	private TownPlanEvent apply(@NonNull final ItProjectUpdatedEvent event) {
		return withEntityEvent(event, () -> getElement(ItProjectImpl.class, event.getKey()).ifPresent(project -> {
			project.setDescription(event.getDescription());
			project.setTitle(event.getTitle());
			project.setType(event.getType());
			getElement(EnterpriseImpl.class, project.getEnterprise() == null ? "" : project.getEnterprise().getKey())
					.ifPresent(currentlyAttachedEnterprise -> currentlyAttachedEnterprise.removeChild(project));
			getElement(EnterpriseImpl.class, event.getEnterprise()).ifPresent(newEnterpriseToAttach -> {
				project.setParentElement(newEnterpriseToAttach);
				newEnterpriseToAttach.addChild(project);
			});
		}));
	}

	private TownPlanEvent apply(@NonNull final ItProjectMilestoneAddedEvent event) {
		return withEntityEvent(event, () -> getElement(ItProjectImpl.class, event.getProject()).ifPresent(project -> {
			ItProjectMilestoneImpl milestone = ItProjectMilestoneImpl.builder().key(event.getKey())
					.title(event.getTitle()).description(event.getDescription()).sortKey(event.getSortKey())
					.parentElement(project).build();
			log.debug("milestone {}", milestone);
			this.elements.put(milestone.getKey(), milestone);
			project.addChild(milestone);
		}));
	}

	private TownPlanEvent apply(@NonNull final ItProjectMilestoneUpdatedEvent event) {
		return withEntityEvent(event,
				() -> getElement(ItProjectMilestoneImpl.class, event.getKey()).ifPresent(milestone -> {
					milestone.setDescription(event.getDescription());
					milestone.setTitle(event.getTitle());
					milestone.setSortKey(event.getSortKey());
					getElement(ItProjectImpl.class, milestone.getProject().getKey())
							.ifPresent(currentlyAttachedProject -> currentlyAttachedProject.removeChild(milestone));
					getElement(ItProjectImpl.class, event.getProject()).ifPresent(newProjectToAttach -> {
						milestone.setParentElement(newProjectToAttach);
						newProjectToAttach.addChild(milestone);
					});
				}));
	}

	private TownPlanEvent apply(final @NonNull CapabilityAddedEvent event) {
		return withEntityEvent(event, () -> {
			Optional<BusinessCapabilityImpl> parent = getElement(BusinessCapabilityImpl.class, event.getParent());

			BusinessCapabilityImpl businessCapability = BusinessCapabilityImpl.builder().key(event.getKey())
					.title(event.getTitle()).description(event.getDescription())
					.enterprise(getElement(EnterpriseImpl.class, event.getEnterprise()).orElse(null))
					.parentElement(parent.orElse(null))
					.sortKey(event.getSortKey() == null ? event.getKey() : event.getSortKey()).build();

			this.elements.put(businessCapability.getKey(), businessCapability);
			parent.ifPresent(p -> p.addChild(businessCapability));
			getElement(EnterpriseImpl.class, businessCapability.getEnterprise().getKey()).ifPresent(enterprise -> {
				if (businessCapability.getLevel() == 0)
					enterprise.addChild(businessCapability);
			});
		});
	}

	private TownPlanEvent apply(final @NonNull CapabilityUpdatedEvent event) {
		return withEntityEvent(event,
				() -> getElement(BusinessCapabilityImpl.class, event.getKey()).ifPresent(capability -> {
					capability.setSortKey(event.getSortKey());
					capability.setTitle(event.getTitle());
					capability.setDescription(event.getDescription());
					getElement(EnterpriseImpl.class,
							capability.getEnterprise() == null ? "" : capability.getEnterprise().getKey()).ifPresent(
									currentlyAttachedEnterprise -> currentlyAttachedEnterprise.removeChild(capability));
					getElement(EnterpriseImpl.class, event.getEnterprise()).ifPresent(newEnterpriseToAttach -> {
						newEnterpriseToAttach.addChild(capability);
						capability.setEnterprise(newEnterpriseToAttach);
					});
					getElement(BusinessCapabilityImpl.class,
							capability.getParentCapability().map(BusinessCapability::getKey).orElse("")).ifPresent(
									currentlyAttachedParent -> currentlyAttachedParent.removeChild(capability));
					getElement(BusinessCapabilityImpl.class, event.getParent()).ifPresent(newParentToAttach -> {
						newParentToAttach.addChild(capability);
						capability.setParentElement(newParentToAttach);
					});
				}));
	}

	private TownPlanEvent apply(final @NonNull BuildingBlockAddedEvent event) {
		return withEntityEvent(event, () -> {
			ArchitectureBuildingBlockImpl architectureBuildingBlock = ArchitectureBuildingBlockImpl.builder()
					.key(event.getKey()).title(event.getTitle()).description(event.getDescription())
					.enterprise(getElement(EnterpriseImpl.class, event.getEnterprise()).orElse(null)).build();

			this.elements.put(architectureBuildingBlock.getKey(), architectureBuildingBlock);
		});
	}

	private TownPlanEvent apply(final @NonNull BuildingBlockUpdatedEvent event) {
		return withEntityEvent(event,
				() -> getElement(ArchitectureBuildingBlockImpl.class, event.getKey()).ifPresent(buildingBlock -> {
					buildingBlock.setDescription(event.getDescription());
					buildingBlock.setTitle(event.getTitle());
					getElement(EnterpriseImpl.class,
							buildingBlock.getEnterprise() == null ? "" : buildingBlock.getEnterprise().getKey())
									.ifPresent(currentlyAttachedEnterprise -> currentlyAttachedEnterprise
											.removeChild(buildingBlock));
					getElement(EnterpriseImpl.class, event.getEnterprise()).ifPresent(newEnterpriseToAttach -> {
						newEnterpriseToAttach.addChild(buildingBlock);
						buildingBlock.setEnterprise(newEnterpriseToAttach);
					});
				}));
	}

	private TownPlanEvent apply(final @NonNull ActorAddedEvent event) {
		return withEntityEvent(event,
				() -> getElement(EnterpriseImpl.class, event.getEnterprise()).ifPresent(enterprise -> {
					BusinessActorImpl businessActor = BusinessActorImpl.builder().key(event.getKey())
							.title(event.getTitle()).description(event.getDescription()).actorType(event.getType())
							.enterprise(enterprise).build();

					this.elements.put(businessActor.getKey(), businessActor);
					enterprise.addChild(businessActor);
				}));
	}

	private TownPlanEvent apply(final @NonNull ActorUpdatedEvent event) {
		return withEntityEvent(event, () -> getElement(BusinessActorImpl.class, event.getKey()).ifPresent(actor -> {
			actor.setActorType(event.getType());
			actor.setDescription(event.getDescription());
			actor.setTitle(event.getTitle());
			getElement(EnterpriseImpl.class, event.getEnterprise()).ifPresent(newEnterpriseToAttach -> {
				getElement(EnterpriseImpl.class, actor.getEnterprise() == null ? "" : actor.getEnterprise().getKey())
						.ifPresent(currentlyAttachedEnterprise -> currentlyAttachedEnterprise.removeChild(actor));
				actor.setEnterprise(newEnterpriseToAttach);
				newEnterpriseToAttach.addChild(actor);
			});
		}));
	}

	private TownPlanEvent apply(final @NonNull TechnologyAddedEvent event) {
		return withEntityEvent(event, () -> {
			TechnologyImpl technology = TechnologyImpl.builder().key(event.getKey()).title(event.getTitle())
					.description(event.getDescription()).type(event.getType()).recommendation(event.getRecommendation())
					.technologyType(event.getTechnologyType()).build();

			this.elements.put(technology.getKey(), technology);
		});
	}

	private TownPlanEvent apply(final @NonNull ITPlatformAddedEvent event) {
		return withEntityEvent(event, () -> {
			ItPlatformImpl itPlatform = ItPlatformImpl.builder().key(event.getKey()).title(event.getTitle())
					.description(event.getDescription()).build();

			this.elements.put(itPlatform.getKey(), itPlatform);
		});
	}

	private TownPlanEvent apply(final @NonNull ITSystemAddedEvent event) {
		return withEntityEvent(event, () -> {
			ItPlatformImpl platform = getElement(ItPlatformImpl.class,
					Optional.ofNullable(event.getPlatform()).orElse("")).orElse(null);
			ItSystemImpl itSystem = ItSystemImpl.builder().key(event.getKey()).title(event.getTitle())
					.description(event.getDescription()).parentElement(platform).build();

			this.elements.put(itSystem.getKey(), itSystem);
			if (platform != null)
				platform.addChild(itSystem);
		});
	}

	private TownPlanEvent apply(final @NonNull ITSystemIntegrationAddedEvent event) {
		return withEntityEvent(event, () -> getElement(ItSystemImpl.class, event.getSourceSystem()).ifPresent(
				sourceSystem -> getElement(ItSystemImpl.class, event.getTargetSystem()).ifPresent(targetSystem -> {
					ItSystemIntegrationImpl integration = ItSystemIntegrationImpl.builder().key(event.getKey())
							.title(event.getTitle()).description(event.getDescription()).sourceSystem(sourceSystem)
							.targetSystem(targetSystem).criticality(event.getCriticality()).volume(event.getVolume())
							.frequency(event.getFrequency()).criticalityDescription(event.getCriticalityDescription())
							.resilience(event.getResilience()).build();
					this.elements.put(integration.getKey(), integration);
					sourceSystem.addIntegration(integration);
					targetSystem.addIntegration(integration);
				})));
	}

	private TownPlanEvent apply(final @NonNull ITSystemIntegrationUpdatedEvent event) {
		return withEntityEvent(event,
				() -> getElement(ItSystemIntegrationImpl.class, event.getKey()).ifPresent(integration -> {
					integration.setTitle(event.getTitle());
					integration.setDescription(event.getDescription());
					integration.setCriticality(event.getCriticality());
					integration.setVolume(event.getVolume());
					integration.setFrequency(event.getFrequency());
					integration.setResilience(event.getResilience());
					integration.setCriticalityDescription(event.getCriticalityDescription());
				}));
	}

	private TownPlanEvent apply(final @NonNull ITSystemUpdatedEvent event) {
		return withEntityEvent(event, () -> getElement(ItSystemImpl.class, event.getKey()).ifPresent(itSystem -> {
			itSystem.setTitle(event.getTitle());
			itSystem.setDescription(event.getDescription());

			itSystem.getPlatform().filter(ItPlatformImpl.class::isInstance).map(ItPlatformImpl.class::cast)
					.ifPresent(existingPlatform -> {
						existingPlatform.removeChild(itSystem);
					});

			itSystem.setParentElement(null);

			getElement(ItPlatformImpl.class, Optional.ofNullable(event.getPlatform()).orElse(""))
					.ifPresent(platform -> {
						platform.addChild(itSystem);
						itSystem.setParentElement(platform);
					});
		}));
	}

	private TownPlanEvent apply(final @NonNull ITPlatformUpdatedEvent event) {
		return withEntityEvent(event, () -> getElement(ItPlatformImpl.class, event.getKey()).ifPresent(itPlatform -> {
			itPlatform.setTitle(event.getTitle());
			itPlatform.setDescription(event.getDescription());
		}));
	}

	private TownPlanEvent apply(final @NonNull ITContainerAddedEvent event) {
		return withEntityEvent(event, () -> getElement(ItSystemImpl.class, event.getSystem()).ifPresent(system -> {
			ItContainerImpl container = ItContainerImpl.builder().key(event.getKey()).title(event.getTitle())
					.description(event.getDescription()).containerType(event.getType())
					.parentElement(getElement(ItSystemImpl.class, event.getSystem()).orElse(null)).build();

			this.elements.put(container.getKey(), container);
			system.addChild(container);
		}));
	}

	private TownPlanEvent apply(final @NonNull ITContainerUpdatedEvent event) {
		return withEntityEvent(event, () -> getElement(ItContainerImpl.class, event.getKey()).ifPresent(itContainer -> {
			itContainer.setContainerType(event.getType());
			itContainer.setDescription(event.getDescription());
			itContainer.setTitle(event.getTitle());
			String currentlyAttachedSystemKey = itContainer.getSystem() == null ? "" : itContainer.getSystem().getKey();
			getElement(ItSystemImpl.class, event.getSystem()).ifPresent(newSystemToAttach -> {
				getElement(ItSystemImpl.class, currentlyAttachedSystemKey)
						.ifPresent(currentlyAttachedSystem -> currentlyAttachedSystem.removeChild(itContainer));
				itContainer.setParentElement(newSystemToAttach);
				newSystemToAttach.addChild(itContainer);
			});
		}));
	}

	private TownPlanEvent apply(final @NonNull DataEntityUpdatedEvent event) {
		return withEntityEvent(event, () -> getElement(DataEntityImpl.class, event.getKey()).ifPresent(entity -> {
			if (entity.getMasterSystem() != null)
				getElement(ItSystemImpl.class, entity.getMasterSystem().getKey())
						.ifPresent(system -> system.removeChild(entity));
			if (entity.getBusinessCapability() != null)
				getElement(BusinessCapabilityImpl.class, entity.getBusinessCapability().getKey())
						.ifPresent(businessCapability -> businessCapability.removeChild(entity));
			if (entity.getEnterprise() != null)
				getElement(EnterpriseImpl.class, entity.getEnterprise().getKey())
						.ifPresent(enterprise -> enterprise.removeChild(entity));

			entity.setTitle(event.getTitle());
			entity.setDescription(event.getDescription());
			entity.setEntityType(event.getEntityType());
			entity.setConsentNeeded(event.isConsentNeeded());
			entity.setSensitivity(event.getSensitivity());
			entity.setCommercialValue(event.getCommercialValue());
			entity.setEnterprise(getElement(EnterpriseImpl.class, event.getEnterprise()).orElse(null));
			entity.setMasterSystem(getElement(ItSystemImpl.class, event.getMasterSystem()).orElse(null));
			entity.setBusinessCapability(
					getElement(BusinessCapabilityImpl.class, event.getBusinessCapability()).orElse(null));
			getElement(ItSystemImpl.class, event.getMasterSystem()).ifPresent(system -> system.addChild(entity));
			getElement(BusinessCapabilityImpl.class, event.getBusinessCapability())
					.ifPresent(businessCapability -> businessCapability.addChild(entity));
			getElement(EnterpriseImpl.class, event.getEnterprise())
					.ifPresent(enterprise -> enterprise.addChild(entity));
		}));
	}

	private TownPlanEvent apply(final @NonNull DataEntityFieldAddedEvent event) {
		return withEntityEvent(event, () -> {
			getElement(DataEntityImpl.class, event.getDataEntityKey()).ifPresent(dataEntity -> {
				DataEntityFieldImpl field = DataEntityFieldImpl.builder().key(event.getKey()).title(event.getTitle())
						.description(event.getDescription()).type(event.getType()).defaultValue(event.getDefaultValue())
						.fieldConstraints(event.getFieldConstraints()).localization(event.getLocalization())
						.mandatory(event.isMandatory()).unique(event.isUnique()).sortKey(event.getSortKey())
						.parentElement(dataEntity).build();
				this.elements.put(field.getKey(), field);

				dataEntity.addChild(field);
			});
		});
	}

	private TownPlanEvent apply(final @NonNull DataEntityFieldUpdatedEvent event) {
		return withEntityEvent(event, () -> {
			getElement(DataEntityFieldImpl.class, event.getKey()).ifPresent(field -> {
				field.setFieldConstraints(event.getFieldConstraints());
				field.setLocalization(event.getLocalization());
				field.setMandatory(event.isMandatory());
				field.setUnique(event.isUnique());
				field.setType(event.getType());
				field.setDefaultValue(event.getDefaultValue());
				field.setDescription(event.getDescription());
				field.setTitle(event.getTitle());
				field.setSortKey(event.getSortKey());
			});
		});
	}

	private TownPlanEvent apply(final @NonNull DataEntityAddedEvent event) {
		return withEntityEvent(event, () -> {
			DataEntityImpl entity = DataEntityImpl.builder().key(event.getKey()).title(event.getTitle())
					.description(event.getDescription()).entityType(event.getEntityType())
					.consentNeeded(event.isConsentNeeded()).sensitivity(event.getSensitivity())
					.commercialValue(event.getCommercialValue())
					.enterprise(getElement(EnterpriseImpl.class, event.getEnterprise()).orElse(null))
					.masterSystem(getElement(ItSystemImpl.class, event.getMasterSystem()).orElse(null))
					.businessCapability(
							getElement(BusinessCapabilityImpl.class, event.getBusinessCapability()).orElse(null))
					.build();

			this.elements.put(entity.getKey(), entity);

			log.debug(entity.getEnterprise());
			log.debug(entity.getMasterSystem());
			log.debug(entity.getBusinessCapability());
			getElement(ItSystemImpl.class, event.getMasterSystem()).ifPresent(system -> system.addChild(entity));
			getElement(BusinessCapabilityImpl.class, event.getBusinessCapability())
					.ifPresent(businessCapability -> businessCapability.addChild(entity));
			getElement(EnterpriseImpl.class, event.getEnterprise())
					.ifPresent(enterprise -> enterprise.addChild(entity));
		});
	}

	private TownPlanEvent apply(final @NonNull ITContainerTechnologyAddedEvent event) {
		return withEntityEvent(event, () -> {
			final Optional<ItContainerImpl> container = getElement(ItContainerImpl.class, event.getContainer());
			final Optional<TechnologyImpl> technology = getElement(TechnologyImpl.class, event.getTechnology());

			container.ifPresent(c -> technology.ifPresent(c::addTechnology));
		});
	}

	private TownPlanEvent apply(final @NonNull RelationshipTechnologyAddedEvent event) {
		return withEntityEvent(event,
				() -> getRelationship(event.getRelationship()).filter(RelationshipImpl.class::isInstance)
						.map(RelationshipImpl.class::cast)
						.ifPresent(relationship -> getElement(TechnologyImpl.class, event.getTechnology())
								.ifPresent(relationship::addTechnology)));
	}

	private TownPlanEvent apply(final @NonNull AwsRegionAddedEvent event) {
		return withEntityEvent(event, () -> {
			AwsRegionImpl region = AwsRegionImpl.builder().key(event.getKey()).title(event.getTitle())
					.description(event.getDescription()).build();
			this.elements.put(region.getKey(), region);
		});
	}

	private TownPlanEvent apply(final @NonNull AwsVpcAddedEvent event) {
		return withEntityEvent(event, () -> getElement(AwsAccountImpl.class, event.getAccount())
				.ifPresent(account -> getElement(AwsRegionImpl.class, event.getRegion()).ifPresent(awsRegion -> {
					AwsVpcImpl vpc = AwsVpcImpl.builder().key(event.getKey()).title(event.getTitle())
							.description(event.getDescription()).networkMask(event.getNetworkMask()).region(awsRegion)
							.account(account).build();

					this.elements.put(vpc.getKey(), vpc);
					awsRegion.addChild(vpc);
					account.addChild(vpc);
				})));
	}

	private TownPlanEvent apply(final @NonNull AwsAvailabilityZoneAddedEvent event) {
		return withEntityEvent(event, () -> getElement(AwsRegionImpl.class, event.getRegion()).ifPresent(awsRegion -> {
			AwsAvailabilityZoneImpl az = AwsAvailabilityZoneImpl.builder().key(event.getKey()).title(event.getTitle())
					.description(event.getDescription()).region(awsRegion).build();
			this.elements.put(az.getKey(), az);
			awsRegion.addChild(az);
		}));
	}

	private TownPlanEvent apply(final @NonNull AwsRouteTableAddedEvent event) {
		return withEntityEvent(event, () -> getElement(AwsVpcImpl.class, event.getVpc()).ifPresent(vpc -> {
			AwsRouteTableImpl routeTable = AwsRouteTableImpl.builder().key(event.getKey()).title(event.getTitle())
					.description(event.getDescription()).vpc(vpc).build();
			this.elements.put(routeTable.getKey(), routeTable);
			vpc.addChild(routeTable);
		}));
	}

	private TownPlanEvent apply(final @NonNull AwsNetworkAclAddedEvent event) {
		return withEntityEvent(event, () -> getElement(AwsVpcImpl.class, event.getVpc()).ifPresent(vpc -> {
			AwsNetworkAclImpl acl = AwsNetworkAclImpl.builder().key(event.getKey()).title(event.getTitle())
					.description(event.getDescription()).vpc(vpc).build();
			this.elements.put(acl.getKey(), acl);
			vpc.addChild(acl);
		}));
	}

	private TownPlanEvent apply(final @NonNull AwsSubnetAddedEvent event) {
		return withEntityEvent(event, () -> getElement(AwsVpcImpl.class, event.getVpc()).ifPresent(
				vpc -> getElement(AwsAvailabilityZoneImpl.class, event.getAvailabilityZone()).ifPresent(az -> {
					AwsSubnetImpl subnet = AwsSubnetImpl.builder().key(event.getKey()).title(event.getTitle())
							.description(event.getDescription()).availabilityZone(az).vpc(vpc)
							.subnetMask(event.getSubnetMask()).publicSubnet(event.isPublicSubnet()).build();
					this.elements.put(subnet.getKey(), subnet);
					vpc.addChild(subnet);
				})));
	}

	private TownPlanEvent apply(final @NonNull AwsSecurityGroupAddedEvent event) {
		return withEntityEvent(event, () -> getElement(AwsVpcImpl.class, event.getVpc()).ifPresent(vpc -> {
			AwsSecurityGroupImpl securityGroup = AwsSecurityGroupImpl.builder().key(event.getKey())
					.title(event.getTitle()).description(event.getDescription()).vpc(vpc).build();

			this.elements.put(securityGroup.getKey(), securityGroup);
			vpc.addChild(securityGroup);
		}));
	}

	private TownPlanEvent apply(final @NonNull AwsInstanceAddedEvent event) {
		return withEntityEvent(event, () -> getElement(AwsAccountImpl.class, event.getAccount()).ifPresent(account -> {
			Optional<AwsVpcImpl> vpc = getElement(AwsVpcImpl.class, event.getVpc() == null ? "" : event.getVpc());
			Optional<AwsRegionImpl> region = getElement(AwsRegionImpl.class,
					event.getRegion() == null ? "" : event.getRegion());
			AwsInstanceImpl instance = AwsInstanceImpl.builder().key(event.getKey()).title(event.getTitle())
					.description(event.getDescription()).account(account).vpc(vpc.orElse(null))
					.awsInstanceType(event.getInstanceType()).region(region.orElse(null)).build();
			elements.put(instance.getKey(), instance);
			account.addChild(instance);
			vpc.ifPresent(v -> v.addChild(instance));
			region.ifPresent(r -> r.addChild(instance));
		}));
	}

	private TownPlanEvent apply(final @NonNull SecurityGroupAssignedToInstanceEvent event) {
		return withEntityEvent(event,
				() -> getElement(AwsSecurityGroupImpl.class, event.getSecurityGroupKey())
						.ifPresent(awsSecurityGroup -> getElement(AwsInstanceImpl.class, event.getInstanceKey())
								.ifPresent(instance -> {
									awsSecurityGroup.addChild(instance);
									instance.addChild(awsSecurityGroup);
								})));
	}

	private TownPlanEvent apply(final @NonNull SubnetAssignedToInstanceEvent event) {
		return withEntityEvent(event, () -> getElement(AwsSubnetImpl.class, event.getSubnetKey()).ifPresent(
				awsSubnet -> getElement(AwsInstanceImpl.class, event.getInstanceKey()).ifPresent(awsInstance -> {
					awsSubnet.addChild(awsInstance);
					awsInstance.addChild(awsSubnet);
				})));
	}

	private TownPlanEvent apply(final @NonNull AwsAccountAddedEvent event) {
		return withEntityEvent(event, () -> {
			AwsAccountImpl account = AwsAccountImpl.builder().key(event.getKey()).title(event.getTitle())
					.accountId(event.getAccountId()).description(event.getDescription()).build();
			elements.put(account.getKey(), account);
		});
	}

	private TownPlanEvent apply(final @NonNull AwsAutoscalingGroupAddedEvent event) {
		return withEntityEvent(event, () -> getElement(AwsVpcImpl.class, event.getVpc()).ifPresent(vpc -> {
			AwsAutoscalingGroupImpl autoscalingGroup = AwsAutoscalingGroupImpl.builder().key(event.getKey())
					.title(event.getTitle()).vpc(vpc).title(event.getTitle()).description(event.getDescription())
					.build();
			elements.put(autoscalingGroup.getKey(), autoscalingGroup);
			vpc.addChild(autoscalingGroup);
		}));
	}

	private TownPlanEvent apply(final @NonNull SwotAspectAddedEvent event) {
		return withEntityEvent(event, () -> {
			final AbstractElementImpl element = elements.get(event.getConceptKey());
			final RelationshipImpl relationship = relationships.get(event.getConceptKey());
			final AbstractConceptImpl concept = element == null ? relationship : element;
			if (concept != null) {
				SWOTImpl swot = SWOTImpl.builder().type(event.getType()).description(event.getDescription()).build();
				concept.addAspect(swot);
			}
		});
	}

	private TownPlanEvent apply(final @NonNull SwotAspectDeletedEvent event) {
		return withEntityEvent(event, () -> getConceptImpl(event.getConceptKey())
				.ifPresent(concept -> concept.removeAspect(event.getSwotKey())));
	}

	private TownPlanEvent apply(final @NonNull ITContainerTechnologyDeletedEvent event) {
		return withEntityEvent(event, () -> getElement(ItContainerImpl.class, event.getContainer())
				.ifPresent(container -> container.deleteTechnology(event.getTechnology())));
	}

	private TownPlanEvent apply(final @NonNull AvailabilityZoneAssignedToAutoScalingGroupEvent event) {
		return withEntityEvent(event,
				() -> getElement(AwsAvailabilityZoneImpl.class, event.getAvailabilityZone())
						.ifPresent(az -> getElement(AwsAutoscalingGroupImpl.class, event.getAutoScalingGroup())
								.ifPresent(autoscaling -> autoscaling.addChild(az))));
	}

	private TownPlanEvent apply(final @NonNull SubnetAssignedToAutoScalingGroupEvent event) {
		return withEntityEvent(event,
				() -> getElement(AwsSubnet.class, event.getSubnet())
						.ifPresent(subnet -> getElement(AwsAutoscalingGroupImpl.class, event.getAutoScalingGroup())
								.ifPresent(autoscaling -> autoscaling.addChild(subnet))));
	}

	private TownPlanEvent apply(final @NonNull InstanceAssignedToAutoScalingGroupEvent event) {
		return withEntityEvent(event,
				() -> getElement(AwsInstance.class, event.getInstance())
						.ifPresent(instance -> getElement(AwsAutoscalingGroupImpl.class, event.getAutoscalingGroup())
								.ifPresent(autoscaling -> autoscaling.addChild(instance))));
	}

	private TownPlanEvent apply(final @NonNull AwsElasticIpAddressAddedEvent event) {
		return withEntityEvent(event, () -> getElement(AwsAccountImpl.class, event.getAccount()).ifPresent(account -> {
			Optional<AwsInstanceImpl> instanceOptional = getElement(AwsInstanceImpl.class, event.getInstance());
			AwsElasticIpAddressImpl ip = AwsElasticIpAddressImpl.builder().key(event.getKey()).title(event.getTitle())
					.description(event.getDescription()).allocationId(event.getAllocationId())
					.associationId(event.getAssociationId()).carrierIp(event.getCarrierIp())
					.customerOwnedIp(event.getCustomerOwnedIp()).customerOwnedIpv4Pool(event.getCustomerOwnedIpv4Pool())
					.domain(event.getDomain()).instanceId(event.getInstanceId())
					.networkBorderGroup(event.getNetworkBorderGroup()).networkInterfaceId(event.getNetworkInterfaceId())
					.networkInterfaceOwnerId(event.getNetworkInterfaceOwnerId())
					.privateIpAddress(event.getPrivateIpAddress()).publicIpAddress(event.getPublicIpAddress())
					.publicIpv4Pool(event.getPublicIpv4Pool()).region(event.getRegion()).account(account)
					.instance(instanceOptional.orElse(null)).build();

			elements.put(ip.getKey(), ip);
		}));
	}

	private TownPlanEvent apply(final @NonNull ExternalIdAspectAddedEvent event) {
		return withEntityEvent(event,
				() -> getConceptImpl(event.getConceptKey()).ifPresent(
						concept -> concept.addAspect(ExternalIdImpl.builder().key(UUID.randomUUID().toString())
								.value(event.getValue()).externalIdType(event.getType()).build())));
	}

	private TownPlanEvent apply(final @NonNull ExternalIdAspectDeletedEvent event) {
		return withEntityEvent(event,
				() -> getConceptImpl(event.getConceptKey()).ifPresent(concept -> concept.getExternalIds().stream()
						.filter(externalId -> Objects.equals(externalId.getExternalIdType(), event.getType())
								&& Objects.equals(externalId.getValue(), event.getValue()))
						.findFirst().ifPresent(externalId -> concept.removeAspect(externalId.getKey()))));
	}

	private TownPlanEvent apply(final @NonNull ConstraintAspectAddedEvent event) {
		return withEntityEvent(event,
				() -> getConceptImpl(event.getModelComponentKey()).ifPresent(concept -> concept.addAspect(ConstraintImpl
						.builder().description(event.getDescription()).key(event.getKey()).title(event.getTitle())
						.sortKey(event.getSortKey()).weight(event.getWeight()).build())));
	}

	private TownPlanEvent apply(final @NonNull ConstraintScoreAspectAddedEvent event) {
		return withEntityEvent(event,
				() -> getConceptImpl(event.getModelComponentKey())
						.ifPresent(concept -> concept.getConstraint(event.getConstraintKey())
								.ifPresent(constraint -> concept.addAspect(ConstraintScoreImpl.builder()
										.constraint(constraint).description(event.getDescription())
										.weight(event.getWeight()).key(event.getKey()).build()))));
	}

	private TownPlanEvent apply(final @NonNull QualityAttributeRequirementScoreAspectAddedEvent event) {
		return withEntityEvent(event,
				() -> getConceptImpl(event.getModelComponentKey()).ifPresent(concept -> concept
						.getQualityAttributeRequirement(event.getQualityAttributeRequirementKey())
						.ifPresent(requirement -> concept.addAspect(QualityAttributeRequirementScoreImpl.builder()
								.qualityAttributeRequirement(requirement).description(event.getDescription())
								.weight(event.getWeight()).key(event.getKey()).build()))));
	}

	private TownPlanEvent apply(final @NonNull FunctionalRequirementScoreAspectAddedEvent event) {
		return withEntityEvent(event, () -> getConceptImpl(event.getModelComponentKey()).ifPresent(concept -> {
			log.debug("concept exists, adding functional requirement score");
			concept.getFunctionalRequirement(event.getFunctionalRequirementKey()).ifPresent(requirement -> {
				log.debug("requirement exists, adding score");
				concept.addAspect(FunctionalRequirementScoreImpl.builder().functionalRequirement(requirement)
						.description(event.getDescription()).weight(event.getWeight()).key(event.getKey()).build());
			});
		}));
	}

	private TownPlanEvent apply(final @NonNull FunctionalRequirementAspectAddedEvent event) {
		return withEntityEvent(event,
				() -> getConceptImpl(event.getModelComponentKey()).ifPresent(concept -> concept.addAspect(
						FunctionalRequirementImpl.builder().description(event.getDescription()).title(event.getTitle())
								.key(event.getKey()).sortKey(event.getSortKey()).weight(event.getWeight()).build())));
	}

	private TownPlanEvent apply(final @NonNull QualityAttributeRequirementAspectAddedEvent event) {
		return withEntityEvent(event, () -> getConceptImpl(event.getModelComponentKey())
				.ifPresent(concept -> concept.addAspect(QualityAttributeRequirementImpl.builder().key(event.getKey())
						.environment(event.getEnvironment()).response(event.getResponse())
						.responseMeasure(event.getResponseMeasure()).sourceOfStimulus(event.getSourceOfStimulus())
						.stimulus(event.getStimulus()).title(event.getTitle()).sortKey(event.getSortKey())
						.weight(event.getWeight()).build())));
	}

	private TownPlanEvent apply(final @NonNull SecurityConcernAspectAddedEvent event) {
		return withEntityEvent(event,
				() -> getConceptImpl(event.getModelComponentKey()).ifPresent(
						concept -> concept.addAspect(SecurityConcernImpl.builder().key(UUID.randomUUID().toString())
								.title(event.getTitle()).description(event.getDescription())
								.concernType(event.getConcernType()).sortKey(event.getSortKey()).build())));
	}

	private TownPlanEvent apply(final @NonNull SecurityImpactAspectAddedEvent event) {
		return withEntityEvent(event,
				() -> getConceptImpl(event.getModelComponentKey()).ifPresent(concept -> concept.addAspect(
						SecurityImpactImpl.builder().key(UUID.randomUUID().toString()).impactType(event.getImpactType())
								.description(event.getDescription()).impactLevel(event.getImpactLevel()).build())));
	}

	private TownPlanEvent apply(final @NonNull ContentDistributionAspectAddedEvent event) {
		return withEntityEvent(event,
				() -> getModelComponentImpl(event.getModelComponentKey())
						.ifPresent(modelComponent -> modelComponent.addContentDistribution(ContentDistributionImpl
								.builder().type(event.getType()).url(event.getUrl()).title(event.getTitle()).build())));
	}

	private TownPlanEvent apply(final @NonNull DocumentationAspectAddedEvent event) {
		return withEntityEvent(event, () -> {
			final AbstractElementImpl element = elements.get(event.getConceptKey());
			final RelationshipImpl relationship = relationships.get(event.getConceptKey());
			final AbstractConceptImpl concept = element == null ? relationship : element;
			if (concept != null) {
				DocumentationImpl documentation = DocumentationImpl.builder().type(event.getType())
						.sortKey(event.getSortKey()).description(event.getDescription()).build();
				concept.addAspect(documentation);
			}
		});
	}

	private TownPlanEvent apply(final @NonNull DocumentationAspectDeletedEvent event) {
		return withEntityEvent(event, () -> getConceptImpl(event.getConceptKey())
				.ifPresent(concept -> concept.removeAspect(event.getDocumentationKey())));
	}

	private TownPlanEvent apply(final @NonNull LifecycleAspectSetEvent event) {
		return withEntityEvent(event, () -> {
			final AbstractElementImpl element = elements.get(event.getConceptKey());
			final RelationshipImpl relationship = relationships.get(event.getConceptKey());
			final AbstractConceptImpl concept = element == null ? relationship : element;
			if (concept != null) {
				LifecycleImpl lifecycle = LifecycleImpl.builder().type(event.getType())
						.description(event.getDescription()).build();
				concept.setLifecycle(lifecycle);
			}
		});
	}

	private TownPlanEvent apply(final @NonNull ArchitectureVerdictAspectSetEvent event) {
		return withEntityEvent(event,
				() -> getConceptImpl(event.getConceptKey()).ifPresent(concept -> concept
						.setArchitectureVerdict(ArchitectureVerdictImpl.builder().key(UUID.randomUUID().toString())
								.verdictType(event.getVerdictType()).description(event.getDescription()).build())));
	}

	private TownPlanEvent apply(final @NonNull RelationshipAddedEvent event) {
		return withEntityEvent(event, () -> {
			log.debug("adding relationship between {} and {}", event.getSource(), event.getTarget());
			final AbstractElementImpl source = elements.get(event.getSource());
			final AbstractElementImpl target = elements.get(event.getTarget());
			final Optional<ItSystemIntegrationImpl> integration = event.getIntegrationIdentifier() == null
					? Optional.empty()
					: getElement(ItSystemIntegrationImpl.class, event.getIntegrationIdentifier());

			if (source != null && target != null && source.acceptsOutgoingDependency(target.getClass())
					&& target.acceptsIncomingDependency(source.getClass())) {
				RelationshipImpl relationship = RelationshipImpl.builder()
						.key(getRelationshipKey(event.getSource(), event.getTarget(), event.getType())).source(source)
						.target(target).bidirectional(event.isBidirectional()).type(event.getType().name())
						.relationshipType(event.getType()).influence(event.getInfluence()).interest(event.getInterest())
						.description(event.getDescription()).title(event.getTitle()).build();
				this.relationships.put(relationship.getKey(), relationship);
				source.addRelationship(relationship);
				target.addRelationship(relationship);
			}
		});
	}

	private TownPlanEvent apply(final @NonNull RelationshipUpdatedEvent event) {
		return withEntityEvent(event,
				() -> getRelationship(event.getSource(), event.getTarget(), event.getType())
						.filter(RelationshipImpl.class::isInstance).map(RelationshipImpl.class::cast)
						.ifPresent(relationship -> {
							relationship.setBidirectional(event.isBidirectional());
							relationship.setDescription(event.getDescription());
							relationship.setTitle(event.getTitle());
							relationship.setInfluence(event.getInfluence());
							relationship.setInterest(event.getInterest());
						}));
	}

	private TownPlanEvent apply(final @NonNull RelationshipDeletedEvent event) {
		return withEntityEvent(event,
				() -> getRelationship(event.getSource(), event.getTarget(), event.getType())
						.filter(RelationshipImpl.class::isInstance).map(RelationshipImpl.class::cast)
						.ifPresent(relationship -> {
							getElement(AbstractElementImpl.class, event.getSource())
									.ifPresent(existingSource -> existingSource.removeRelationship(relationship));
							getElement(AbstractElementImpl.class, event.getTarget())
									.ifPresent(existingTarget -> existingTarget.removeRelationship(relationship));
							relationships.remove(relationship.getKey());
						}));
	}

	private String getRelationshipKey(@NonNull final String sourceKey, @NonNull final String targetKey,
			final RelationshipType type) {
		return sourceKey + "_" + type.name() + "_" + targetKey;
	}

	private TownPlanEvent apply(final @NonNull ImpactViewAddedEvent event) {
		return withEntityEvent(event, () -> {
			ImpactViewImpl impactView = ImpactViewImpl.builder().key(event.getKey()).title(event.getTitle())
					.description(event.getDescription()).build();

			impactViews.put(impactView.getKey(), impactView);
		});
	}

	private TownPlanEvent apply(final @NonNull ImpactViewCapabilityAddedEvent event) {
		return withEntityEvent(event,
				() -> getImpactView(event.getView()).filter(ImpactViewImpl.class::isInstance)
						.map(ImpactViewImpl.class::cast)
						.ifPresent(view -> getElement(BusinessCapability.class, event.getCapability())
								.ifPresent(capability -> view.addCapability(capability, event.isImpacted()))));
	}

	private TownPlanEvent apply(final @NonNull FlowViewAddedEvent event) {
		return withEntityEvent(event, () -> {
			FlowViewImpl flowView = FlowViewImpl.builder().key(event.getKey()).title(event.getTitle())
					.description(event.getDescription()).showStepCounter(event.isShowStepCounter()).build();

			flowViews.put(flowView.getKey(), flowView);
		});
	}

	private TownPlanEvent apply(final @NonNull FlowViewUpdatedEvent event) {
		return withEntityEvent(event, () -> getFlowView(event.getKey()).filter(FlowViewImpl.class::isInstance)
				.map(FlowViewImpl.class::cast).ifPresent(flowView -> {
					flowView.setDescription(event.getDescription());
					flowView.setTitle(event.getTitle());
					flowView.setShowStepCounter(event.isShowStepCounter());
				}));
	}

	private TownPlanEvent apply(final @NonNull FlowViewDeletedEvent event) {
		return withEntityEvent(event, () -> flowViews.remove(event.getKey()));
	}

	private TownPlanEvent apply(final @NonNull FlowViewStepsClearedEvent event) {
		return withEntityEvent(event, () -> {
			FlowViewImpl flowView = flowViews.get(event.getView());
			if (flowView != null)
				flowView.clearSteps();
		});
	}

	private TownPlanEvent apply(final @NonNull FlowViewStepAddedEvent event) {
		return withEntityEvent(event, () -> {
			FlowViewImpl flowView = flowViews.get(event.getView());
			if (flowView != null) {
				RelationshipImpl relationship = relationships.get(event.getRelationship());
				if (relationship != null) {
					flowView.addStep(StepImpl.builder().title(event.getTitle()).relationship(relationship)
							.response(event.isResponse()).build());
				}
			}
		});
	}

	private TownPlanEvent apply(final @NonNull FlowViewStepsSetEvent event) {
		log.debug("applying flow view steps {}", event);
		return withEntityEvent(event, () -> {
			FlowViewImpl flowView = flowViews.get(event.getView());
			flowView.clearSteps();
			event.getSteps()
					.forEach(step -> getRelationship(step.getRelationship())
							.ifPresent(relationship -> flowView.addStep(StepImpl.builder().title(step.getTitle())
									.relationship(relationship).response(step.isResponse()).build())));
		});
	}

	private TownPlanEvent apply(final @NonNull SystemIntegrationStepsSetEvent event) {
		log.debug("applying integration steps set event {}", event);
		return withEntityEvent(event,
				() -> getElement(ItSystemIntegrationImpl.class, event.getKey()).ifPresent(integration -> {
					integration.clearSteps();
					event.getSteps().forEach(step -> getRelationship(step.getRelationship()).stream()
							.peek(relationship -> log.debug("relationship to add to system integration: {}",
									relationship.getKey()))
							.forEach(relationship -> integration.addStep(StepImpl.builder().title(step.getTitle())
									.relationship(relationship).response(step.isResponse()).build())));
				}));
	}

	private TownPlanEvent apply(final @NonNull AwsElementClearedEvent event) {
		return withEntityEvent(event, () -> {
			Set.copyOf(this.relationships.values()).stream().filter(
					relationship -> Objects.equals(relationship.getRelationshipType(), RelationshipType.AWS_DEPENDENCY))
					.forEach(relationship -> this.relationships.remove(relationship.getKey()));
			Set.copyOf(this.elements.values()).stream().filter(element -> element instanceof AwsElement)
					.filter(element -> !(element instanceof AwsRegion))
					.filter(element -> !(element instanceof AwsAccount))
					.filter(element -> element.getRelationships().stream()
							.noneMatch(relationship -> Objects.equals(relationship.getRelationshipType(),
									RelationshipType.IMPLEMENTATION)))
					.forEach(element -> this.elements.remove(element.getKey()));
		});
	}

	@Override
	public <T extends Element> Set<T> getElements(Class<T> elementClass) {
		return elements.values().stream().filter(elementClass::isInstance).map(elementClass::cast)
				.collect(Collectors.toSet());
	}

	@Override
	public Set<FlowView> getFlowViews() {
		return Set.copyOf(flowViews.values());
	}

	@Override
	public Optional<FlowView> getFlowView(String key) {
		if (key == null)
			return Optional.empty();
		return Optional.ofNullable(flowViews.get(key));
	}

	@Override
	public Set<Relationship> getAllRelationships() {
		return Set.copyOf(relationships.values());
	}

	@Override
	public Optional<Relationship> getRelationship(@NonNull final String sourceKey, @NonNull final String targetKey,
			RelationshipType type) {
		return Optional.ofNullable(relationships.get(getRelationshipKey(sourceKey, targetKey, type)));
	}

	@Override
	public Optional<Relationship> getRelationship(Element source, Element target, RelationshipType type) {
		return Optional.ofNullable(relationships.get(getRelationshipKey(source.getKey(), target.getKey(), type)));
	}

	@Override
	public Optional<Relationship> getRelationship(String key) {
		return Optional.ofNullable(relationships.get(key));
	}

	@Override
	public boolean hasRelationship(String key) {
		return relationships.containsKey(key);
	}

	@Override
	public Set<? extends Concept> getAllConcepts() {
		return Sets.union(Set.copyOf(relationships.values()), Set.copyOf(elements.values()));
	}

	@Override
	public Optional<? extends Concept> getConcept(@NonNull final String key) {
		if (relationships.containsKey(key))
			return Optional.of(relationships.get(key));
		else if (elements.containsKey(key))
			return Optional.of(elements.get(key));
		else
			return Optional.empty();
	}

	@Override
	public Optional<? extends ModelComponent> getModelComponent(@NonNull final String key) {
		if (relationships.containsKey(key))
			return Optional.of(relationships.get(key));
		else if (elements.containsKey(key))
			return Optional.of(elements.get(key));
		else if (flowViews.containsKey(key))
			return Optional.of(flowViews.get(key));
		else
			return Optional.empty();
	}

	private Optional<? extends AbstractConceptImpl> getConceptImpl(@NonNull final String key) {
		return getConcept(key).filter(AbstractConceptImpl.class::isInstance).map(AbstractConceptImpl.class::cast);
	}

	private Optional<? extends AbstractModelComponentImpl> getModelComponentImpl(@NonNull final String key) {
		return getModelComponent(key).filter(AbstractModelComponentImpl.class::isInstance)
				.map(AbstractModelComponentImpl.class::cast);
	}

	@Override
	public Set<? extends ModelComponent> getAllModelComponents() {
		return Sets.union(getAllConcepts(), getFlowViews());
	}

	@Override
	public Set<ImpactView> getImpactViews() {
		return Set.copyOf(impactViews.values());
	}

	@Override
	public Optional<ImpactView> getImpactView(String key) {
		if (key == null)
			return Optional.empty();
		return Optional.ofNullable(impactViews.get(key));
	}

	@Override
	public List<KeyPointInTime> getKeyPointsInTime() {
		Collection<KeyPointInTimeImpl> points = new ArrayList<>(keyPointsInTime.values());
		if (points.stream().noneMatch(KeyPointInTimeImpl::isToday)) {
			points.add(KeyPointInTimeImpl.builder().date(LocalDate.now()).title("Today").build());
		}
		return points.stream().sorted(Comparator.comparing(KeyPointInTimeImpl::getDate)).collect(Collectors.toList());
	}
}
