package com.innovenso.townplan.domain;

import com.innovenso.townplan.api.value.Relationship;
import com.innovenso.townplan.api.value.view.Step;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;
import org.apache.commons.lang3.StringUtils;

import java.util.Optional;

@SuperBuilder
public class StepImpl implements Step {
	private @NonNull final Relationship relationship;
	private final String title;
	private final boolean response;

	@Override
	public Relationship getRelationship() {
		return relationship;
	}

	@Override
	public Optional<String> getTitle() {
		return Optional.ofNullable(title);
	}

	public String getActualTitle() {
		if (StringUtils.isEmpty(title))
			return relationship.getTitle();
		else
			return title;
	}

	@Override
	public boolean isResponse() {
		return response;
	}

	public String getLabel() {
		StringBuilder builder = new StringBuilder(getActualTitle());
		if (!isResponse() && !getRelationship().getTechnologies().isEmpty())
			builder.append(" [").append(getRelationship().getTechnologyLabel()).append("]");
		return builder.toString();
	}
}
