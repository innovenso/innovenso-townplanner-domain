package com.innovenso.townplan.domain;

import com.innovenso.townplan.api.value.aspects.ConstraintScore;
import com.innovenso.townplan.api.value.aspects.ScoreWeight;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

@Data
@Builder
public class ConstraintScoreImpl implements ConstraintScore {
	String key;
	String description;
	ConstraintImpl constraint;
	@Builder.Default
	@NonNull
	ScoreWeight weight = ScoreWeight.UNKNOWN;
}
