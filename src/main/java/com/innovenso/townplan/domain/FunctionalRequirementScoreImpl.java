package com.innovenso.townplan.domain;

import com.innovenso.townplan.api.value.aspects.FunctionalRequirementScore;
import com.innovenso.townplan.api.value.aspects.ScoreWeight;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

@Data
@Builder
public class FunctionalRequirementScoreImpl implements FunctionalRequirementScore {
	String key;
	String description;
	FunctionalRequirementImpl functionalRequirement;
	@Builder.Default
	@NonNull
	ScoreWeight weight = ScoreWeight.UNKNOWN;
}
