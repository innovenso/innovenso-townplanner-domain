package com.innovenso.townplan.domain.sample;

import com.innovenso.townplan.api.KeyPointInTime;
import com.innovenso.townplan.api.command.*;
import com.innovenso.townplan.api.value.Concept;
import com.innovenso.townplan.api.value.Element;
import com.innovenso.townplan.api.value.Relationship;
import com.innovenso.townplan.api.value.RelationshipType;
import com.innovenso.townplan.api.value.aspects.*;
import com.innovenso.townplan.api.value.business.BusinessActor;
import com.innovenso.townplan.api.value.business.Enterprise;
import com.innovenso.townplan.api.value.enums.*;
import com.innovenso.townplan.api.value.it.*;
import com.innovenso.townplan.api.value.it.decision.*;
import com.innovenso.townplan.api.value.it.principle.Principle;
import com.innovenso.townplan.api.value.strategy.BusinessCapability;
import com.innovenso.townplan.api.value.view.FlowView;
import com.innovenso.townplan.api.value.view.Step;
import com.innovenso.townplan.api.value.view.StepKeyValuePair;
import com.innovenso.townplan.domain.TownPlanImpl;
import com.thedeanda.lorem.LoremIpsum;
import lombok.NonNull;

import java.security.SecureRandom;
import java.time.LocalDate;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.function.IntConsumer;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class SampleFactory {
	private final TownPlanImpl townPlan;
	private final Random random = new SecureRandom();
	private final LoremIpsum lorem = LoremIpsum.getInstance();

	public SampleFactory(@NonNull final TownPlanImpl townPlan) {
		this.townPlan = townPlan;
	}

	public TownPlanImpl getTownPlan() {
		return townPlan;
	}

	public Enterprise enterprise() {
		return enterprise(id(), title());
	}

	public Enterprise enterprise(String key, String title) {
		townPlan.execute(AddEnterpriseCommand.builder().key(key).title(title).description(description()).build());
		return townPlan.getElement(Enterprise.class, key).orElse(null);
	}

	public ItSystem system(LifecyleType lifecyleType) {
		String key = id();
		townPlan.execute(AddITSystemCommand.builder().key(key).title(title()).description(description()).build());
		setLifecycle(key, lifecyleType);
		return townPlan.getElement(ItSystem.class, key).orElse(null);
	}

	public ItSystem system(String key, String title) {
		return system(key, title, null);
	}

	public ItSystem system(String key, String title, String platformKey) {
		townPlan.execute(AddITSystemCommand.builder().key(key).title(title).description(description())
				.platform(platformKey).build());
		return townPlan.getElement(ItSystem.class, key).orElse(null);
	}

	public ItSystemIntegration integration(ItSystem system1, ItSystem system2) {
		return integration(id(), system1, system2);
	}

	public ItSystemIntegration integration(String key, ItSystem system1, ItSystem system2) {
		return integration(id(), title(), system1, system2);
	}

	public ItSystemIntegration integration(String key, String title, ItSystem system1, ItSystem system2) {
		townPlan.execute(AddITSystemIntegrationCommand.builder().key(key).title(title).description(description())
				.sourceSystem(system1.getKey()).targetSystem(system2.getKey()).build());
		return townPlan.getElement(ItSystemIntegration.class, key).orElse(null);
	}

	public ItPlatform platform(String key, String title) {
		townPlan.execute(AddITPlatformCommand.builder().key(key).title(title).description(description()).build());
		return townPlan.getElement(ItPlatform.class, key).orElse(null);
	}

	public ItSystem system() {
		return system(id(), title());
	}

	public DataEntity entity(String enterpriseKey) {
		return entity(id(), randomEnum(EntityType.class), enterpriseKey);
	}

	public DataEntity entity(String key, EntityType entityType, String enterpriseKey) {
		townPlan.execute(AddDataEntityCommand.builder().key(key).title(title()).description(description())
				.entityType(entityType).enterprise(enterpriseKey).build());
		randomTimes(20, it -> entityField(key));
		return townPlan.getElement(DataEntity.class, key).orElse(null);
	}

	public DataEntityField entityField(String entityKey) {
		String key = id();
		townPlan.execute(AddDataEntityFieldCommand.builder().key(key).type(title()).title(title())
				.description(description()).dataEntityKey(entityKey).fieldConstraints(title()).build());
		return townPlan.getElement(DataEntityField.class, key).orElse(null);
	}

	public void setLifecycle(String key, LifecyleType lifecyleType) {
		townPlan.execute(SetLifecycleAspectCommand.builder().conceptKey(key).description(description())
				.lifecyleType(lifecyleType).build());
	}

	public void architectureVerdict(Concept concept) {
		townPlan.execute(SetArchitectureVerdictAspectCommand.builder().conceptKey(concept.getKey())
				.description(description()).verdictType(randomEnum(ArchitectureVerdictType.class)).build());
	}

	public void swots(Concept concept) {
		randomTimes(10, it -> swot(concept, randomEnum(SWOTType.class)));
	}

	public void swot(Concept concept, SWOTType swotType) {
		townPlan.execute(AddSwotAspectCommand.builder().conceptKey(concept.getKey()).description(description())
				.type(swotType).build());
	}

	public void documentations(Concept concept) {
		randomTimes(10, it -> documentation(concept, randomEnum(DocumentationType.class)));
	}

	public void documentation(Concept concept, DocumentationType documentationType) {
		townPlan.execute(AddDocumentationAspectCommand.builder().conceptKey(concept.getKey()).description(description())
				.type(documentationType).build());
	}

	public void security(Concept concept) {
		Stream.of(SecurityConcernType.values()).forEach(type -> securityConcern(concept, type));
		Stream.of(SecurityImpactType.values()).forEach(type -> securityImpact(concept, type));
	}

	public void securityConcern(Concept concept, SecurityConcernType concernType) {
		townPlan.execute(AddSecurityConcernAspectCommand.builder().concernType(concernType)
				.modelComponentKey(concept.getKey()).sortKey(id()).title(title()).description(description()).build());
	}

	public void securityImpact(Concept concept, SecurityImpactType impactType) {
		townPlan.execute(AddSecurityImpactAspectCommand.builder().modelComponentKey(concept.getKey())
				.impactLevel(randomEnum(SecurityImpactLevel.class)).description(description()).impactType(impactType)
				.build());
	}

	public void sparx(Concept concept) {
		sparx(concept.getKey());
	}

	public void sparx(String key) {
		townPlan.execute(
				AddExternalIdAspectCommand.builder().conceptKey(key).type(ExternalIdType.SPARX).value(id()).build());
	}

	public void fatherTime(Concept concept, LocalDate date, FatherTimeType type) {
		townPlan.execute(AddFatherTimeAspectCommand.builder().key(id()).conceptKey(concept.getKey()).title(title())
				.description(description()).pointInTime(date).fatherTimeType(type).build());
	}

	public KeyPointInTime keyPointInTime(LocalDate localDate) {
		townPlan.execute(
				AddKeyPointInTimeCommand.builder().title(title()).date(localDate).diagramsNeeded(true).build());
		return townPlan.getKeyPointsInTime().stream().filter(it -> it.getDate().equals(localDate)).findFirst()
				.orElse(null);
	}

	public ItContainer container(ItSystem system, LifecyleType lifecyleType) {
		String key = id();
		townPlan.execute(AddITContainerCommand.builder().key(key).title(title()).description(description())
				.type(randomEnum(ContainerType.class)).system(system.getKey()).build());
		setLifecycle(key, lifecyleType);
		return townPlan.getElement(ItContainer.class, key).map(container -> {
			randomTimes(5, it -> addTechnology(container));
			return container;
		}).orElse(null);
	}

	public ItContainer container(ItSystem system) {
		return container(system, id(), title());
	}

	public ItContainer container(ItSystem system, String key, String title) {
		return container(system, key, title, "microservice");
	}

	public ItContainer container(ItSystem system, String key, String title, String type) {
		townPlan.execute(AddITContainerCommand.builder().key(key).title(title).description(description())
				.type(randomEnum(ContainerType.class)).system(system.getKey()).build());
		return townPlan.getElement(ItContainer.class, key).map(container -> {
			randomTimes(5, it -> addTechnology(container));
			return container;
		}).orElse(null);
	}

	public ItProject project(Enterprise enterprise) {
		String key = id();
		townPlan.execute(AddItProjectCommand.builder().enterprise(enterprise.getKey()).key(key).title(title())
				.description(description()).type(title()).build());
		return townPlan.getElement(ItProject.class, key).orElse(null);
	}

	public Principle principle(Enterprise enterprise) {
		String key = id();
		townPlan.execute(AddPrincipleCommand.builder().key(key).title(title()).description(description()).sortKey(id())
				.type(title()).enterprise(enterprise.getKey()).build());
		return townPlan.getElement(Principle.class, key).orElse(null);
	}

	public Decision decision(Enterprise enterprise) {
		String key = id();
		townPlan.execute(AddDecisionCommand.builder().key(key).title(title()).description(description())
				.status(randomEnum(DecisionStatus.class)).enterprise(enterprise.getKey()).outcome(description())
				.build());
		return townPlan.getElement(Decision.class, key).map(decision -> {
			requirements(decision);
			return decision;
		}).orElse(null);
	}

	public Technology technology() {
		String key = id();
		townPlan.execute(AddTechnologyCommand.builder().key(key).title(title()).description(description())
				.type("technology").technologyType(randomEnum(TechnologyType.class))
				.recommendation(randomEnum(TechnologyRecommendation.class)).build());
		return townPlan.getElement(Technology.class, key).orElse(null);
	}

	public DecisionOption option(Decision decision) {
		String key = id();
		townPlan.execute(AddDecisionOptionCommand.builder().key(key).decision(decision.getKey()).title(title())
				.description(description()).verdict(randomEnum(DecisionOptionVerdict.class)).sortKey(id()).build());
		randomTimes(10, it -> townPlan.execute(AddSwotAspectCommand.builder().type(randomEnum(SWOTType.class))
				.conceptKey(key).description(title()).build()));
		return townPlan.getElement(DecisionOption.class, key).map(option -> {
			scores(option);
			costs(option, 2022);
			costs(option, 2023);
			return option;
		}).orElse(null);
	}

	public void requirements(Concept concept) {
		randomTimes(20, it -> functionalRequirement(concept));
		randomTimes(20, it -> qar(concept));
		randomTimes(20, it -> constraint(concept));
	}

	public FunctionalRequirement functionalRequirement(Concept concept) {
		String key = id();
		townPlan.execute(AddFunctionalRequirementAspectCommand.builder().key(key).title(title())
				.description(description()).modelComponentKey(concept.getKey()).sortKey(id())
				.weight(randomEnum(RequirementWeight.class)).build());
		return townPlan.getConcept(concept.getKey()).flatMap(c -> c.getFunctionalRequirements().stream()
				.filter(requirement -> requirement.getKey().equals(key)).findFirst()).orElse(null);
	}

	public QualityAttributeRequirement qar(Concept concept) {
		String key = id();
		townPlan.execute(AddQualityAttributeRequirementAspectCommand.builder().key(key).title(title())
				.environment(title()).response(title()).responseMeasure(title()).sourceOfStimulus(title())
				.stimulus(title()).modelComponentKey(concept.getKey()).sortKey(id())
				.weight(randomEnum(RequirementWeight.class)).build());
		return townPlan.getConcept(concept.getKey()).flatMap(c -> c.getQualityAttributes().stream()
				.filter(requirement -> requirement.getKey().equals(key)).findFirst()).orElse(null);
	}

	public Constraint constraint(Concept concept) {
		String key = id();
		townPlan.execute(AddConstraintAspectCommand.builder().key(key).title(title()).description(description())
				.modelComponentKey(concept.getKey()).sortKey(id()).weight(randomEnum(RequirementWeight.class)).build());
		return townPlan.getConcept(concept.getKey()).flatMap(
				c -> c.getConstraints().stream().filter(requirement -> requirement.getKey().equals(key)).findFirst())
				.orElse(null);
	}

	public void scores(Concept concept) {
		String key = id();
		concept.getFunctionalRequirements()
				.forEach(requirement -> townPlan.execute(AddFunctionalRequirementScoreAspectCommand.builder()
						.modelComponentKey(concept.getKey()).key(id()).functionalRequirementKey(requirement.getKey())
						.description(description()).weight(randomEnum(ScoreWeight.class)).build()));
		concept.getQualityAttributes()
				.forEach(requirement -> townPlan.execute(
						AddQualityAttributeRequirementScoreAspectCommand.builder().modelComponentKey(concept.getKey())
								.key(id()).qualityAttributeRequirementKey(requirement.getKey())
								.description(description()).weight(randomEnum(ScoreWeight.class)).build()));
		concept.getConstraints()
				.forEach(requirement -> townPlan.execute(AddConstraintScoreAspectCommand.builder().key(id())
						.modelComponentKey(concept.getKey()).constraintKey(requirement.getKey())
						.description(description()).weight(randomEnum(ScoreWeight.class)).build()));
	}

	public void costs(Concept concept, Integer fiscalYear) {
		randomTimes(15,
				it -> townPlan.execute(AddCostAspectCommand.builder().key(id()).description(title()).title(title())
						.category(title()).conceptKey(concept.getKey()).fiscalYear(fiscalYear)
						.costPerUnit(randomDouble(2000)).unitOfMeasure(word()).costType(randomEnum(CostType.class))
						.numberOfUnits(randomDouble(12)).build()));
	}

	public DecisionContext currentState(Decision decision) {
		return decisionContext(decision, DecisionContextType.CURRENT_STATE);
	}

	public DecisionContext consequence(Decision decision) {
		return decisionContext(decision, DecisionContextType.CONSEQUENCE);
	}

	public DecisionContext goal(Decision decision) {
		return decisionContext(decision, DecisionContextType.GOAL);
	}

	public DecisionContext assumption(Decision decision) {
		return decisionContext(decision, DecisionContextType.ASSUMPTION);
	}

	public void descriptionDocumentation(Concept concept) {
		townPlan.execute(AddDocumentationAspectCommand.builder().type(DocumentationType.DESCRIPTION)
				.conceptKey(concept.getKey()).description(description()).build());
	}

	public DecisionContext decisionContext(Decision decision, DecisionContextType contextType) {
		String key = id();
		townPlan.execute(AddDecisionContextCommand.builder().key(key).contextType(contextType)
				.decision(decision.getKey()).sortKey(id()).title(title()).description(description()).build());
		return townPlan.getElement(DecisionContext.class, key).orElse(null);
	}

	public ItProjectMilestone milestone(ItProject project) {
		String key = id();
		townPlan.execute(AddItProjectMilestoneCommand.builder().key(key).title(title()).description(description())
				.project(project.getKey()).build());
		return townPlan.getElement(ItProjectMilestone.class, key).orElse(null);
	}

	public void addTechnology(ItContainer container) {
		Technology technology = technology();
		townPlan.execute(AddITContainerTechnologyCommand.builder().container(container.getKey())
				.technology(technology.getKey()).build());
	}

	public BusinessActor actor(Enterprise enterprise) {
		String key = id();
		townPlan.execute(AddActorCommand.builder().key(key).enterprise(enterprise.getKey()).description(description())
				.title(title()).type(randomEnum(ActorType.class)).build());
		return townPlan.getElement(BusinessActor.class, key).orElse(null);
	}

	public ArchitectureBuildingBlock buildingBlock(Enterprise enterprise) {
		return buildingBlock(id(), enterprise);
	}

	public BusinessCapability capability(Enterprise enterprise) {
		return capability(id(), enterprise, null);
	}

	public BusinessCapability capability(Enterprise enterprise, BusinessCapability parent) {
		return capability(id(), enterprise, parent == null ? null : parent.getKey());
	}

	public BusinessCapability capability(String key, Enterprise enterprise, String parentKey) {
		townPlan.execute(AddCapabilityCommand.builder().key(key).title(title()).description(description())
				.enterprise(enterprise.getKey()).sortKey(id()).parent(parentKey).build());
		return townPlan.getElement(BusinessCapability.class, key).orElse(null);
	}

	public ArchitectureBuildingBlock buildingBlock(String key, Enterprise enterprise) {
		townPlan.execute(AddBuildingBlockCommand.builder().key(key).title(title()).description(description())
				.enterprise(enterprise.getKey()).build());
		return townPlan.getElement(ArchitectureBuildingBlock.class, key).orElse(null);
	}

	public ArchitectureBuildingBlock buildingBlock(Enterprise enterprise, BusinessCapability capability) {
		return buildingBlock(id(), enterprise, capability);
	}

	public ArchitectureBuildingBlock buildingBlock(String key, Enterprise enterprise, BusinessCapability capability) {
		ArchitectureBuildingBlock block = buildingBlock(key, enterprise);
		realizes(block, capability);
		return block;
	}

	public FlowView flowView(String key, boolean showStepCounter) {
		townPlan.execute(AddFlowViewCommand.builder().key(key).title(title()).description(description())
				.showStepCounter(showStepCounter).build());
		return townPlan.getFlowView(key).orElse(null);
	}

	public FlowView flowView(boolean showStepCounter) {
		return flowView(id(), showStepCounter);
	}

	public FlowView flowView() {
		return flowView(true);
	}

	public void flowViewStep(FlowView view, Relationship relationship, boolean response) {
		townPlan.execute(AddFlowViewStepCommand.builder().relationship(relationship.getKey()).view(view.getKey())
				.response(response).title(title()).build());
	}

	public void flowViewStep(FlowView view, Relationship relationship) {
		townPlan.execute(AddFlowViewStepCommand.builder().relationship(relationship.getKey()).view(view.getKey())
				.response(false).title(title()).build());
	}

	public void steps(ItSystemIntegration integration, List<Relationship> relationships) {
		townPlan.execute(SetSystemIntegrationStepsCommand.builder().key(integration.getKey())
				.steps(relationships.stream().map(relationship -> StepKeyValuePair.builder()
						.relationship(relationship.getKey()).response(random.nextBoolean()).title(title()).build())
						.toList())
				.build());
	}

	public Relationship composedOf(Element from, Element to) {
		return relationship(from, to, RelationshipType.COMPOSITION);
	}

	public Relationship realizes(Element from, Element to) {
		return relationship(from, to, RelationshipType.REALIZATION);
	}

	public Relationship hasOne(Element from, Element to) {
		return relationship(from, to, RelationshipType.HAS_ONE);
	}

	public Relationship hasOneOrMore(Element from, Element to) {
		return relationship(from, to, RelationshipType.HAS_ONE_OR_MORE);
	}
	public Relationship hasZeroOrMore(Element from, Element to) {
		return relationship(from, to, RelationshipType.HAS_ZERO_OR_MORE);
	}
	public Relationship hasZeroOrOne(Element from, Element to) {
		return relationship(from, to, RelationshipType.HAS_ZERO_OR_ONE);
	}

	public Relationship flow(Element from, Element to) {
		return relationship(from, to, RelationshipType.FLOW);
	}

	public Relationship change(ItProjectMilestone milestone, Element to) {
		return relationship(milestone, to, RelationshipType.IMPACT_CHANGE);
	}

	public Relationship remove(ItProjectMilestone milestone, Element to) {
		return relationship(milestone, to, RelationshipType.IMPACT_REMOVE);
	}

	public Relationship add(ItProjectMilestone milestone, Element to) {
		return relationship(milestone, to, RelationshipType.IMPACT_CREATE);
	}

	public Relationship stay(ItProjectMilestone milestone, Element to) {
		return relationship(milestone, to, RelationshipType.IMPACT_REMAINS_UNCHANGED);
	}

	public Relationship accountable(BusinessActor actor, Element to) {
		return relationship(actor, to, RelationshipType.ACCOUNTABLE);
	}

	public Relationship responsible(BusinessActor actor, Element to) {
		return relationship(actor, to, RelationshipType.RESPONSIBLE);
	}

	public Relationship consulted(BusinessActor actor, Element to) {
		return relationship(actor, to, RelationshipType.HAS_BEEN_CONSULTED);
	}

	public Relationship informed(BusinessActor actor, Element to) {
		return relationship(actor, to, RelationshipType.HAS_BEEN_INFORMED);
	}

	public Relationship influence(BusinessActor actor, Element to) {
		return relationship(actor, to, RelationshipType.INFLUENCES);
	}

	public Relationship relationship(Element from, Element to, RelationshipType relationshipType) {
		townPlan.execute(AddRelationshipCommand.builder().type(relationshipType).title(title())
				.description(description()).source(from.getKey()).target(to.getKey()).build());
		return townPlan.getRelationship(from, to, relationshipType).orElse(null);
	}

	public String title() {
		return lorem.getTitle(3, 6);
	}

	public String word() {
		return lorem.getWords(1);
	}

	public String description() {
		return lorem.getWords(5, 100);
	}

	public String id() {
		return UUID.randomUUID().toString().replace("-", "_");
	}

	public <T extends Enum<?>> T randomEnum(Class<T> clazz) {
		int x = random.nextInt(clazz.getEnumConstants().length);
		return clazz.getEnumConstants()[x];
	}

	public int randomInt(int bound) {
		return random.nextInt(bound);
	}

	public double randomDouble(int bound) {
		return (double) randomInt(bound);
	}

	public void randomTimes(int bound, IntConsumer function) {
		IntStream.range(0, bound).forEach(function);
	}

}
