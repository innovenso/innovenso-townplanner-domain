package com.innovenso.townplan.domain;

import com.innovenso.townplan.api.value.aspects.QualityAttributeRequirement;
import com.innovenso.townplan.api.value.aspects.QualityAttributeRequirementScore;
import com.innovenso.townplan.api.value.aspects.ScoreWeight;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

@Data
@Builder
public class QualityAttributeRequirementScoreImpl implements QualityAttributeRequirementScore {
	String key;
	String description;
	QualityAttributeRequirement qualityAttributeRequirement;
	@Builder.Default
	@NonNull
	ScoreWeight weight = ScoreWeight.UNKNOWN;
}
