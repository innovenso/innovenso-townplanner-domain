package com.innovenso.townplan.domain;

import com.innovenso.townplan.api.value.aspects.ContentDistribution;
import com.innovenso.townplan.api.value.aspects.ContentOutputType;
import lombok.Builder;
import lombok.Setter;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.Optional;

@Builder
public class ContentDistributionImpl implements ContentDistribution {
	private ContentOutputType type;
	private String url;
	@Setter
	private String title;

	public Optional<String> getTitle() {
		return Optional.ofNullable(title);
	}

	@Override
	public ContentOutputType getOutputType() {
		return type;
	}

	public void setOutputType(ContentOutputType type) {
		this.type = type;
	}

	@Override
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String getKey() {
		final String keyString = type.label + "_" + url;
		return DigestUtils.sha1Hex(keyString.getBytes());
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("outputType", type).append("url", url).append("title", title)
				.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;

		if (o == null || getClass() != o.getClass())
			return false;

		ContentDistributionImpl that = (ContentDistributionImpl) o;

		return new EqualsBuilder().append(type, that.type).append(url, that.url).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37).append(type).append(url).toHashCode();
	}
}
