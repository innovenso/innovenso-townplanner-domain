package com.innovenso.townplan.domain;

import com.innovenso.townplan.api.value.aspects.FatherTime;
import com.innovenso.townplan.api.value.aspects.FatherTimeType;
import lombok.Builder;
import lombok.Value;

import java.time.LocalDate;

@Value
@Builder
public class FatherTimeImpl implements FatherTime {
	String key;
	String title;
	String description;
	LocalDate pointInTime;
	FatherTimeType fatherTimeType;
}
