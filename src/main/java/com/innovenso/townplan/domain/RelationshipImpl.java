package com.innovenso.townplan.domain;

import com.innovenso.townplan.api.value.Element;
import com.innovenso.townplan.api.value.Relationship;
import com.innovenso.townplan.api.value.RelationshipType;
import com.innovenso.townplan.api.value.it.ItSystemIntegration;
import com.innovenso.townplan.api.value.it.Technology;
import lombok.Builder;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

@SuperBuilder
public class RelationshipImpl extends AbstractConceptImpl implements Relationship {
	@Override
	public Optional<ItSystemIntegration> getSystemIntegration() {
		return Optional.ofNullable(systemIntegration);
	}

	public void setSystemIntegration(ItSystemIntegrationImpl systemIntegration) {
		this.systemIntegration = systemIntegration;
	}

	@Builder.Default
	private Map<String, TechnologyImpl> technologyMap = new HashMap<>();

	@Override
	public Set<Technology> getTechnologies() {
		if (technologyMap.isEmpty()) {
			return Set.of();
		} else {
			return Set.copyOf(technologyMap.values());
		}
	}

	void addTechnology(@NonNull final TechnologyImpl technology) {
		this.technologyMap.putIfAbsent(technology.getKey(), technology);
	}

	void removeTechnology(@NonNull final TechnologyImpl technology) {
		this.technologyMap.remove(technology.getKey());
	}

	@Override
	public boolean hasTechnology(@NonNull final String key) {
		return technologyMap.containsKey(key);
	}

	private ItSystemIntegrationImpl systemIntegration;

	@Override
	public Element getSource() {
		return source;
	}

	public void setSource(Element source) {
		this.source = source;
	}

	@Override
	public Element getTarget() {
		return target;
	}

	public void setTarget(Element target) {
		this.target = target;
	}

	@Override
	public boolean isBidirectional() {
		return bidirectional;
	}

	public void setBidirectional(boolean bidirectional) {
		this.bidirectional = bidirectional;
	}

	@Override
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	private @NonNull Element source;
	private @NonNull Element target;
	private boolean bidirectional;
	@Deprecated
	@NonNull
	private String type;

	@Override
	public Integer getInterest() {
		return interest == null ? 100 : interest;
	}

	public void setInterest(Integer interest) {
		this.interest = interest;
	}

	@Override
	public Integer getInfluence() {
		return influence == null ? 100 : influence;
	}

	public void setInfluence(Integer influence) {
		this.influence = influence;
	}

	private @Builder.Default @NonNull Integer interest = 100;
	private @Builder.Default @NonNull Integer influence = 100;

	@Override
	public RelationshipType getRelationshipType() {
		return relationshipType;
	}

	public void setRelationshipType(@NonNull RelationshipType relationshipType) {
		this.relationshipType = relationshipType;
	}

	@NonNull
	private RelationshipType relationshipType;

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("source", source).append("target", target)
				.append("bidirectional", bidirectional).append("relationshipType", relationshipType)
				.appendSuper(super.toString()).toString();
	}
}
