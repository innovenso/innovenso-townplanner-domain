package com.innovenso.townplan.domain;

import com.google.common.base.Strings;
import com.innovenso.townplan.api.value.Element;
import com.innovenso.townplan.api.value.business.BusinessActor;
import com.innovenso.townplan.api.value.business.Enterprise;
import com.innovenso.townplan.api.value.it.decision.Decision;
import com.innovenso.townplan.api.value.it.principle.Principle;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@SuperBuilder
@Data
public class PrincipleImpl extends AbstractElementImpl implements Principle {
	private Enterprise enterprise;
	private String type;
	private String sortKey;
	private final Set<Class<? extends Element>> acceptedIncomingDependencies = Set.of(BusinessActor.class);
	private final Set<Class<? extends Element>> acceptedOutgoingDependencies = Set.of(Decision.class);
	private final Set<Class<? extends Element>> acceptedChildren = Set.of();

	public String getSortKey() {
		return Strings.isNullOrEmpty(sortKey) ? getKey() : sortKey;
	}
}
