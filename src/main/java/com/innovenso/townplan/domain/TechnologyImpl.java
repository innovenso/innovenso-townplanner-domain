package com.innovenso.townplan.domain;

import com.innovenso.townplan.api.value.Element;
import com.innovenso.townplan.api.value.enums.TechnologyRecommendation;
import com.innovenso.townplan.api.value.enums.TechnologyType;
import com.innovenso.townplan.api.value.it.Technology;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import java.util.Set;

@SuperBuilder
@ToString
public class TechnologyImpl extends AbstractElementImpl implements Technology {
	@Override
	public TechnologyRecommendation getRecommendation() {
		return recommendation;
	}

	public void setRecommendation(TechnologyRecommendation recommendation) {
		this.recommendation = recommendation;
	}

	private TechnologyRecommendation recommendation;

	private TechnologyType technologyType;

	@Override
	@Deprecated
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	private String type;

	@Override
	public TechnologyType getTechnologyType() {
		if (technologyType == null)
			return TechnologyType.getTechnologyType(getType()).orElse(TechnologyType.UNDEFINED);
		else
			return technologyType;
	}

	public void setTechnologyType(TechnologyType technologyType) {
		this.technologyType = technologyType;
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedIncomingDependencies() {
		return Set.of();
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedOutgoingDependencies() {
		return Set.of();
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedChildren() {
		return Set.of();
	}
}
