package com.innovenso.townplan.domain;

import com.innovenso.townplan.api.value.aspects.FunctionalRequirement;
import com.innovenso.townplan.api.value.aspects.RequirementWeight;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class FunctionalRequirementImpl implements FunctionalRequirement {
	private String key;
	private String title;
	private String description;
	private String sortKey;
	@Builder.Default
	private RequirementWeight weight = RequirementWeight.SHOULD_HAVE;

	public RequirementWeight getWeight() {
		return weight == null ? RequirementWeight.SHOULD_HAVE : weight;
	}
}
