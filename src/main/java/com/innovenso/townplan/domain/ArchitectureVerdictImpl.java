package com.innovenso.townplan.domain;

import com.innovenso.townplan.api.value.aspects.ArchitectureVerdict;
import com.innovenso.townplan.api.value.aspects.ArchitectureVerdictType;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class ArchitectureVerdictImpl implements ArchitectureVerdict {
	private String key;
	private String description;
	private ArchitectureVerdictType verdictType;
}
