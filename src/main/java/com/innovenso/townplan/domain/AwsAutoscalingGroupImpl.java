package com.innovenso.townplan.domain;

import com.innovenso.townplan.api.value.Element;
import com.innovenso.townplan.api.value.deployment.aws.*;
import lombok.experimental.SuperBuilder;
import lombok.extern.log4j.Log4j2;

import java.util.Set;

@SuperBuilder
@Log4j2
public class AwsAutoscalingGroupImpl extends AbstractAwsElementImpl implements AwsAutoScalingGroup {
	private AwsVpc vpc;

	public void setVpc(AwsVpc vpc) {
		this.vpc = vpc;
	}

	@Override
	public AwsVpc getVpc() {
		return vpc;
	}

	@Override
	public Set<AwsAvailabilityZone> getAvailabilityZones() {
		return getChildren(AwsAvailabilityZone.class);
	}

	@Override
	public Set<AwsSubnet> getSubnets() {
		return getChildren(AwsSubnet.class);
	}

	@Override
	public Set<AwsInstance> getInstances() {
		return getChildren(AwsInstance.class);
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedIncomingDependencies() {
		return Set.of(AwsInstanceImpl.class);
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedOutgoingDependencies() {
		return Set.of(AwsInstanceImpl.class);
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedChildren() {
		return Set.of(AwsSubnetImpl.class, AwsInstanceImpl.class, AwsAvailabilityZoneImpl.class);
	}

	@Override
	public String getType() {
		return "AutoscalingGroup";
	}

	@Override
	public String getCategory() {
		return "AWS";
	}
}
