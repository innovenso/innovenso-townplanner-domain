package com.innovenso.townplan.domain;

import com.innovenso.townplan.api.value.Element;
import com.innovenso.townplan.api.value.deployment.aws.*;
import com.innovenso.townplan.api.value.enums.AwsInstanceType;
import com.innovenso.townplan.api.value.it.ItContainer;
import lombok.experimental.SuperBuilder;
import lombok.extern.log4j.Log4j2;

import java.util.Optional;
import java.util.Set;

@SuperBuilder
@Log4j2
public class AwsInstanceImpl extends AbstractAwsElementImpl implements AwsInstance {
	private AwsInstanceType awsInstanceType;
	private AwsVpc vpc;
	private AwsAccount account;
	private AwsRegion region;

	@Override
	public Optional<AwsRegion> getRegion() {
		return Optional.ofNullable(region);
	}

	public void setRegion(AwsRegion region) {
		this.region = region;
	}

	@Override
	public AwsAccount getAccount() {
		return account;
	}

	public void setAccount(AwsAccount account) {
		this.account = account;
	}

	@Override
	public AwsInstanceType getInstanceType() {
		return awsInstanceType;
	}

	public void setAwsInstanceType(AwsInstanceType awsInstanceType) {
		this.awsInstanceType = awsInstanceType;
	}

	@Override
	public Optional<AwsVpc> getVpc() {
		return Optional.ofNullable(vpc);
	}

	public void setVpc(final AwsVpc vpc) {
		this.vpc = vpc;
	}

	@Override
	public Set<AwsSecurityGroup> getSecurityGroups() {
		return getChildren(AwsSecurityGroup.class);
	}

	@Override
	public Set<AwsSubnet> getSubnets() {
		return getChildren(AwsSubnet.class);
	}

	@Override
	public Optional<AwsAutoScalingGroup> getAutoScalingGroup() {
		return Optional.empty();
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedIncomingDependencies() {
		return Set.of(AwsInstance.class, AwsAutoscalingGroupImpl.class, AwsSubnet.class, AwsRouteTable.class,
				AwsElasticIpAddress.class);
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedOutgoingDependencies() {
		return Set.of(AwsInstance.class, ItContainer.class, AwsAutoscalingGroupImpl.class, AwsSubnet.class,
				AwsRouteTable.class, AwsElasticIpAddress.class);
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedChildren() {
		return Set.of(AwsSecurityGroup.class, AwsSubnet.class);
	}

	@Override
	public String getType() {
		return awsInstanceType.label;
	}

	@Override
	public String getCategory() {
		return "AWS";
	}
}
