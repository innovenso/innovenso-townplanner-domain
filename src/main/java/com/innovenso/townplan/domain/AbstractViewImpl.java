package com.innovenso.townplan.domain;

import com.innovenso.townplan.api.value.view.View;
import lombok.experimental.SuperBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@SuperBuilder
public abstract class AbstractViewImpl extends AbstractModelComponentImpl implements View {
	private String title;
	private String description;

	@Override
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;

		if (o == null || getClass() != o.getClass())
			return false;

		AbstractViewImpl that = (AbstractViewImpl) o;

		return new EqualsBuilder().append(getKey(), that.getKey()).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37).append(getKey()).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("key", getKey()).toString();
	}
}
