package com.innovenso.townplan.domain;

import com.innovenso.townplan.api.value.Element;
import com.innovenso.townplan.api.value.deployment.aws.*;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;
import lombok.extern.log4j.Log4j2;

import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@SuperBuilder
@Log4j2
public class AwsVpcImpl extends AbstractAwsElementImpl implements AwsVpc {
	private AwsAccountImpl account;

	@Override
	public AwsAccount getAccount() {
		return account;
	}

	public void setAccount(@NonNull final AwsAccountImpl account) {
		this.account = account;
	}

	private String networkMask;

	public void setRegion(@NonNull final AwsRegionImpl region) {
		this.region = region;
	}

	private AwsRegionImpl region;

	@Override
	public AwsRegion getRegion() {
		return region;
	}

	@Override
	public Set<AwsAvailabilityZone> getAvailabilityZones() {
		return getRegion().getAvailabilityZones();
	}

	@Override
	public Set<AwsSubnet> getSubnets(AwsAvailabilityZone availabilityZone) {
		return getChildren(AwsSubnet.class).stream()
				.filter(subnet -> Objects.equals(subnet.getAvailabilityZone(), availabilityZone))
				.collect(Collectors.toSet());
	}

	@Override
	public Set<AwsSecurityGroup> getSecurityGroups() {
		return getChildren(AwsSecurityGroup.class);
	}

	@Override
	public Set<AwsNetworkAcl> getNetworkAcls() {
		return getChildren(AwsNetworkAcl.class);
	}

	@Override
	public String getNetworkMask() {
		return networkMask;
	}

	public void setNetworkMask(String networkMask) {
		this.networkMask = networkMask;
	}

	@Override
	public Set<AwsRouteTable> getRouteTables() {
		return getChildren(AwsRouteTable.class);
	}

	@Override
	public Set<AwsInstance> getInstances() {
		return getChildren(AwsInstance.class);
	}

	@Override
	public Set<AwsAutoScalingGroup> getAutoScalingGroups() {
		return getChildren(AwsAutoScalingGroup.class);
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedIncomingDependencies() {
		return Set.of();
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedOutgoingDependencies() {
		return Set.of();
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedChildren() {
		return Set.of(AwsRouteTableImpl.class, AwsNetworkAclImpl.class, AwsSubnetImpl.class, AwsSecurityGroupImpl.class,
				AwsInstanceImpl.class, AwsAutoscalingGroupImpl.class);
	}

	@Override
	public String getType() {
		return "VPC";
	}

	@Override
	public String getCategory() {
		return "AWS";
	}
}
