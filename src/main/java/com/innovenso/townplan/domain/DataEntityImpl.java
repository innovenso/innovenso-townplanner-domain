package com.innovenso.townplan.domain;

import com.innovenso.townplan.api.value.Element;
import com.innovenso.townplan.api.value.business.Enterprise;
import com.innovenso.townplan.api.value.enums.EntityType;
import com.innovenso.townplan.api.value.it.*;
import com.innovenso.townplan.api.value.strategy.BusinessCapability;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@SuperBuilder
public class DataEntityImpl extends AbstractElementImpl implements DataEntity {
	private ItSystem masterSystem;
	private boolean consentNeeded;
	private String sensitivity;
	private String commercialValue;
	private Enterprise enterprise;
	private BusinessCapability businessCapability;
	@Getter
	@Setter
	private EntityType entityType;

	@Override
	public Enterprise getEnterprise() {
		return enterprise;
	}

	public void setEnterprise(Enterprise enterprise) {
		this.enterprise = enterprise;
	}

	@Override
	public BusinessCapability getBusinessCapability() {
		return businessCapability;
	}

	@Override
	public List<DataEntityField> getFields() {
		return getChildren(DataEntityFieldImpl.class).stream().sorted(Comparator.comparing(DataEntityField::getSortKey))
				.collect(Collectors.toList());
	}

	public void setBusinessCapability(BusinessCapability businessCapability) {
		this.businessCapability = businessCapability;
	}

	@Override
	public ItSystem getMasterSystem() {
		return masterSystem;
	}

	public void setMasterSystem(ItSystem masterSystem) {
		this.masterSystem = masterSystem;
	}

	@Override
	public boolean isConsentNeeded() {
		return consentNeeded;
	}

	public void setConsentNeeded(boolean consentNeeded) {
		this.consentNeeded = consentNeeded;
	}

	@Override
	public String getSensitivity() {
		return sensitivity;
	}

	public void setSensitivity(String sensitivity) {
		this.sensitivity = sensitivity;
	}

	@Override
	public String getCommercialValue() {
		return commercialValue;
	}

	public void setCommercialValue(String commercialValue) {
		this.commercialValue = commercialValue;
	}

	@Override
	public String getType() {
		return getEntityType().getLabel();
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedIncomingDependencies() {
		return Set.of(DataEntity.class, ItProjectMilestone.class, ItSystem.class);
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedOutgoingDependencies() {
		return Set.of(DataEntity.class, ItSystemIntegration.class);
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedChildren() {
		return Set.of(DataEntityField.class);
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("masterSystem", masterSystem).append("consentNeeded", consentNeeded)
				.append("sensitivity", sensitivity).append("commercialValue", commercialValue)
				.append("type", entityType).appendSuper(super.toString()).toString();
	}
}
