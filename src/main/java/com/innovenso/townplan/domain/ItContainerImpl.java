package com.innovenso.townplan.domain;

import com.innovenso.townplan.api.value.Element;
import com.innovenso.townplan.api.value.business.BusinessActor;
import com.innovenso.townplan.api.value.deployment.aws.AwsInstance;
import com.innovenso.townplan.api.value.enums.ContainerType;
import com.innovenso.townplan.api.value.enums.TechnologyType;
import com.innovenso.townplan.api.value.it.ItContainer;
import com.innovenso.townplan.api.value.it.ItSystem;
import com.innovenso.townplan.api.value.it.Technology;
import com.innovenso.townplan.api.value.it.decision.Decision;
import com.innovenso.townplan.api.value.it.decision.DecisionOption;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@SuperBuilder
public class ItContainerImpl extends AbstractElementImpl implements ItContainer {
	@Getter
	@Setter
	private ContainerType containerType;

	@Builder.Default
	private Map<String, TechnologyImpl> technologyMap = new HashMap<>();

	@Override
	public ItSystem getSystem() {
		return getParent().filter(ItSystem.class::isInstance).map(ItSystem.class::cast).orElse(null);
	}

	@Override
	public Set<Technology> getTechnologies(@NonNull TechnologyType technologyType) {
		return getTechnologies().stream().filter(technology -> technology.getTechnologyType().equals(technologyType))
				.collect(Collectors.toSet());
	}

	@Override
	public Set<Technology> getTechnologies() {
		if (technologyMap.isEmpty()) {
			return Set.of();
		} else {
			return Set.copyOf(technologyMap.values());
		}
	}

	public void addTechnology(@NonNull final TechnologyImpl technology) {
		this.technologyMap.putIfAbsent(technology.getKey(), technology);
	}

	public void deleteTechnology(@NonNull final String technologyKey) {
		this.technologyMap.remove(technologyKey);
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedIncomingDependencies() {
		return Set.of(ItSystem.class, ItContainer.class, BusinessActor.class, AwsInstance.class, Decision.class,
				DecisionOption.class);
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedOutgoingDependencies() {
		return Set.of(ItSystem.class, ItContainer.class);
	}

	@Override
	public Set<Class<? extends Element>> getAcceptedChildren() {
		return Set.of();
	}

	@Override
	public String getCategory() {
		return "Container";
	}

	@Override
	public boolean hasTechnology(@NonNull final String key) {
		return technologyMap.containsKey(key);
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("type", containerType).append("technologyMap", technologyMap)
				.appendSuper(super.toString()).toString();
	}
}
